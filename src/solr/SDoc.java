package solr;

import org.json.JSONObject;

/**
 * Document representation from Solr response
 *
 * @author smoliji4
 */
public final class SDoc {

    public String content;
    public String url;
    public String apiClass;
    public double apiProb;

    SDoc(JSONObject d) {
        try{
            content = d.getString("content");
            url = d.getString("url");
            apiClass = d.getString("api-class");
            apiProb = d.getDouble("api-prob");
        } catch (Exception e) {
            System.err.println("JSON object not in correct format:" + d);
        }
    }
}
