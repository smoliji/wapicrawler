package solr;

import http.Rest;
import wapicrawler.Configuration;

/**
 *
 * @author smoliji
 */
public class Solr {
    
    public static final String SOLR_URL = "solr.url";
    
    private static final String URL = Configuration.getInstance().get(SOLR_URL);
    private static final String UPDATE_URL = URL + "update";
    
    public static void update(String[] selectFields, String[] selectValues,
                              String[] updateFields, String[] updateValues,
                              boolean autoCommit) {
    
        StringBuilder xmlMessageBuilder = new StringBuilder("<add><doc>");
        for (int i = 0; i < Math.min(selectFields.length, selectValues.length); i++) {
            xmlMessageBuilder.append(String.format(
                    "<field name=\"%s\">%s</field>",
                    selectFields[i], selectValues[i]
            ));
        }
        for (int i = 0; i < Math.min(updateFields.length, updateValues.length); i++) {
            xmlMessageBuilder.append(String.format(
                    "<field name=\"%s\" update=\"set\">%s</field>",
                    updateFields[i], updateValues[i]
            ));
        }
        xmlMessageBuilder.append("</doc></add>");
        
        Rest.postXml(UPDATE_URL, xmlMessageBuilder.toString());
        
        if (autoCommit) {
            commit();
        }
    }
    
    public static void update(String fieldId, String id, String field, String value, boolean autoCommit) {
        
        update(new String[]{fieldId}, new String[]{id}, new String[]{field}, new String[]{value}, autoCommit);
        
    }
    
    public static void delete(String query, boolean autoCommit) {
        
        String data = 
                String.format("<delete><query>%s</query></delete>", query);
        
        Rest.postXml(UPDATE_URL, data);
        
        if (autoCommit) {
            commit();
        }
    }
    
    public static void commit() {
        Rest.get(UPDATE_URL + "?commit=true");
    }
    
}
