package solr;

import http.Response;
import org.json.JSONObject;

/**
 *
 * @author smoliji4
 */
public final class SolrResponse {
    
    public SResponse response;
    
    public SolrResponse(Response response) {
        JSONObject o = new JSONObject(response.content);
        this.response = new SResponse(o.getJSONObject("response"));
    }
    
}
