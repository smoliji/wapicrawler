package solr;

import http.Rest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import wapicrawler.Configuration;

/**
 *
 * @author smoliji4
 */
public class SolrQuery {
    
    private String solr = Configuration.getInstance().get("solr.url");
    
    private int     rows    = 10;
    private int     start   = 0;
    private boolean indent  = true;
    private String  wt      = "json";
    private String  q       = "*:*";
    
    public SolrResponse select() {
        try {
            
            StringBuilder qUrl = new StringBuilder(solr);
            qUrl    .append("select")
                    .append("?indent=").append(indent)
                    .append("&rows=").append(rows)
                    .append("&start=").append(start)
                    .append("&wt=").append(wt)
                    .append("&q=").append(URLEncoder.encode(q, "UTF-8"));
            return new SolrResponse(Rest.get(qUrl.toString()));
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SolrQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public SolrQuery setRows(int r) {
        rows = r;
        return this;
    }
    
    public SolrQuery setIndent(boolean i) {
        indent = i;
        return this;        
    }
    
    public SolrQuery setWt(String w) {
        wt = w;
        return this;
    }
    
    public SolrQuery setQ(String q) {
        this.q = q;
        return this;
    }
    
    public SolrQuery setStart(int s) {
        start = s;
        return this;
    }
    
}
