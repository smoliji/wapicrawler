package solr;

import solr.*;

/**
 *
 * @author smoliji4
 */
public class SolrPaginator {
    
    private int curr = 0;
    private int perPage = 10;
    private SolrResponse lastResponse = null;
    private final String q;
    
    public SolrPaginator(String selectQuery) {
        q = selectQuery;
        lastResponse = current();
    }
    
    /**
     * @param selectQuery
     * @param pp Results per page
     */
    public SolrPaginator(String selectQuery, int pp){
        this(selectQuery);
        perPage = pp;
    }
    
    public SolrResponse current() {
        if (lastResponse != null && lastResponse.response.start == curr) {
            return lastResponse;
        }
        return query();
    }
    
    public SolrResponse get() {
        SolrResponse r = current();
        curr += perPage;
        return r;
    }
    
    public SolrResponse next() {
        curr += perPage;
        return query();
    }
    
    public boolean hasNext() {        
        return lastResponse.response.numFound 
                >= curr;
    }
    
    private SolrResponse query() {
        SolrResponse r = new SolrQuery()
                .setQ(q)
                .setStart(curr)
                .setRows(perPage)
                .select();
        lastResponse = r;        
        if (!hasNext()) return null;
        return r;
    }
    
}
