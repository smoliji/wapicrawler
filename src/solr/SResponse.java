package solr;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Representing Object in Solr response
 *
 * @author smoliji4
 */
public final class SResponse {

    public int numFound;
    public int start;
    public SDoc[] docs;

    SResponse(JSONObject r) {
        numFound = r.getInt("numFound");
        start = r.getInt("start");
        JSONArray arr = r.getJSONArray("docs");
        docs = new SDoc[arr.length()];
        for (int i = 0; i < arr.length(); i++) {
            docs[i] = new SDoc(arr.getJSONObject(i));
        }
    }
}
