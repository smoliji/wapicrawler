package machinelearning;

/**
 *
 * @author smoliji4
 */
public final class InstanceEvaluation {

    public double predClsIdx;
    public double instId;
    public String actualCls;
    public String predCls;
    public double prob;

    public InstanceEvaluation(double predClsIdx, double instId, String actualCls, String predCls, double prob) {
        this.predClsIdx = predClsIdx;
        this.instId = instId;
        this.actualCls = actualCls;
        this.predCls = predCls;
        this.prob = prob;
    }

}
