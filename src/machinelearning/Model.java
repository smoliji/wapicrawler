package machinelearning;

import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author smoliji4
 */
public class Model {
    
    Instances trainSet = null;
    Instances testSet = null;
    Classifier classifier = null;
    
    public Model() {
        initClassifier();
    }
    
    public void train(Instances trainSet) {
        try {
            this.trainSet = trainSet;
            classifier.buildClassifier(trainSet);
        } catch (Exception ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void evaluateTrainingSet() {
        try {
            Evaluation eval = new Evaluation(trainSet);
            eval.evaluateModel(classifier, trainSet);
            System.out.println(eval.toSummaryString("\nResults\n======\n", false));
        } catch (Exception ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public InstanceEvaluation evalInstance(int i) throws Exception {
        double pred = classifier.classifyInstance(testSet.instance(i));
        return new InstanceEvaluation(
                pred,
                testSet.instance(i).value(0), 
                testSet.instance(i).stringValue(1), 
                testSet.classAttribute().value((int) pred),
                classifier.distributionForInstance(testSet.instance(i))[(int)pred]);
    }
    
    public void setTestSet(Instances testSet) {
        this.testSet = testSet;
    }
    
    public Instances getTestSet() {
        return this.testSet;
    }
    
    public void setClassifier(Classifier c) {
        classifier = c;
    }
    
    private void initClassifier() {
        ClassifierFactory cf = new ClassifierFactory();
        setClassifier(cf.build());
    }
    
    private void buildClassifier() {
        ClassifierFactory cf = new ClassifierFactory();
        classifier = cf.build();
    }
    
}
