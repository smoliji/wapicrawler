package machinelearning;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

/**
 *
 * @author smoliji4
 */
public abstract class Set {
    
    protected Instances instances;
    
    public Instances getInstances() {
        return instances;
    }
    
    /**
     * Writes instances to ARFF File
     * @param file 
     */
    public void writeToFile(File file) {
        try {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(instances);
            saver.setFile(file);
            saver.writeBatch();
        } catch (IOException ex) {
            Logger.getLogger(TrainingSet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Joins instances a to instances b.
     * @param a 
     * @param b
     * @return 
     */
    public static Instances joinTo(Instances a, Instances b) {
        for (int i = 0; i < a.numInstances(); i++) {
            b.add(a.instance(i));
        }
        return a;
    }
    
}
