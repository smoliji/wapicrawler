package machinelearning;

import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 *
 * @author smoliji4
 */
public class WekaExample {
    
    public WekaExample() throws Exception {
        
        //init data sources from arff files
        DataSource trainData = new DataSource("TEST-yt1-train.arff");    
        DataSource testData = new DataSource("TEST-yt1-test.arff");
        
        //create instances from data source
        Instances train = trainData.getDataSet();
        Instances test = testData.getDataSet();
        
        System.out.println(train.instance(0).attribute(1));
        
        //set index of class (in this case, only 2 are present and 
        //1. is document itself in one string and 2. is class
        train.setClassIndex(1);
        test.setClassIndex(1);
        
        //initialize filter and it's options
        StringToWordVector filter = new StringToWordVector();
        String[] options = {
            "-R", "first-last", 
            "-W", "1000", 
            "-prune-rate", "-1.0",
            "-N", "0",
            "-stemmer", "weka.core.stemmers.NullStemmer",
            "-M", "1",
            "-tokenizer", "weka.core.tokenizers.WordTokenizer",
            "-delimiters", "\" \\r\\n\\t.,;:\\\'\\\"()?!\"",
        };
        filter.setOptions(options);
        
        //initialize classifier
        J48 j48 = new J48();
        j48.setConfidenceFactor(0.25f);
        j48.setMinNumObj(2);
        
        //Use filtered classifier > required for on-the fly filtering and 
        //classification
        FilteredClassifier fc = new FilteredClassifier();
        fc.setFilter(filter);
        fc.setClassifier(j48);
        
        //train classifier on training set
        fc.buildClassifier(train); 
        
        //test each instance
        for (int i = 0; i < test.numInstances(); i++) {            
            //predicted most likely class index
            double pred = fc.classifyInstance(test.instance(i));            
            //instance id
            double instId = test.instance(i).value(0);
            //actual class
            String actual = test.instance(i).stringValue(1);
            //predicted class
            String predicted = test.classAttribute().value((int) pred);
            //probability that instance belongs to class 
            double prob = fc.distributionForInstance(test.instance(i))[(int)pred];
            
            System.out.print("ID: " + instId);
            System.out.print(", actual: " + actual);
            System.out.println(", predicted: " + predicted + " " + prob);
        }
        
        Evaluation eval = new Evaluation(train);
        eval.evaluateModel(fc, train);
        //System.out.println(eval.toSummaryString("\nResults\n======\n", false));
        
        System.out.println(train.toString());
    }
    
}
