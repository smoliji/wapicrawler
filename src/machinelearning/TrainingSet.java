package machinelearning;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import wapicrawler.Configuration;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Instance;

/**
 *
 * @author smoliji4
 */
public class TrainingSet extends Set {
    
    private static final String YES = "yes";
    private static final String NO = "no";
    
    private static final String TRAINING_SET = "trainingSet";

    public TrainingSet() {
        String tSet = Configuration.getInstance().get(TRAINING_SET);
        if (new File(tSet).isDirectory()) {
            fromDir();
        }
        else {
            fromArff(tSet);
        }
    }
    
    public final void fromDir() {
        String dirPath = Configuration.getInstance().get(TRAINING_SET);
        File dir = new File(dirPath);   
        instances = DataModel.createInstances("trainingSet", 200);        
        Instance[] yes = instancesFromDir(dir.getAbsolutePath() + File.separator + YES); 
        Instance[] no = instancesFromDir(dir.getAbsolutePath() + File.separator + NO);
        for (Instance i: yes) {
            instances.add(i);
        }
        for (Instance i: no) {
            instances.add(i);
        }
    }
    
    /**
     * Creates Instances from directory and assigns class corresponding
     * with directory name. Files are sorted by name
     * @param dir
     * @return Array of created instances 
     */
    private Instance[] instancesFromDir(String dir) {
        File d = new File(dir);
        Instance[] o = new Instance[d.listFiles().length];
        File[] files = d.listFiles();
        Arrays.sort(files);
        int i = 0;
        for (File f: files) {
            try {
                String content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));                
                o[i] = DataModel.createInstance(content.replace("\n", " "));                   
                o[i].setDataset(instances);
                o[i].setClassValue(d.getName());
                i++;
            } catch (IOException ex) {
                Logger.getLogger(TrainingSet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return o;
    }
    
    private void fromArff(String arff) {
        try {
            DataSource source = new DataSource(arff);
            instances = source.getDataSet();
            instances.setClassIndex(instances.numAttributes() - 1);
        } catch (Exception ex) {
            Logger.getLogger(TrainingSet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
