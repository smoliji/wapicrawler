package machinelearning;

import java.util.logging.Level;
import java.util.logging.Logger;
import wapicrawler.Configuration;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.functions.SMO;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.meta.LogitBoost;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.NBTree;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.RandomTree;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 * @author smoliji4
 */
public class ClassifierFactory {
    
    public static final String CLASSIFIER = "classifier";
    public static final String CLASSIFIER_OPTIONS = "classifier.parameters";
    
    public Classifier build() {
        String classifierName = Configuration.getInstance().get(CLASSIFIER);
        String type = Configuration.getInstance().get(DataModel.MODEL);
        Classifier o;
        if (type.equals(DataModel.DOCUMENT_SPEC)) {            
            o = buildFiltered(classifierName);
        }
        else {
            o = build(classifierName);
        }        
        return o;
    }
   
    /**
     * Creates classifier from name and sets its options.
     * 
     * Supported algorithms:
     *  j48
     *  naiveBayes
     *  SVM
     *  RandomForest
     *  SMO
     *  NBTree
     *  RandomTree
     *  LogitBoost
     *  DecisionTable
     * 
     * @param clName
     * @return Correct classifier.
     */
    public Classifier build(String clName) {
        
        Classifier c = null;
        
        c = clName.equalsIgnoreCase("j48") ? new J48(): 
            clName.equalsIgnoreCase("naiveBayes")? new NaiveBayes():
            clName.equalsIgnoreCase("svm")? new LibSVM():
            clName.equalsIgnoreCase("smo")? new SMO():
            clName.equalsIgnoreCase("randomForest")? new RandomForest():
            clName.equalsIgnoreCase("NBTree")? new NBTree()://slooooooow
            clName.equalsIgnoreCase("logistic")? new Logistic()://slooow
            clName.equalsIgnoreCase("randomTree")? new RandomTree():
            clName.equalsIgnoreCase("decisionTable")? new DecisionTable():
            clName.equalsIgnoreCase("logitBoost")? new LogitBoost():
            clName.equalsIgnoreCase("mlp")? new MultilayerPerceptron()://slooooo--..
            null;
        
        if (null != c) {
            setOptions(c, getClassifierOptions());
        } else {
            System.err.println("No classifier set.");
        }
        return c;
    }
    
    public FilteredClassifier buildFiltered(String name) {
        try {
            //initialize filter and it's options
            StringToWordVector filter = new StringToWordVector();
            String[] options = {
                "-R", "first-last",
                "-W", "1000",
                "-prune-rate", "-1.0",
                "-N", "0",
                "-stemmer", "weka.core.stemmers.NullStemmer",
                "-M", "1",
                "-tokenizer", "weka.core.tokenizers.WordTokenizer",
                "-delimiters", "\" \\r\\n\\t.,;:\\\'\\\"()?!\"",
            };
            filter.setOptions(options);
            
            //initialize classifier
            Classifier classificationAlg = build(name);
            
            //Use filtered classifier > required for on-the fly filtering and
            //classification
            FilteredClassifier fc = new FilteredClassifier();
            fc.setFilter(filter);
            fc.setClassifier(classificationAlg);
            
            return fc;
        } catch (Exception ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void setOptions(Classifier classifier, String[] options) {
        try {
            classifier.setOptions(options);
        } catch (Exception ex) {
            System.err.println("Arguments passed to classifier are not valid. Application will now exit");
            Logger.getLogger(ClassifierFactory.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
    }
    
    private String[] getClassifierOptions() {
        return Configuration.getInstance().get(CLASSIFIER_OPTIONS).split("\\s+");
    }
    
}
