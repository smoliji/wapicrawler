package machinelearning;

import java.io.File;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import solr.SDoc;
import solr.SolrQuery;
import solr.SolrResponse;
import solr.SolrPaginator;

/**
 *
 * @author smoliji4
 */
public class SolrSet extends Set {
    
    private final String solrUrl;
    
    private String[] ids;
    
    public SolrSet(String solr) {
        solrUrl = solr;
        fetchInstances();
    }
    
    public final void fetchInstances() {
        SolrResponse r;
        String query = "*:*";
        SolrPaginator paginator = new SolrPaginator(query);
        int docCount = new SolrQuery().setQ(query).select().response.numFound;        
        
        instances = DataModel.createInstances("solr", docCount);
        ids = new String[docCount];
        
        int cnt = 0;
        while (null != (r = paginator.get())) {
            for (SDoc doc: r.response.docs){
                instances.add(DataModel.createInstance(doc.content));
                ids[cnt] = doc.url;
                cnt++;
            }
        }
    }
    
    public String[] getIds() {
        return ids;
    }
    
    public String instanceId(int index) {
        return ids[index];
    }

    @Override
    public void writeToFile(File file) {
        PrintWriter writer = null;    ;
        super.writeToFile(file);
        try {
            writer = new PrintWriter(file + "-ids", "UTF-8");
            for (String url: ids) {
                writer.println(url);
            }
            writer.close();
        } catch (Exception ex) {
            Logger.getLogger(SolrSet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }
    
}
