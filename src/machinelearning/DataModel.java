package machinelearning;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import wapicrawler.Configuration;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author smoliji4
 */
public class DataModel {

    public static final String MODEL = "model";

    public static final String DOCUMENT_SPEC = "document";

    private static DataModel instance = new DataModel();
    
    private static int kwThreshold = Integer.parseInt(Configuration.getInstance().get("threshold"));

    private final HashMap<String, Attribute> attrWordsMap;
    private final Attribute attrDocument;
    private final FastVector attrWords;
    private final Attribute attrClass;

    private DataModel() {
        attrWordsMap = new HashMap<>();
        attrDocument = createDocumentAttribute();
        attrWords = createKeywordAttributes();
        attrClass = createClassAttribute();
    }

    /**
     * Creates instance according to configuration.
     *
     * @param content Content of the document.
     * @return Returns created Instance.
     */
    public static Instance createInstance(String content) {
        String modelCfg = Configuration.getInstance().get(MODEL);
        if (modelCfg.equals(DOCUMENT_SPEC)) {
            return instance.createDocumentInstance(content);
        } else {
            String[] keywords = modelCfg.split(",");
            return instance.createKeywordsInstance(content, keywords);
        }
    }

    /**
     * Creates Document instance. arff example: "This is document contents."
     *
     * @param content
     * @return
     */
    private Instance createDocumentInstance(String content) {
        Instance inst = new Instance(2);
        inst.setValue(instance.attrDocument, content.replaceAll("\\s+", " ").replaceAll("'", ""));
        return inst;
    }

    /**
     * Creates instance based on keywords contained in document content. arff
     * example: y,n,y,y
     *
     * @param content
     * @param keywords
     * @return
     */
    private Instance createKeywordsInstance(String content, String[] keywords) {
        Instance inst = new Instance(attrWords.size() + 1);
        for (String word : keywords) {
            int cnt = substrInStr(content, word, kwThreshold);
            String attrVal = cnt == kwThreshold ? "y" : "n";
            inst.setValue(attrWordsMap.get(word), attrVal);
        }
        return inst;
    }

    /**
     * Finds number of occurences of a substring in a string. Max 
     * occurences are limited by max param.
     * @param haystack
     * @param needle
     * @param max
     * @return Number of occurences
     */
    public static int substrInStr(String haystack, String needle, int max) {
        Pattern p = Pattern.compile(String.format("\\b%s\\b", needle), Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(haystack);
        int count = 0;
        while (m.find() && count < max) {
            count += 1;
        }
        return count;
    }

    /**
     * Creates Instances according to configuration.
     *
     * @param name Name of instances.
     * @param cap Initial capacity of Instances.
     * @return Instances
     */
    public static Instances createInstances(String name, int cap) {
        Instances o = new Instances(name, instance.attributes(), cap);
        o.setClassIndex(o.numAttributes() - 1);
        return o;
    }

    /**
     * @return Returns attributes according to configuration schema.
     */
    private FastVector attributes() {
        FastVector attributes;
        String modelCfg = Configuration.getInstance().get(MODEL);
        if (modelCfg.equals(DOCUMENT_SPEC)) {
            attributes = new FastVector(1 + 1);
            attributes.addElement(instance.attrDocument);
        } else {
            String[] keywords = modelCfg.split(",");
            attributes = new FastVector(keywords.length + 1);
            for (String key : instance.attrWordsMap.keySet()) {
                attributes.addElement(instance.attrWordsMap.get(key));
            }
        }
        attributes.addElement(instance.attrClass);
        return attributes;
    }

    private FastVector createKeywordAttributes() {
        String[] keywords = Configuration.getInstance().get(MODEL).split(",");
        FastVector attributes = new FastVector(keywords.length + 1);
        for (String w : keywords) {
            FastVector fvVals = new FastVector(2);
            fvVals.addElement("y");
            fvVals.addElement("n");
            Attribute attr = new Attribute(w, fvVals);
            attributes.addElement(attr);
            attrWordsMap.put(w, attr);
        }
        return attributes;
    }

    /**
     * @return Returns Document attribute
     */
    private Attribute createDocumentAttribute() {
        return new Attribute("Document", (FastVector) null);
    }

    /**
     * @return Returns class atribute.
     */
    private Attribute createClassAttribute() {
        FastVector fvClassVal = new FastVector(2);
        fvClassVal.addElement("yes");
        fvClassVal.addElement("no");
        Attribute classAttribute = new Attribute("apiClass", fvClassVal);
        return classAttribute;
    }

}
