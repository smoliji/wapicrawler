package wapicrawler;

import java.io.File;

/**
 *
 * @author smoliji4
 */
public class IndexingProcess extends NutchProcess {

    private String crawled;
    private String scriptPath;
    private String crawldb;
    private String linkdb;
    private String segments;

    public IndexingProcess(String crawled, String nutchHome, String solrAddr) {
        super(nutchHome, solrAddr);
        this.crawled = crawled;
        scriptPath = nutchHome + File.separator + "bin" + File.separator + "nutch";
        crawldb = crawled + File.separator + "crawldb";
        linkdb = crawled + File.separator + "linkdb";
        segments = crawled + File.separator + "segments" + File.separator + "*";
    }
    
    public void start() {   
        System.out.println("Starting Solr indexing (all segments): " + crawled );
        super.start(
                scriptPath,
                "solrindex",
                solrAddr,
                crawldb,
                "-linkdb",
                linkdb,
                segments
                );
        System.out.println("Solr indexing finished.");
    }
    
}
