package wapicrawler;

import cse.CseCommunicator;
import cse.SearchResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author smoliji4
 */
public class Seeds {
    
    private final int n;
    private final String term;
    private final SearchResult[] feeds;
    private File feedsFile = null;
    
    public Seeds(int n, String term) {
        this.n = n;
        this.term = term;
        feeds = CseCommunicator.topN(n, term);
    }
    
    public File writeToFile(String fileName) {
        feedsFile = new File(fileName);
        try {
            PrintWriter pw = new PrintWriter(feedsFile);
            for (SearchResult sr: feeds) {
                pw.println(sr.link);
            }
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Seeds.class.getName()).log(Level.SEVERE, null, ex);
        }
        return feedsFile;
    }
    
    public File getFeedsFile() {
        return feedsFile;
    }
    
}
