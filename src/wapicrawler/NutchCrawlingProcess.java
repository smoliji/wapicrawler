package wapicrawler;

import java.io.File;

/**
 *
 * @author smoliji4
 */
public class NutchCrawlingProcess extends NutchProcess {
    
    private final int depth;
    private final String destPath;
    private final String seeds;
    private final String scriptPath;

    public NutchCrawlingProcess(String nutchHome, String solrAddr, int depth, String destPath, String seeds) {
        super(nutchHome, solrAddr);
        this.depth = depth;
        this.destPath = destPath;
        this.seeds = seeds;
        scriptPath = nutchHome + File.separator + "bin" + File.separator + "crawl";
    }
    
    public void start() {
        System.out.printf("Starting Apache Nutch\n"
                        + "  Script: %s\n"
                        + "  Seeds: %s\n"
                        + "  Crawl dir: %s\n"
                        + "  Solr: %s\n"
                        + "  Depth: %d\n",
            scriptPath,
            seeds,
            destPath,
            solrAddr,
            depth);
        super.start(scriptPath, seeds, destPath, solrAddr, Integer.toString(depth));
    }
    
}
