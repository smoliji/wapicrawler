package wapicrawler;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains crawler configuration.
 * 
 * @author smoliji4
 */
public class Configuration {
    
    private static final String CONF_FILE = "crawler.properties";
    
    private Properties props;    
    
    private static final Configuration instance = new Configuration();
    
    private Configuration(){
        
        props = new Properties();
        
        InputStream is = null;
        
        try {
            
            is = new FileInputStream(CONF_FILE);
            
            props.load(is);
            
        } catch (IOException ex) {
            System.err.println(ex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        }
    }
    
    public static Configuration getInstance() {
        return instance;
    }
    
    public String get(String key) {
        try {
            String o = props.getProperty(key);
            return o;
        } catch (Exception ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void put(String key, String value) {
        props.put(key, value);
    }
    
}
