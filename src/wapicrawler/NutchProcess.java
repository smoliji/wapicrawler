package wapicrawler;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author smoliji4
 */
public class NutchProcess {
    
    protected final String nutchHome;
    protected final String solrAddr;
    
    private Process p = null;

    public NutchProcess(String nutchHome, String solrAddr) {
        this.nutchHome = nutchHome;
        this.solrAddr = solrAddr;
        
    }
    
    public void start(String... args) {
        ProcessBuilder pb;
        System.out.println("");
        pb = new ProcessBuilder(args);
        pb.redirectOutput(Redirect.INHERIT);
        try {
            p = pb.start();
            try {
                p.waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(NutchProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage()+". Application will now exit.");
            System.exit(1);
        } finally {
            try {
                p.waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(NutchProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Process " + args[0] + " finished.");
    }
    

    
}
