package wapicrawler;

import java.io.File;
import machinelearning.InstanceEvaluation;
import machinelearning.Model;
import machinelearning.Set;
import machinelearning.SolrSet;
import machinelearning.TrainingSet;
import solr.Solr;

/**
 *
 * @author smoliji4
 */
public class Crawler {

    public final String CRAWL_DEST = "wapicrawl";
    public final String SEEDS = "seeds.txt";    

    public final String nutchHome;
    public final String solr;

    private int crawlingDepth = 5;
    private int cseResultsCnt = 10;
    private File seedsFile;
    
    private NutchCrawlingProcess nutchCrawlingProcess;
    private IndexingProcess indexingProcess;

    public Crawler(String nutch, String solr) {
        nutchHome = nutch;
        this.solr = solr;
    }

    public void crawl(String term) {
        clearIndex();
        supplySeeds(cseResultsCnt, term);
        runNutchCrawl(term);
        runIndexing(CRAWL_DEST + File.separator + term);        
    }
    
    public void assignApiClass() {
        
        Model m = new Model();
        Set trainingSet = new TrainingSet();
        m.train(trainingSet.getInstances());
        SolrSet test = new SolrSet(solr);
        m.setTestSet(test.getInstances());
        InstanceEvaluation e = null;  
        
        for (int i = 0; i < m.getTestSet().numInstances(); i++) {
            try {
                e = m.evalInstance(i);
            } catch (Exception ex) {
                System.err.printf("Failed evaluating instance %d.\n", i);
            }
            Solr.update(new String[]{"id"}, new String[]{test.instanceId(i)},
                        new String[]{"api-class", "api-prob"},                         
                        new String[]{e.predCls, Double.toString(e.prob)},
                        false);            
        } 
        Solr.commit();
    }

    private void supplySeeds(int n, String term) {
        Seeds f = new Seeds(n, term);
        seedsFile = f.writeToFile(SEEDS);
        seedsFile = new File(SEEDS);
    }

    private void runNutchCrawl(String term) {
        String dest = CRAWL_DEST + File.separator + term;
        File destFile = new File(dest);
        destFile.mkdirs();
        nutchCrawlingProcess = new NutchCrawlingProcess(
                nutchHome,
                solr,
                crawlingDepth,
                dest,
                seedsFile.getAbsolutePath());
        nutchCrawlingProcess.start();        
    }
    
    private void runIndexing(String dirname) {
        indexingProcess = new IndexingProcess(dirname, nutchHome, solr);
        indexingProcess.start();
    }
    
    public void clearIndex() {
        Solr.delete("*:*", true);
    }

    public int getCseResultsCnt() {
        return cseResultsCnt;
    }

    public void setCseResultsCnt(int cseResultsCnt) {
        this.cseResultsCnt = cseResultsCnt;
    }

    public int getCrawlingDepth() {
        return crawlingDepth;
    }

    public void setCrawlingDepth(int crawlingDepth) {
        this.crawlingDepth = crawlingDepth;
    }

}
