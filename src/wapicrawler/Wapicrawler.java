package wapicrawler;

import solr.Solr;

/**
 *
 * @author smoliji4
 */
public class Wapicrawler {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {     
        
        String query = null;
        
        if (args.length == 0) {
            System.err.println("Usage: wapicrawler.jar query");
            return;
        }
        
        query = args[0];
        
        Configuration conf = Configuration.getInstance();        
        Crawler crawler = new Crawler(conf.get("nutch.home"), conf.get(Solr.SOLR_URL));
        crawler.setCrawlingDepth(Integer.parseInt(conf.get("depth")));
        crawler.setCseResultsCnt(Integer.parseInt(conf.get("num")));        
        crawler.crawl(query);
        crawler.assignApiClass();
        
    }
}
