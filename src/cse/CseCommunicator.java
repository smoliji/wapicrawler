package cse;

import wapicrawler.Configuration;
import java.util.ArrayList;

/**
 * Manages communication with Google Custom Search Engine
 * 
 * @author smoliji4
 */
public class CseCommunicator {
    
    public static SearchResult[] topN (int n, String keyword) {
        ArrayList<SearchResult> list = new ArrayList<>();        
        
        int perResponse = 10;
        int round = 0;
        
        System.out.printf("Sending request for top %d to Google CSE (CX:%s) for keyword \"%s\".",
                    n, Configuration.getInstance().get(CSE.CSE_ID), keyword);
        do {
            Response r = new Request()
                    .setNum(10)
                    .setStart(round*perResponse + 1)
                    .get(keyword);            
            
            SearchResult[] results = r.items;
            if (results == null) {
                System.err.println("There was an error communicating Google CSE.");
                break;
            }
            if (results.length == 0) {
                System.err.printf("No search results for term \"%s\" found by "
                        + "Google CSE (CX:%s). Application will now exit.\n",
                        keyword, Configuration.getInstance().get(CSE.CSE_ID));
                System.exit(0);
            }
            for (SearchResult result: results) {
                list.add(result);
                System.out.println(result.link);
                if (list.size() >= n) {
                    break;
                }
            }
            if (null == r.NextStartIndex) {
                System.out.println("All results from CSE already fetched.");
                break;
            }
            
            round++;
        } while (list.size() < n);
                
        return list.toArray(new SearchResult[list.size()]);
    }
    
}
