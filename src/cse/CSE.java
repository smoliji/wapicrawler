package cse;

/**
 * Custom Search Engine parameters
 *
 * @author smoliji4
 */
public interface CSE {
    /**
     * API key for public access to Google Services
     */
    String API_KEY = "cse.api-key";
    /**
     * CSE Id
     */
    String CSE_ID = "cse.cx";
    
}
