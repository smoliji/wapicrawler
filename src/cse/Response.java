package cse;

import org.json.*;

/**
 *
 * @author smoliji4
 */
public final class Response {
    
    public int code;
    
    private JSONObject response;
    
    public Integer NextStartIndex = null;
    public Integer PrevStartIndex = null;
    public SearchResult[] items = null;
    
    public Response(int code, String jsonText) {
        
        this.code = code;
        
        try {
        response = new JSONObject(jsonText);
        } catch (JSONException e) {
            System.out.println(jsonText);
            System.exit(1);
        }
        
        int totalResults = response
                .getJSONObject("searchInformation")
                .getInt("totalResults");
        
        if (totalResults == 0) {
            items = new SearchResult[0];
            return;
        }
        
        JSONObject queries = response.getJSONObject("queries");
        try {
        PrevStartIndex = queries
                .getJSONArray("previousPage")
                .getJSONObject(0)
                .getInt("startIndex");
        } catch(JSONException e) {}
        try {
        NextStartIndex = queries
                .getJSONArray("nextPage")
                .getJSONObject(0)
                .getInt("startIndex");
        } catch(JSONException e) {}
        
        
        JSONArray jItems= response.getJSONArray("items");
        int count = jItems.length();
        items = new SearchResult[count];
        for (int i = 0; i < count; i++) {
            JSONObject jItem = jItems.getJSONObject(i);
            items[i] = new SearchResult(
                    jItem.getString("title"), 
                    jItem.getString("link"),
                    jItem.getString("displayLink"),
                    jItem.getString("snippet")
            );
        }
        
    }
    
}
