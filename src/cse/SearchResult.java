package cse;

/**
 *
 * @author smoliji4
 */
public final class SearchResult {
    
    public String title;
    public String link;
    public String displayLink;
    public String snippet;
    
    public SearchResult(String title, String link, String displayLink, String snippet) {
        this.title = title;
        this.link = link;
        this.displayLink = displayLink;
        this.snippet = snippet;
    }
    
    public String toString() {
        return link;
    }
    
}
