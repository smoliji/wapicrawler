package cse;

import wapicrawler.Configuration;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
/**
 *
 * @author smoliji4
 */
public class Request {
    
    public static final String GOOGLE_API = "https://www.googleapis.com/customsearch/v1?";
    
    /**
     * Number of search results to return [1;10]
     */
    private int num = 10;
    /**
     * The index of the first result to return
     */
    private int start = 1;
    
    /**
     * Sends request to CSE
     * 
     * @param q Search query (i.e. keyword)
     * @return 
     */
    public Response get(String q) {
        URL url;
        HttpURLConnection conn;
        BufferedReader br;
        StringBuilder sb = new StringBuilder();
        String line;
        Response response = null;
        Configuration conf = Configuration.getInstance();
        
        try {
            url = new URL(GOOGLE_API 
                    + "key=" + conf.get(CSE.API_KEY)
                    + "&cx=" + conf.get(CSE.CSE_ID)
                    + "&q="  + URLEncoder.encode(q, "UTF-8")
                    + "&num="+ num
                    + "&start="+ start
            );
            
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");            
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));            
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            
            response = new Response(conn.getResponseCode(), sb.toString());
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return response;
    }
    
    public Request setNum(int n) {
        if (n < 1 || n > 10) {
            System.err.println("Parameter num out of range [1;10]. Using default value: " + num);
            return this;
        }
        num = n;
        return this;
    }
    
    public Request setStart(int i) {
        if (i < 0) {
            System.err.println("Parameter start must be > 0. Using default value: " + start );
            return this;
        }
        start = i;
        return this;
    }
    
}
