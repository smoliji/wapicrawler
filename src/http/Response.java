package http;

/**
 *
 * @author smoliji4
 */
public final class Response {
    
    public int code;
    public String content;
    
    public Response() {
        code = -1;
        content = "";
    }
    
    public Response(int code, String content) {
        this.code = code;
        this.content = content;
    }
    
    public String toString() {
        return "["+code+"]"+content;
    }
}
