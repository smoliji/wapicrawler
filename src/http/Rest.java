package http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author smoliji4
 */
public class Rest {
    
    public static Response get(String urlString) {
        URL url;
        HttpURLConnection conn;
        BufferedReader br;
        StringBuilder sb = new StringBuilder();
        String line;
        Response response = new Response();
        
        try {
            url = new URL(urlString);
            
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");            
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));            
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            
            response = new Response(conn.getResponseCode(), sb.toString());
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return response;
    }
    
    public static Response post(String urlString, String data, String contentType) {
        Response response = new Response();
        URL url;
        HttpURLConnection conn;
        StringBuilder sb = new StringBuilder();
        BufferedReader br;
        String line;
        try {
            url = new URL(urlString);
            
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");   
            conn.setRequestProperty("Content-Type", contentType);
            conn.setDoOutput(true);
            
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.write(data.getBytes());
            out.flush();
            out.close();
            
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));            
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            
            response = new Response(conn.getResponseCode(), sb.toString());
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return response;
    }
    
    public static Response postXml(String url, String data) {
        return post(url, data, "text/xml");
    }
    
}
