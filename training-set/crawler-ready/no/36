Twilio PRODUCTS  PRICING  SOLUTIONS  API & DOCS  HELP SIGNUP LOGIN
TWILIO DOCS
Quickstart
 
How-Tos
 
Helper Libraries
 
API Docs
 
Security
Quickstart Tutorials
How-Tos and Examples
Helper Libraries
TwiML Reference
REST API Reference
SIP Reference
Elastic SIP Trunking Reference
Network Traversal Service Reference
Client Reference
Twilio.js
Twilio.Device Singleton
Twilio.Connection Object
Changelog
Twilio Client iOS SDK
TwilioClient
TCConnection
TCConnectionDelegate
TCDevice
TCDeviceDelegate
TCPresenceEvent
Changelog
Twilio Client Android SDK
Connection
ConnectionListener
Device
DeviceListener
PresenceEvent
Twilio
Changelog
Capability Tokens
Error Codes
Connect Reference
Debugging Errors
Security
Availability & Reliability
Learning Resources
Twilio Client iOS SDK

Overview

Want your iPad, iPhone, or iPod touch app to make and receive calls? Want to add voice-chat support to your iOS game for players to mock each other? There's an SDK for that!

Twilio Client iOS is an Objective-C library for iOS that enables voice communications with landlines or other Twilio Client devices, including web browsers and other mobile devices.

Download the iOS SDK

The easiest way to incorporate TwilioSDK to your project is using CocoaPods. Simply add the following line to your Podfile:

pod 'TwilioSDK', '~>1.2'

Alternatively, you may download the tar.bz2 file (27 MB).

Architecture

There are three major pieces in a Twilio Client app:

Your iOS app that uses the libTwilioClient.a library
A server to grant capabilities to your Client app as well as to serve up TwiML and/or make Twilio REST API calls
Twilio’s cloud services to handle the telephony and process TwiML and/or REST API calls
Twilio Client iOS SDK Architecture

Client-side Classes

The primary class for connecting to Twilio services from your app is TCDevice. This class coordinates service authorization with Twilio, listens for incoming connections, and establishes outgoing connections. An instance of this class is created using a "capability token", described in the next section.

Connections to Twilio, either incoming or outgoing, are represented by instances of the class TCConnection.

In addition, status callbacks are provided to objects that implement the delegate protocols TCDeviceDelegate and TCConnectionDelegate.

Server-side Security: Capability Tokens

The Twilio Client SDK uses a capability token to sign communications from your iOS app to Twilio. These tokens are created by you on your server and allow you to specify what capabilities are going to be available to your app, such as whether it can receive incoming connections, make outgoing connections, etc. These tokens always have an expiration, which means all tokens have a limited lifetime to protect you from abuse. It is up to you to determine how often these tokens must be refreshed.

Twilio capability tokens are based on the JSON Web Token standard. They can be generated with our helper libraries that come in a variety of languages.

For the security of your Twilio account, you should not embed a Capability Token or your Twilio Auth Token as strings in the app you submit to the App Store.
Class Reference

TwilioClient
TCConnection
TCDevice
TCPresenceEvent
Protocol Reference

TCConnectionDelegate
TCDeviceDelegate
COMPANY
About Us
Team
Jobs
PRODUCTS
Voice
Client
Messaging
RESOURCES
API & Docs
Security
International
Open Source
COMMUNITY
Twilio Blog
Engineering Blog
Twilio.org
SOLUTIONS
Elements
Showcase
Partners
DOers
SUPPORT
Help Center
Status
Talk to Sales
© 2009 - 2015 Twilio, Inc. All rights reserved. Patents Pending. Terms of Service | Privacy Policy
