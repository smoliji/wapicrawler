WordPress.org

Showcase
Themes
Plugins
Mobile
Support
Get Involved
About
Blog
Hosting
Download WordPress
Codex
Codex tools: Log in
Attention Interested in functions, hooks, classes, or methods? Check out the new WordPress Code Reference!
Search Engine Optimization for WordPress

Languages: English • 日本語 • ไทย • Türkçe • (Add your language)

Contents

1 Search Engine Site Submissions
2 Search Engine Optimization Resources
2.1 Meta Tags
2.2 Robots.txt Optimization
2.2.1 See also:
2.3 Feed Submissions
2.4 Technorati Tags
2.5 Permalinks
2.6 Sitemaps
2.7 Google Sitemaps
2.8 Link Relationships
3 More Resources and Tutorials
4 Translations
WordPress, straight out of the box, comes ready to embrace search engines. Its features and functions guide a search engine through the posts, pages, and categories to help the search engine crawl your site and gather the information it needs to include your site within its database.

WordPress comes with several built in search optimization tools, including the ability to use .htaccess to create apparently static URLs called permalinks, blogrolling, and pinging. There are also a number of third party plugins and hacks which can be used for search engine optimization (SEO).

However, once you start using various WordPress Themes and customizing WordPress to meet your own needs, you may break some of those useful search engine friendly features. To maintain your WordPress site's optimal friendliness towards search engine spiders and crawlers, here are a few tips:

Good, Clean Code 
Make sure your site's code validates. Errors in your code may prevent a search engine from moving through the site successfully.
Content Talks 
Search engines can't "see" a site. They can only "read" a site. Pretty does not talk to a search engine. What "talks" to a search engine are the words, the content, the material in your site that explains, shares, informs, educates, and babbles. Make sure you have quality word content for a search engine to examine and compare with all the parts and pieces to give you a good "score".
Write Your Content with Searchers in Mind 
How do you find information on the Internet? If you are writing something that you want to be "found" on the Internet, think about the words and phrases someone would use to find your information. Use them more than once as you write, but not in every sentence. Learn how search engines scan your content, evaluate it, and categorize it so you can help yourself get in good favor with search engines.
Content First 
A search engine enters your site and, for the most part, ignores the styles and CSS. It just plows through the site gathering content and information. Most WordPress Themes are designed with the content as close to the top of the unstyled page as possible, keeping sidebars and footers towards the bottom. Few search engines scan more than the first third of the page before moving on. Make sure your Theme puts the content near the top.
Keywords, Links, and Titles Meet Content 
Search engines do not evaluate your site on how pretty it is, but they do evaluate the words and put them through a sifter, giving credit to certain words and combinations of words. Words found within your document are compared to words found within your links and titles. The more that match, the better your "score."
Content in Links and Images 
Your site may not have much text, mostly photographs and links, but you have places in which to add textual content. Search engines look for alt and title in link and image tags. While these have a bigger purpose of making your site more accessible, having good descriptions and words in these attributes helps provide more content for search engines to digest.
Link Popularity 
It is not how good your site is, it is how good the sites are that link to you. This still holds weight with search engine favoritism. It's about who links to you. Blogrolls, pingbacks, and trackbacks are all built into WordPress. These help you link to other people, which gives them credit, but it also helps them link to you, connecting the "links." The number of incoming links your site has that have been recognized by Google can be checked by typing link:www.yoursite.com into Google (other search engines have similar functions). Other ways to generate incomming links to your site include:
Add your site's url to your signature on forum posts on other sites.
Submit your site to directories (see below).
Note: Leaving comments on blogs will not help with this, since all modern blogging tools use the rel="nofollow" attribute. Don't be a comment spammer.
Good Navigation Links 
A search engine crawls through your site, moving from page to page. Good navigational links to the categories, archives, and various pages on your site will invite a search engine to move gracefully from one page to another, following the connecting links and visiting most of your site.
Search Engine Site Submissions
There are many resources that will "help" you submit your site to search engines. Some are free, some for a fee. Or you can manually submit your site to search engines yourself. Whatever method you choose to use, once your site has been checked for errors and is ready to go, search engines will welcome your WordPress site.

Here are some tips for successful site submissions:

Make sure you have content for search engines to scan. In general, have more than 10 posts on your site to give the search engines something to examine and evaluate.
Do not submit your site to the same search engine more than once a month or longer, depending upon their criteria, not your anxiousness to be listed.
Have ready to type, or copy and paste, the title of the site, and the categories your site may belong to in a search engine directory.
Have a list of your website's various "addresses/URLs" ready. You can submit your root directory as well as specific categories and feeds to search engines, expanding your search engine coverage.
Keep a list of the various search engines and directories you submit to so you do not accidentally resubmit too soon, and you can keep track of how they include you among their pages and results.
Directory Sites

It is also useful for traffic generation and search optimization purposes to submit your site to directories. Both comprehensive directory sites and those specific to the subject or localisation of your site can be used.

DMOZ.org this is the most important directory. Its content is licensed in an open fashion, allowing it to be syndicated through out the web. Its content is also used directly in some fashion by almost all of the major search engines.

Search Engine Optimization Resources
While WordPress comes ready for search engines, the following are more resources and information you may want to know about preparing and maintaining your site for search engines' robots and crawlers.

Meta Tags
Meta Tags contain information that describes your site's purpose, description, and keywords used within your site. The meta tags are stored within the head of your header.php template file. By default, they are not included in WordPress, but you can manually include them and the article on Meta Tags in WordPress takes you through the process of adding meta tags to your WordPress site.

The WordPress Custom Fields option can also be used to include keywords and descriptions for posts and Pages. There are also several WordPress Plugins that can also help you to add meta tags and keyword descriptions to your site found within the Official WordPress Plugin Directory.

Robots.txt Optimization
Search Engines read a yourserver.com/robots.txt file to get information on what they should and shouldn't be looking for, and where.

Specifying where search engines should look for content in high-quality directories or files you can increase the ranking of your site, and is recommended by Google and all the search engines.

An exampleWordPress robots.txt file:

Sitemap: http://www.example.com/sitemap.xml

# global
User-agent: *
Disallow: /cgi-bin/
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /wp-content/plugins/
Disallow: /wp-content/cache/
Disallow: /wp-content/themes/
Disallow: /trackback/
Disallow: /comments/
Disallow: */trackback/
Disallow: */comments/
Disallow: wp-login.php
Disallow: wp-signup.php
See also:

How Google Crawls My Site
How do I use a robots.txt file to control access to my site?
WordPress Robots.txt example and explanation from Yoast
Using robots.txt for SEO
Removing duplicate search engine content using robots.txt
The Web Robots Pages
wikipedia - Robots.txt
KB Robots.txt plugin for WordPress (Last updated 2007-8-14, WordPress 2.5 or below)
Feed Submissions
WordPress comes built-in with various feeds, allowing your site to be viewed by various feed readers. Many search engines are now accepting feed submissions, and there are many site which specialize in directories of feeds and feed services.

To submit your site's feeds, you need to know the link to the various feeds your site provides. The article WordPress Feeds lists the various links of the feeds that come built into WordPress.

For information on customizing these links, see the article on Customizing Feeds.

Pingomatic
MyPagerank
Feed Shark
Robin Good's RSSTop55 - Best Blog Directory And RSS Submission Sites
Ari Paparo's Big List of Blog Search Engines and Feed Services
Wordpress compressed all inclusive ping list
Technorati Tags
Technorati is a "real-time search engine that keeps track of what is going on in the blogosphere — the world of weblogs." According to the site, "Technorati tracks the number of links, and the perceived relevance of blogs, as well as the real-time nature of blogging. Because Technorati automatically receives notification from weblogs as soon as they are updated, it can track the thousands of updates per hour that occur in the blogosphere, and monitor the communities (who's linking to whom) underlying these conversations."

Technorati tags are used to categorize the different topics and information used by blogs. Technorati uses WordPress categories as tags automatically. You can add more tags by adding a rel="tag" to any link on your site. For example:

<a href="http://wordpress.org/" rel="tag">WordPress</a>
<a href="http://codex.wordpress.org/" rel="tag">Codex</a>
There are also several WordPress plugins for maximizing Technorati tags.

Note: In WordPress v1.5.x, Technorati will automatically recognize your category names as tags. For more info, see: WordPress Categories, Technorati Tags and Search Engine Optimisation

Note: For optimal Technorati listing, you should include the Atom feed in the header of your theme. For this you may use the following code, which you can add somewhere between the <head> and </head> tags:

<link rel="alternate" type="application/atom+xml" 
title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
Without adding the Atom feed to your header, your posts in Technorati will most likely be displayed wrongly. Which can result in the post content on Technorati duplicating the post title and listing other (meta) information, which is displayed between the post title and the real post content on your weblog.

Permalinks
Permalinks are enhancements to your existing URLs which can improve search engine optimization by presenting your post, page, and archive URLs as something like http://example.com/2003/05/23/my-cheese-sandwich/ rather than http://example.com/index.php?p=423. See Using Permalinks for more information.

As search engines use links and the title as part of their information gathering, links to posts and articles within your site gain importance with Permalinks.

See also:

Boost Search Engine Optimisation (SEO) using Permalinks
Permalinks Migration PluginWith this plugin, you can safely change your permalink structure without breaking the old links to your website.
As an added bonus, enable the Permalink Redirect plugin. This plugin "replies a 301 permanent redirect, if request URI is different from entry’s (or archive’s) permalink. It is used to ensure that there is only one URL associated with each blog entry."

Sitemaps
A sitemap or "site map" is a single page listing of all the posts on your website. It is intended for your visitors to get a good overview on what your site is about and to find a blog post quickly but it also has great benefits in the search engines as a good link is always pointing to all your blog posts. By having a link to your sitemap on all your sites pages both visitors and search engines will easily get to it and find all your posts.

Here is a tutorial with three different examples of sitemaps with demos and how to set them up:

Tutorial: Automatic Sitemap for WordPress
See also:

XML sitemaps and WordPress website optimization
Google Sitemaps
As of June 2005, Google is now accepting sitemaps of your site as part of their website submissions. Google needs to have this sitemap formatted in a special way using XML. You can find more information about Google's Sitemap Submissions from Google, and the discussion on the WordPress Forum about WordPress and Google Site maps.

Some utilities have been created to help the WordPress user to create a Google site map of their site for submission to Google. For more information on these and Google sitemaps:

Inside Google Sitemaps (Official Google Blog)
Google Sitemap Generator Plugin for WordPress
Make your sitemap human readable
Simple Sitemap Generator
Google Sitemaps - UltimateTagWarrior Tag Plugin Addon
Link Relationships
There is some debate over whether listing the link relations actually effect search engine ranking however it is simple to implement.

World Wide Web Consortium (W3C)
Linkrel Plugin
Link rel Plugin
Theme Code
More Resources and Tutorials
There is a lot to learn about search engine optimization and site submission. Here are just a few sites to help you learn more about how this works:

The Blogger's guide to SEO
Beginners Guide to Search Engine Optimization
WordPress SEO - The Definitive Guide to Higher Rankings for your Blog
WordPress SEO - Presentation by Joost de Valk
31 Ways to Boost The SEO of Your WordPress Site
SEO for WordPress Blogs - The Complete Guide
More WordPress Codex Articles on Website Development
All About Search Indexing Robots and Spiders
Boost Search Engine Optimization (SEO) Using Permalinks
Search Engine Optimized WordPress Themes
Getting Ready for Search Engine Submissions
Search Engine Robots that Search Your Site
Understanding Search Results Pages
On-Page SEO: Anatomy of a Perfectly Optimized Page
Website Development Step-by-Step
SEO For Wordpress Tips
Wordpress search engine optimization without code modification
Google to Yahoo MSN Sitemaps Creator Tool
Search Engine Optimization plugins for WordPress
How Can I Drive More Traffic to My blog? (13 tips)
The Important SEO Elements of a Web Page
Two Part Tutorial on SEO using the HeadSpace Plugin for WordPress
Wordpress Silo Site Structure
Blog SEO - Whitehat SEO for Bloggers in a Nutshell
WordPress SEO techniques - Page sculpting
Performing Effective Keyword Research
Translations
If you have translated this article or have some similar one on your blog post a link here. Please mark Full Translated articles with (t) and similar ones with (s).

WordPress için Arama Motoru Optimizasyonu (SEO) - (Türkçe) (t)
SøgeMaskineOptimering (SEO) for WordPress (t)
Ye werdpres diregetsochin mashashal asfelaginet - (Amharic version) (t)
Wp, Webdesign & SEO - وردپرس، طراحی و بهینه سازی in Persian(فارسی). (s)
سئو و بهینه سازی وردپرس in Persian(فارسی). (t)
Weblog Optimierung Suchmaschinen WP (s)
SEO für Wordpress (s)
SEO avagy keresőoptimalizálás (s)
SEO para Wordpress - (Portuguese-br version) (t)
Ottimizzazione delle ricerche per Wordpress (t)
WordPress的搜索引擎优化 (简体中文) (t)
Оптимизация за търсачки на Wordpress ( Bulgarian Version ) (t)
Optimiser son référencement avec Wordpress - (French version) (t)
Search Engine Optimization (SEO) สำหรับ WordPress - (Thai version, ภาษาไทย) (t)
4 Dasar SEO yang sering Terlupakan (s)
Wordpress SEO: Optimizarea Wordpress pentru motoarele de căutare - Versiunea in Română (t)
Search Engine Optimizacija za WordPress Platformu - Serbian version (t)
Category:
WordPress Lessons
Home Page
WordPress Lessons
Getting Started
Working with WordPress
Design and Layout
Advanced Topics
Troubleshooting
Developer Docs
About WordPress
Codex Resources
Community portal
Current events
Recent changes
Random page
Help
About
Blog
Hosting
Jobs
Support
Developers
Get Involved
Learn
Showcase
Plugins
Themes
Ideas
WordCamp
WordPress.TV
BuddyPress
bbPress
WordPress.com
Matt
Privacy
License / GPLv2


Code is Poetry
