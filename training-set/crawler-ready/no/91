WooThemes
Themes
Plugins
WooCommerce
WooCodex
Support
Ideas Board

All Documentation

All Documentation
Themes
Plugins
      Contact Details
      Homepage Control
      Our Team
      Projects
      See You Later
      Sensei
      WooCommerce
      WooDojo
      WooFramework Extensions
      WooSidebars
      WooSlider
Extensions
 SEARCHSearch
☺ 19☹ 0⚠ Report
Documentation / Plugins / WooCommerce / Extensions
Measurement Price Calculator
Overview
Installation
Calculator Basics
Creating Measurement Products
Calculator Modes
Room Walls Calculator
Features Supported
Pricing Table
Product Variations Support
Demos and Sample Products
Frequently Asked Questions
Translating Labels
Questions & Feedback
Relevant Links
Installing Plugins
Developer Documentation
Installing and Activating WooThemes Licenses
Using WooThemes Support Resources
More from “Extensions”
Purchase Demo Changelog
Overview ↑ Back to Top
Please Note: This plugin has a lot of functionality, so we strongly recommend carefully reviewing all documentation to see the various setups and selling options it provides. Looking to add units or change functionality? Check out the Measurement Price Calculator Developer Documentation instead. Want sample setups? Check out our demo settings.
This plugin allows you to add quantity/user-defined calculators to enable the listing of products that are sold per unit with a known measurement (for instance, tiles with an advertised square footage per box), or products which are priced by the foot, square meter, cubic yard, kilogram, etc and are purchased using a custom measurement (such as fabric).

By determining what kind of measurement you’d like your pricing based on, you can then set price by a defined quantity (for example, price based on a box with 12 tiles), or price by allowing customers to enter their own measurement (for example, price by linear foot).

The two calculator modes (the quantity-based calculator and the user-defined calculator) are possible for each type of measurement, and you can change between them by enabling or disabling one checkbox. The quantity-based mode allows the customer to enter the measurement of product they require, and the calculator will determine the quantity (i.e., boxes of tiles) required to meet or exceed the required measurement. With the user-defined mode, you set a price per measurement (i.e., dollars per foot) and the customer enters the dimensions they require, and the total price is then calculated.

With either calculator, you can choose between dimensions (i.e., length), perimeter, area, volume, or weight. You can use any combination of units (perhaps a square foot area with depth in inches to calculate the amount and cost of mulch priced in cubic yards). Finally, you can customize the field labels to reflect whatever product you sell.

This tutorial also gives you some good information on how to use the calculator and goes through some sample configurations.

Installation ↑ Back to Top
Download the extension from your dashboard
Go to Plugins > Add New > Upload and select the ZIP file you just downloaded
Click Install Now, and then Activate
Read the next sections to learn how to setup and configure the plugin.
Calculator Basics ↑ Back to Top
Default Units
The first step in configuring the plugin is to select the default Dimension, Weight, Area, and/or Volume units in which you’ll define your products’ area and volume. This is done from WooCommerce > Settings > Products > Product Data:

WooCommerce Measurement Price Calculator


Configuring the Calculator

The plugin both modifies the “Shipping” tab and adds a new tab named “Measurement” to the Product Data meta box in the product edit admin page. In this tab, you select:

The Measurement to calculate, for instance “Area”
Whether to display the product price per unit on the frontend for easy comparisons (ie $1.68 sq. ft.) or simply the product price (ie $27.48). Enabling the checkbox will calculate a price per unit based on the product price and configured measurement (under Shipping) for the quantity.
Pricing label to display next to the product price per unit (defaults to the “pricing unit” if blank)
Pricing unit to display with price
The label to display next to the input field(s) (ie “Enter Sq Ft Needed”)
The input field’s unit to display on the frontend
For the Dimensions, Area, Volume, and Weight calculators you can choose whether a desired amount can be entered by the user, which will automatically update the selected quantity and price, or whether the value will be an informational display-only value that is dynamically updated as the product quantity is changed
WooCommerce Measurement Price Calculator Product Data

If you’re not sure what to do with each setting, check out our demo setups to get a feel for how each setting changes the calculator. There will probably be a setup similar to what you’re looking for.

Available Units
The following units are available natively with the plugin:

Weight

t (tonne/metric ton)
kg
g
tn (short ton)
lbs
oz
Length

km
m
cm
mm
in
ft
yd
mi (mile)
Area / Surface Area

ha (hectare)
sq km
sq m
sq cm
sq mm
acs (acre)
sq. mi.
sq. yd.
sq. ft.
sq. in.
Volume

cu m
L
mL
gal
qt
pt
cup
fl. oz.
cu. yd.
cu. ft.
cu. in.
Notes:

SI (metric) and English units can be mixed, though a small loss of precision may be experienced. Use a consistent unit system for all measurements and for defining prices for a given product for optimal results.
As explained below in the FAQ, the display of units can be customized, i.e. “m2” rather than “sq m”
Developers: Additional derived units, as well as completely new or even made-up units can be defined through the use of filters in the plugin. See the developer documentation for further details and sample code. Yes, you can even add the Smoot!
Creating Measurement Products ↑ Back to Top
Measurement Types
After you’ve set the default store units, you can start setting up products. The first setup option you’ll need to determine for any product is the Measurement type. You can choose any of the following measurement types:

Dimensions: product length, width, or height
Area: simple area, or length x width
Perimeter: based on (2 x Length) + (2 x Width)
Surface Area: based on 2 x ([LxW] + [LxH] + [WxH])
Volume: simple volume, area x height, or length x width x height
Weight: the product weight
Room Walls: an area-type calculator intended for selling wallpaper
WooCommerce Measurement Price Calculator

Note, for example, the two versions of “area” (simple and L x W). This affects the user input — do you want the customer to enter the square footage (or other area unit)? Use simple area. Want customers to enter the length and width and calculate area for them? Then you’d use the L x W version. This is true for volume as well.

For each measurement type, you can calculate price based on quantity-based mode (for example, measured objects that come in defined packages, such as flooring), or you can allow the user to enter the measurements, called user-defined mode, for which price is calculated (for example, purchasing a number of hours of tutoring).

Calculator Modes ↑ Back to Top
You can switch between calculator modes once you’ve determined measurement type for your product. Determine whether customers should be able to set price based on custom measurements (user-defined mode) or to only allow defined quantities (quantity-based mode). For quantity-based mode, keep the “Calculated Price” box unchecked; for user-defined mode, you must check the “Calculated Price” box:

WooCommerce Measurement Price Calculator Mode Toggle
Select mode by enabling or disabling calculated price
Quantity Mode Configuration

The Quantity-based Mode (disable “Calculated Price”) is used to assist customers with determining the minimum quantity of product required to meet some measurement. For instance, the number of boxes of tiles needed to cover a given square footage would use an area calculator in quantity mode since the final price will be based on a quantity of boxes.

Product Properties
After you’ve selected “Measurement” type, you can set the regular price for the product. For a quantity-based mode calculation, this should be the price of a quantity of “one” — using the tile example, this will be the price of a whole box. The price per square foot can be calculated from that if desired:

WooCommerce measurement price calculator

If you’re happy displaying the price per box/quantity, you can leave the price as is. If you want to display price per square foot (or per unit), you’ll need to take one more step. To enable calculation of price per unit, go to the “Measurement” tab under Product Data and enable the “Show Product Price per Unit” box:

WooCommerce Measurement Price Calculator Enable Unit Price

This will display the price per unit (for easier comparison) instead of the price per quantity in the product catalog.

Next, you’ll need to define the product properties (Weight, Dimensions, Area, or Volume) in the “Shipping” tab of the Product Data meta box for your quantity. For my tile example, I need to set the area that represents a quantity of “1 box”. Since 1 box will hold 12 sq ft of tile, I enter 12 for my area value. This sets the quantity values so that no matter what length x width (or area) the customer enters, the calculator will include the correct number of boxes to cover at least the area needed. (For convenience with the area/volume measurements, you can set an exact value, ie 4 sq. ft. or 1.7 cu m, or you can set a length and a width, ie 2 ft x 2 ft to derive the area.)

WooCommerce Measurement price calculator quantity mode product

After saving the product, the product page will include the quantity-based calculator (this example uses simple area and price per unit display):

WooCommerce Measurement Price Calculator Quantity Mode product
Product Using Quantity Mode
Here the customer supplies the area of the floor they need to cover and the calculator determines the minimum quantity of product (boxes of tile) required to meet or exceed the area.

User-Defined Mode Configuration

Use the User-defined Mode (enable “Calculated Price”) to sell customizable products which are priced per measurement (i.e., fabric sold at $1.50 per yard). The pricing calculator is configured very similarly to the quantity-based calculator above, with a few exceptions. Firstly, dimensions are not required to be configured for a product (under “Shipping”) since there’s no quantity, and hence no defined properties. This calculator is intended for products with user-supplied measurements, such as a tarp with a custom length/width, or mulch sold by the cubic yard.

After selecting a calculator measurement type (dimensions, area, volume, or weight), you enable the “Show Product Price Per Unit” option, and then the “Calculated Price” option which reveals a few new fields: Calculated Weight, Calculated Inventory, and Area Options for the product measurements. The Pricing Label will be shown next to the product price, and the Pricing Unit is the unit the price is defined in.

WooCommerce Measurement Price Calculator Calculated Price

Making these changes also allows you to configure an optional minimum product price (useful for products that have low costs so that some price is charged), as well as a price “per unit” in the “General” tab, which is reflected in the Catalog and Product pages:

WooCommerce Measurement price calculator general product pricing

Pricing Per Unit in Catalog
Pricing Per Unit in Catalog
As shown in the configuration image above, custom field labels can be configured (i.e., “Garden Size” and “Thickness” for the product area and height inputs). Notice that pricing mode also creates Measurement Options. Options can be restricted to certain values by entering them, separated by commas, in the “options” field, as in the above configuration image (here “Area Options” and “Height Options”).

Leaving the “Options” field empty will create a free-form input box that the customer can enter any value into.
Setting “Options” to a single value creates a static measurement field on the frontend which can’t be changed.
Configuring a comma-separated list of values will create a dropdown on the frontend for the customer to choose between; note that fractional values are supported:
Product with Pricing Calculator
Product with Pricing Calculator
With this calculator configured the customer enters the area in square feet they wish to cover with mulch, the depth, and the calculator determines the total cubic yardage and thus price. The product dimensions are shown in the cart:

Pricing Calculator Product in Cart
Pricing Calculator Product in Cart
Remember, any of the calculator measurements (dimensions, area, volume, or weight) can be used with any combination of units and custom labels.

Room Walls Calculator ↑ Back to Top
The Room Walls calculator is simply an Area (length x width) calculator that is tailored for wallpaper sales with length/width labels pre-defined for you. The wallpaper product must have an area (either length x width, or simple area) defined so that the number of rolls required can be calculated. The calculator prompts the customer for the distance around the room and the height of the walls, those values are used to determine the total wall area of the room regardless of shape, and thus the quantity of wallpaper rolls required:

Product with Wallpaper Quantity Calculator
Product with Wallpaper Quantity Calculator
Features Supported ↑ Back to Top
Inventory Support
Product inventory can be managed “per unit” by enabling the “Calculated Inventory” option if you’re using the user-defined mode. Doing so adds the configured pricing units to the Product Data > Inventory – Stock Qty input, meaning that if you sell for instance fabric by the foot, you can configure the number of feet currently in stock:

WooCommerce Measurement Price Calculator Inventory Management
Inventory by the Foot
If the customer orders 2 x 10 foot products a total of 20 feet will be deducted from your available inventory.

Dynamic Weight

Product weight can be configured “per unit” by enabling the “Calculated Weight” option in user-defined mode. Doing this adds the configured pricing units to the Product Data > Shipping – Weight field, allowing you to set the weight “per unit”, (ie lbs / sq ft) for your customizable product:

Example: Shipping based on Pounds per Square Foot
Example: Shipping based on Pounds per Square Foot
Thus when your customer orders 2 square feet of a product weighing 5 lbs per sq ft, the product weight will be set to 10 lbs and accurate shipping prices can be calculated.


Pricing Table ↑ Back to Top
Please note that you must enable Show Product Price Per Unit and Calculated Price to enable the Pricing Table.
The Pricing Table feature allows you to create product prices that vary based on the product measurement: dimension, area, volume, or weight, depending on the measurement calculator in user-defined mode. Pricing table rules can be configured by clicking the “Pricing Table” link found next to the “Calculator Settings” heading on the Product Data > Measurement tab:

WooCommerce Measurement Price Calculator Pricing Table
Easily configure product prices that vary based on size
Defining a pricing rule is easy with the following fields:

Measurement Range – you need at least a starting value (which can be 0). Configure an ending value to create a closed range, or leave the end value empty for the last rule to create an “open” rule that will match all measurements greater than or equal to the starting value. The start/end range is inclusive, so 0-10 will match from 0 up to and including 10.
Price per Unit – a pricing rule must have a price per unit
Sale Price per Unit – set a sale price for this range
These pricing rules will be automatically applied on the frontend as the customer configures their product. If you’ve set a minimum or maximum measurement within you pricing table, customers will see a notice to contact the store because they’re trying to purchase an unavailable quantity:

WooCommerce Measurement Price Calculator Unavailable Quantity Notice
Unavailable Quantity Notice
This can be adjusted if desired by using something similar to this code snippet:

function sv_change_mpc_price_notice( $message ) {
	$message = 'My custom message text';
	return $message;
}
add_filter( 'wc_measurement_price_calculator_no_price_available_notice_text', 'sv_change_mpc_price_notice' );
A customized message will be displayed instead if the customer tries to purchase a quantity outside of your range:

WooCommerce measurement price calculator customized quantity message
Customized Message
Note: Everyone’s pricing breaks will be different, so this plugin respects pricing you set. Depending on your pricing breaks, higher quantities of product may result in cheaper pricing. For example, lets assume that you have a product that is sold by the foot:

0-10 ft: price $1/ft ⇨ if 10ft ordered, price = $10
11-20 ft: price $0.75/ft ⇨ if 11ft ordered, price = $8.25
 

You can easily list the full set of prices with the shortcode [wc_measurement_price_calculator_pricing_table]. When used on a product page, this will display the pricing rules for that product. The shortcode can be used anywhere on the site by simply providing either a product_id or product_sku: [wc_measurement_price_calculator_pricing_table product_sku="ABC123"]. The pricing rules will be displayed in a table which can be styled to match your site:

Prices are always up to date on the frontend thanks to a simple yet powerful shortcode
Prices are always up to date on the frontend thanks to a simple yet powerful shortcode
This can be really helpful for displaying product information within a blog post or on landing pages. We recommend using the WooCommerce Tab Manager to create a custom product tab to display this information.

Window Blinds Sellers: A note for window blinds sellers: this feature will probably not allow a window blinds pricing table to be configured as they generally require a price based on the width and height of a window. The pricing table works only with the product total measurement, for instance the total Area rather than the width/height.

Variable Products: Although the pricing table can be used with product variations, these rules can currently only be defined at the parent product level, meaning that all product variations will share the same set of rules.

Product Variations Support ↑ Back to Top
The Measurement Price Calculator quantity and pricing calculators are fully compatible with product variations: product measurements from the selected variation are used if defined. The pricing calculator is also fully compatible with variable products, but requires WooCommerce 2.0+ for product variations.

The only feature that can’t be used with product variations is the Pricing Table – from above:

Variable Products: Although the pricing table can be used with product variations, these rules can currently only be defined at the parent product level, meaning that all product variations will share the same set of rules.
If you need to use the pricing table with variations, please consider breaking this into separate measurement products rather than variations of the same product.

Demos and Sample Products ↑ Back to Top
Because the price calculator behavior is somewhat complex and nearly impossible to cover fully, please feel free to try out the set of example product calculators found at the WooCommerce Measurement Price Calculator demo site prior to purchasing. There you will find and can test some representative products:

Tile sold by the box covering a certain area
Mulch sold by the cubic yard, with the customer supplying an area and depth
Variable colored fabric by the foot
Wallpaper by the roll based on room dimensions
Here are some quick instructions for the common product configurations as well:

Boxes of Tile (Quantity-Based Mode Product)

Set the price per box/quantity under “Regular Price”.
Go to the “Shipping” tab. Enter the size of your quantity. For example, if this is tile, you’ll want to enter the total area the box will cover. If this is a dimension, volume, or weight product, then you’ll want to enter the quantity for the corresponding measurement that represents “one” of whatever you’re selling.
Go to the “Measurement” tab. Select the measurement type you need; for types such as “area” that have multiple possibilities, choose the type that represents what you’ll want customers to enter. Also select whether you want to display pricing per quantity (regular price) or pricing per unit (calculated unit price).
WooCommerce Measurement Price Calculator Sample Quantity Product
Setting up a quantity product based on area
Leave “Calculated Price” disabled and change the measurement labels if desired. Save the product, and it will look something like this on the frontend:
WooCommerce Measurement Price Calculator Quantity Product
Quantity product using area
Fabric by the Yard (User-defined Mode Product)

Go to the “Measurement” tab under Product Data. Select the Measurement type for your product. Since we’re using fabric here, we’ll need a “Dimension” type (length). You’ll need to enable pricing per unit since customers will be entering the total number of units.
Once “Show Product Price per Unit” is enabled, you must also enable “Calculated Price” so that customers can determine price based on measurement.
Adjust any measurement labels or options if needed.
Go to the “General” tab and enter the price per unit, as well as any sale or minimum pricing if needed.
WooCommerce Measurement Price Calculator Price Mode Product Sample
Price Mode using dimension product
Save the product, and customers will be able to enter the needed measurement on the frontend. The quantity for the product will remain at 1, so customers can add quantity if they need multiple cuts of fabric at the same length:
WooCommerce Measurement Price Calculator Price Mode Frontend

Mulch by the Cubic Yard (User-defined Mode Product)

Go to the “Measurement” tab and choose the measurement type; I’ll use Volume (AxH) in this example. Enable “Show Pricing per Unit” and “Calculate Price”.
Enter your units and change measurement labels if needed. Note that in this example I’m setting fixed options for the mulch height/thickness. This will create a dropdown menu for customers on the frontend:
WooCommerce Measurement Price Calculator Volume Example

Go to the “General” tab and enter pricing information. In this example, I’m going to set the minimum price at $11.00, which will be equivalent to 0.50 cu. yds.
Save the product information. Notice that in the first image, the customer hasn’t entered enough mulch to meet the minimum price, so $11 is listed. Once the customer has exceeded the minimum price, as in the second image, the accurate price is displayed:
WooCommerce Measurement Price Calculator Volume Price Mode

WooCommerce Measurement Price Calculator Volume Price Mode

Wallpaper for a Room (Quantity-based Mode)

Go to the “General” tab and set the regular price as the price per roll of wallpaper.
Go to the “Shipping” tab. Enter either the area per roll, or the length and width of the roll in your default unit to set the size of the quantity (area of one roll).
Go to the “Measurement” tab and select the “Room Walls” product type. Determine whether or not you’d like to display the price per roll or price per unit (enable “Show Price per Unit” if so), and adjust labels / units if needed.
Finish product setup and save. The product page will look something like this:
WooCommerce Measurement Price Calculator Wallpaper Example

Frequently Asked Questions ↑ Back to Top

Q: Is measurement price calculator compatible with WooCommerce Composite Products?
A: Unfortunately due to the technical complexity of these two plugins and the modifications they make to the standard WooCommerce product handling, there is presently no way to create a Composite Product that includes a Measurement product.


Q: Can I set a pricing range for a product based on measurements? For example, can I set weight tiers that determine what measurements of the product customers can order?
A: Absolutely! You’re looking for a pricing table. You can set ranges for the measurements with a unique regular price and sale price for each measurement range. If you set a sale price, it will cross out the regular price and display the sale price (as usual). If you set a minimum or maximum measurement, customers will not be able to order quantities outside that range and will get an error message to contact the store if they try to add the product to their cart.

WooCommerce Measurement Price Calculator Pricing Table Example
Pricing Table Example
Q: Speaking of pricing tables, why can’t I opt to use one?
A: You must enable “Show Product Price Per Unit” and “Calculated Price” for your measurement-based product to enable the Pricing Table.

Q: How do I display “m2” rather than “sq m” on the frontend?
A: This can be achieved by first going to the product edit screen > Product Data panel > Measurement tab, and setting the “Pricing Label” field to m&sup2; or m<sup>2</sup> (this is preferred):

woocommerce-measurement-price-calculator-faq-m2

If you use m&sup2;, then you should add the following code to the bottom of your theme’s functions.php:

add_filter( 'wc_measurement_price_calculator_unit_label', 'wc_measurement_price_calculator_unit_label' );

function wc_measurement_price_calculator_unit_label( $unit_label ) {
	if ( 'sq m' == $unit_label ) $unit_label = 'm&sup2;';
	return $unit_label;
}
Note that this technique can be adapted to work with any other desired unit.

Q: How do I add a new custom unit?
A: This can be done but requires some familiarity with PHP and the WordPress Action/Filter mechanism. See the WooCommerce Measurement Price Calculator Developer Documentation for further details.

Q: Can I place orders with measurement products on behalf of customers from the Admin and configure product dimensions, etc?
A: Unfortunately this is not currently possible directly from the admin. If you need to place an order on behalf of an existing customer we’d recommend using the User Switching plugin, which will allow you to log in as that customer and place an order from the frontend.

Q: Can customers enter fractions as well as decimals?
A: Yep! We’ve added support for fractional measurements in version 3.4.0. Customers can enter 8.5 or 8 1/2 to order the same amount – there must be a space between the whole number and the fraction.

Translating Labels ↑ Back to Top
WooCommerce is supported by WPML for translations. However, measurement and unit labels are not picked up by WPML, but they can be translated using some handy-dandy filters. Here’s some sample code to get you started – everything after the <?php should go at the bottom of your theme’s functions.php or in the Code Snippets plugin.

1
2
3
4
5
6
function sv_mpc_measurement_label( $label ) {

    return apply_filters('translate_string', 'woocommerce-measurement-price-calculator', $label, $label);
}
add_filter( 'wc_measurement_price_calculator_label', 'sv_mpc_measurement_label' );
add_filter( 'wc_measurement_price_calculator_unit_label', 'sv_mpc_measurement_label' );
view rawwc-mpc-measurement-label.php hosted with ❤ by GitHub
Here are a few things to note:

The labels we are trying to register for WPML translation are the measurement labels (such as Area, Height, etc), unit labels (such as sq m, cu m), the Pricing Label and any customizations to these. Basically everything that shows up on the product page:
WooCommerce measurement price calculator unit labels
One of the “Auto-register” options in WPML > String Translation > Auto register strings for translation should be selected:
WooCommerce measurement price calculator WPML
The MPC product should have a translation (even just a dummy one) and the user should visit one of the other languages for the string to be picked up.
No matter where the snippet is placed (theme or plugin) the context in WPML > String Translations will be theme woocommerce-measurement-price-calculator
wp-mpc-wpml-strings
Questions & Feedback ↑ Back to Top
Have a question before you buy? Please fill out this pre-sales form.

Have some feedback for us on this documentation? Please let us know so we can make improvements :).
Take me to the feedback form!

WOOTHEMES

About
Blog
Our Promise
WooCares
Team
Careers
Press
Style Guide
PRODUCTS

Themes
Plugins
WooCommerce
Extensions
On Sale
Submit your idea
Sell Your Extensions
SUPPORT

Help Desk
Community Support
Videos
Support Policy
Contact
WE RECOMMEND

Pre-sales FAQ
Customer Stories
Affiliated Woo Workers
Theme Demo Content
Hosting Solutions
T f B Ninja
COPYRIGHT WOOTHEMES 2015.
TERMS & CONDITIONS.

Back to the top
