Amazon Web ServicesAmazon Mechanical Turk 
Requester UI Guide (API Version 2014-08-15)
AWS Documentation » AWS Mechanical Turk » Requester User Interface Guide » Creating a Project » How to Create a Sentiment Project
Did this page help you?  Yes | No |  Tell us about it...
« PreviousNext »
Go
Welcome
Introduction to Mechanical Turk
Getting Started with the Requester UI
Creating a Project
How to Create a Project
How to Create a Categorization Project
How to Create a Sentiment Project
Publishing a Batch
Managing Batches
Managing Workers
Managing Qualification Types
Document History
View the PDF for this guide.Go to the AWS Discussion Forum for this product.Go to the Kindle Store to download this guide in Kindle format.
How to Create a Sentiment Project

This section shows you how to use the Amazon Mechanical Turk Sentiment Application (App) to create a Sentiment project. Similar to the Categorization App, the Sentiment App includes everything you need: predesigned HITs that do not require editing in HTML, pre-qualified Master Workers who have demonstrated expertise in categorization HITs, price recommendations based on comparable HITs in the Mechanical Turk marketplace and analysis tools that make it easy to verify results.

Sentiment projects are similar to the Categorization projects, but both Sentiment and Categorization projects are different from the other project templates listed under Start a New Project.

The following procedure describes in detail how to create a sentiment project.

To create a sentiment project

On the Mechanical Turk Requester website https://requester.mturk.com/, click the Create tab and then click New Project.

From the list under Start a New Project, click Sentiment, and then click Create Project.

Create Tab - Sentiment Project Selected
On the Create Project page, enter a name for your project and fill in the answer to the question What do you want Workers to judge attitude towards?. The answer should be specific so that it is clear what you are asking Workers to choose. Click Next.

Sentiment Project First Page
Enter instructions for the Workers who will work on your project. Select the number of Workers that you want to rate the sentiment for each item and then click Next.

Sentiment Project Second Page
On the Upload Data File page, click Choose File to locate the .csv data file that you want to upload and then click Upload. For information about creating a HIT csv data file, see Create your HIT data file in How to Create a Project.

Upload Data File
After your data file is uploaded, select the columns from your csv data file that you want to show to Workers and select the type of data from the Types of Data drop-down list to indicate whether the column contains text, a link to an image, or a link to a website. Click Next.

Choose Fields
The Preview page shows you how your sentiment items will look to Workers. If you find something that you want to change in the preview, click Go back and edit. If you are satisfied with the preview, click Continue.

Sentiment Preview
On the Checkout page, review the pricing information for your project. Mechanical Turk recommends that a Worker reward price be based on the complexity of the sentiment task and the price of similar HITs on the Mechanical Turk marketplace. You can change the Reward per Submission price. After you set the price you want, click Publish.

Checkout
On the Congratulations page, click here to go to the status page for your project.

Checkout
You'll receive an email when your sentiment project has been completed. The email contains a link to the Requester website so you can pick up your results. For more information about retrieving results for your project, see Managing Batches.

Document Conventions© 2014, Amazon Web Services, Inc. or its affiliates. All rights reserved.
Terms of Use	
