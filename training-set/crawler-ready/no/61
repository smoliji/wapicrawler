Contribute About Us About You Purpose Index Exclusive updates on:





Facilitating the spread of knowledge and innovation in professional software development Login

En |中文 |日本 |Fr |Br
1,023,962 Dec unique visitors
Development
Java
.Net
Cloud
Mobile
HTML5
JavaScript
Ruby
DSLs
Python
PHP
API
Featured in DevelopmentSpring 4 and Java 8
Java 8 shipped with new language features and libraries and Spring 4.x is already supporting many of these. Some of the new Java 8 features don’t have an impact on Spring and can just be used as is, while other Java 8 features require Spring to explicitly support them. This article will walk you through the new Java 8 features that are supported by Spring 4.0 and 4.1.
All in Development
Architecture
& Design
Architecture
Modeling
Scalability/Performance
DDD
BDD
AOP
Patterns
Security
Cloud
SOA
Featured in Architecture & DesignEvolution of the PayPal API Platform: Enabling the Future of Money
Deepak Nadig discusses how PayPal’s API Platform evolved both internally and externally, the principles and patterns used, and how it is enabling the future of Money.
All in Architecture & Design
Process & Practices
Agile
Leadership
Collaboration
Agile Techniques
Methodologies
Continuous Integration
Lean/Kanban
Featured in Process & PracticesHow to Remain Agile When You Have to Sign a Contract?
Agile development based on a contract that has been accepted by lawyers seems impossible. The nature of traditional purchasing and contracting processes does not match the Agile principles. This is a case story of how a supplier cooperated with a client to develop a huge project in an Agile way, by cutting it into smaller pieces and prepare a matching contract based on mutual trust.
All in Process & Practices
Operations & Infrastructure
Hadoop
Performance
Big Data
DevOps
Cloud
APM
Virtualization
NoSQL
Featured in Operations & InfrastructureExplore Your Data: The Fundamentals of Network Analysis
Amy Heineike covers how to construct clean (and avoid hairball) networks, effective ways to analyze them, as well as open source options for visualizing and interacting with your graphs.
All in Operations & Infrastructure
Enterprise Architecture
Enterprise Architecture
BPM
Business/IT Alignment
Architecture Documentation
IT Governance
SOA
Featured in Enterprise ArchitectureEvo: The Agile Value Delivery Process, Where ‘Done’ Means Real Value Delivered; Not Code
Current agile practices are far too narrowly focused on delivering code to users and customers. There is no systems-wide view of other stakeholders, of databases, and anything else except the code. This article describes what ‘Evo’ is at core, and how it is different from other Agile practices, and why ‘done’ should mean ‘value delivered to stakeholders’.
All in Enterprise Architecture
London 2015
Mar 2 - Mar 6
New York 2015
Jun 08 - Jun 12
Mobile
HTML5
JavaScript
APM
Big Data
Cloud
API Design
NoSQL
All topics
You are here: InfoQ Homepage News Blog Sentiment Analysis Using NoSQL Techniques
Blog Sentiment Analysis Using NoSQL Techniques by Srini Penchikala on Dec 28, 2011 | 1  Discuss ShareShare | 
Share on facebook
Share on digg
Share on dzone
Share on twitter
Share on reddit
Share on delicious
Share on email
Read laterMy Reading List
Corporations are increasingly using the social media to learn more about what their customers are saying about their products and their company. This presents unique challenges as unstructured content needs analytic techniques that involve taking words and associating quantitative metrics with them to interpret the sentiment embodies in them.

Subramanian Kartik and the Greenplum team at EMC worked on a research project involving the analysis of blog posts using both MapReduce and the Python Natural Language Toolkit, in combination with SQL analytics in an EMC Greenplum Database, using sparse vectors and k-means clustering algorithms.

Subramanian spoke about this research at NoSQL Now 2011 Conference this year. InfoQ caught up with him to learn more about the project and the architecture behind the solution they developed.

InfoQ: Can you talk about the Greenplum research project on blog sentiment analysis and the architecture behind it?

Kartik: Sentiment analysis refers to the application of natural language processing, computational linguistics and text analytics to extract subjective information from source materials, in this case, from a set of blog posts. This kind of analysis is becoming very popular in the analysis of social media data as well as for more conventional use cases such as e-Discovery among many of our customers. The end goal is to discover patterns of dominant concepts in a collection of documents, and to associate subsets of these documents with these concepts. An example of this, described here, would be to discover major themes in what bloggers write about concerning a company's products and services.

The architecture spans elements in natural language processing (which is in the realm of unstructured data) on one hand,  to quantifying this numerically and using machine learning algorithms for pattern recognition (typically done with structured data). The intriguing part of this architecture is how we use "Not Only SQL" approaches to solve this by utilizing a combination of Map/Reduce (typically used with Hadoop) and SQL-based analytics in a traditional RDBMS.

InfoQ: What technologies, programming techniques and algorithms were used in the solution?

Kartik: We used the EMC Greenplum database technologies to explore a corpus of blogs for such patterns. Although primarily an MPP relational database, Greenplum is very well suited to this sort of analysis as it has the ability to process structured and unstructured data using user code written in high level languages such as Perl and Python. The code is executed in-database using either the Map/Reduce interface to Greenplum or through SQL-callable User Defined Functions. The pipeline of data processing starts with the ingestion and parsing of the blog files (which are HTML files),  defining quantitative text metrics to characterize each blog with a numerical vector, followed by the use of machine learning algorithms to cluster the blogs around dominant themes. The output is a set of clusters centered around key themes, or "sentiments",  present in the corpus of blogs.

We used the support for Python in Greenplum to leverage the Python Natural Language Toolkit (NLTK) for HTML Parsing, tokenization, stemming and stop word removal from the original text, and then load the resulting term lists into a relational table. The beauty of this is that we could leverage the power of the open source NLTK code base with no modification and invoke it directly inside Greenplum using our Map/Reduce interface. Once we have term lists in a database table we cull it to form a well defined dictionary of terms across the corpus, and use an industry standard computational linguistics metric, term frequency X inverse document frequency (tf-idf), to create vectors that uniquely describe each blog document.  This gave us a vector space on which we define a "distance", and use a well known clustering algorithm in unsupervised learning, k-means clustering, to obtain the final result. Once again, all these operations are done in-database, that is, without having to pull the data out of the RDBMS and processing in place. Both tf-idf and NLTK, as well as k-means clustering are described elsewhere (e.g. Wikipedia) for the curious reader.

InfoQ: What were the main challenges in parsing and analyzing the blog data compared to the data analysis of traditional relational data?

Kartik: It's human language! Language is inherently ambiguous to machines. But beyond the English language, every industry has its own ontology for terms and abbreviations. The mobile phone blogs look very different from the health care blogs with all kinds of special words and special meanings for words. Not to mention the plethora of abbreviations in social media that have to be accounted for. The specific metric used here, with tf-idf, is one of the ways to characterize a document, and is acceptable to characterize a whole blog entry. In reality, subsections of the post may refer to different topics, or express different sentiment. More sophisticated machine learning algorithms such as Topic Models (Latent Dirichlet Allocation) may be needed to accomplish more granular analysis. Some practitioners associate sentiment on a sentence by sentence basis to account for the variation present in a post. This is a vast and growing field of research. The work done here just scratches the surface of a very complex area of research. Relational data in contrast is usually numerical, and we have 30 years of SQL based analytics and maturity in tools/techniques in machine learning to fall back on, so these are much better understood and easier to implement.

InfoQ: What are some NoSQL data patterns or best practices that Greenplum learned from work on this project?

Kartik: The most important lesson is the realization that one must keep an open mind as to what techniques we should use to solve a problem. One could certainly do this work completely in Hadoop, but the elegance of using both Map/Reduce and SQL to solve such a problem highlights the strength of both these paradigms.  As the amount of data increases, the dominant data pattern we see emerging is the need to process data in place, and to harness the computational power of the MPP architecture underlying Greenplum to also do computation in-database. As data volumes grow, we have to bring compute close to data and thus choosing the technology that supports this is critical. The ability to bring open source tools to bear is very important as well - this lets us leverage much intellectual capital without re-inventing the wheel. Once again, the underlying platform architecture and its ability to run user code in-database was critical to enable this.

InfoQ: What is the future road map of this project?

Kartik: This effort paints a picture of capabilities that can be extended for a variety of use cases by our customers and internally. I would love to use these techniques to analyze live Twitter feeds during a major industry event such as EMC World or VMWorld, and associate some visualization technologies with this as well. Theoretically, Topic Models (such as LDA) are interesting to study in this context as an alternative to k-means clustering - Greenplum can do both in-database. Scale is always an interesting frontier to explore as well, and ultimately multiple-language analysis may require even more innovation.

 

SectionsOperations & InfrastructureArchitecture & DesignDevelopmentTopicsDatabaseNoSQLSentiment AnalysisBusiness IntelligenceInfrastructureMapReducePythonBig DataPython Natural Language ToolkitDynamic LanguagesMachine LearningArtificial IntelligenceNatural Language Processing
Related Editorial

InfoQ eMag: The Best of NoSQLDistributed, Fault Tolerant Transactions in NoSQLAlex Bordei on Scaling NoSQL DatabasesMapR-DB NoSQL Database Integrated into MapR Community Edition for Unlimited Production UseBasho Riak - NoSQL Key Value Store Designed for Scale
Related Vendor Content

NoSQL Evaluator's Guide Managing Native Mobile App Performance on iOS and Android 5 Key Phases in Creating a Successful Mobile App New Relic for Amazon Web Services Making the Shift from Relational to NoSQL - 2015 Edition
Related Research

NoSQL: Emerging Trends and InnovationsNoSQL Database Adoption TrendsWhat's Your Next JVM Language?
Tell us what you think
 

Community comments Watch Thread
Risks of sentiment mining by peter lin Posted Dec 29, 2011 02:57
Risks of sentiment mining
Dec 29, 2011 02:57 by peter lin
The topic of using sentiment mining has been going for several years now. For me, mining public social networks is far to risky. It's much too easy for a attacker to create a sentiment storm. There's an imbalance in favor of attackers. The real value I see in sentiment mining is using it on trusted data. I know there are firms actively exploring sentiment mining, but once companies start using it, attackers will exploit it. Writing detection algorithms to determine if the sentiment is real or fake isn't trivial at all. Far too many people are caught up in the "ideal" of sentiment mining without taking time to consider the serious implications of how one defends against attackers. Look at all the recent examples of fake tweets and blogs getting republished by traditional media. Detecting real from fake sentiment at this time is basically impossible.
Reply
Back to top
Educational Content
All
Articles
Presentations
Interviews
Books
Research
How to Remain Agile When You Have to Sign a Contract?
Paweł Bejger, Peter Horsten Jan 29, 2015 
Spring 4 and Java 8
Adib Saikali Jan 28, 2015 
Mike Long on Modern C++ and the C++ Memory Model
Mike Long Jan 28, 2015 
Explore Your Data: The Fundamentals of Network Analysis
Amy Heineike Jan 27, 2015 
Evolution of the PayPal API Platform: Enabling the Future of Money
Deepak Nadig Jan 27, 2015 
Metrics-Driven Prioritization
Sam McAfee Jan 27, 2015 
Older
DevelopmentYouTube Switches to HTML5 Video PlayerThe Road to C# 7 Has BegunMicrosoft Releases Details, Confirms Rumours On Spartan ProjectArchitecture & DesignBad Practices Building MicroservicesTestFlightApp.com is Going to Shut Down Next MonthEvolution of the PayPal API Platform: Enabling the Future of MoneyProcess & PracticesHow to Remain Agile When You Have to Sign a Contract?Evidence-Based Managing of Software DevelopmentExercises for Leading Creative CollaborationOperations & InfrastructureAmazon Adds Managed Email and Calendaring Service to AWS PortfolioMesosphere Release Mesos-DNS Service Discovery for Apache MesosExplore Your Data: The Fundamentals of Network AnalysisEnterprise ArchitectureQCon London 5 Weeks Away; Top Tracks, Sessions, and SpeakersMicrosoft Azure G-Series VMs are Generally AvailableLeveraging Nashorn at Netflix
HomeAll topicsQCon ConferencesAbout usAbout YouContributePurpose IndexCreate accountLoginQCONS WORLDWIDELondon
Mar 2-6, 2015São Paulo
Mar 23-27, 2015Beijing
Apr 23-25, 2015Tokyo, 
April 21, 2015New York
Jun 8-12, 2015Rio de Janeiro
Aug 24-25, 2015Shanghai, 
Oct 15-17, 2015San Francisco
Nov 16-20, 2015InfoQ Weekly NewsletterSubscribe to our Weekly email newsletter to follow all new content on InfoQ 

Your personalized RSS
For daily content and announcements
For major community updates
For weekly community updates
Personalize Your Main Interests




Development
Architecture & Design
Process & Practices
Operations & Infrastructure
Enterprise Architecture
This affects what content you see on the homepage & your RSS feed. Click preferences to access more fine-grained personalization.
General Feedbackfeedback@infoq.comBugsbugs@infoq.comAdvertisingsales@infoq.comEditorialeditors@infoq.comInfoQ.com and all content copyright © 2006-2015 C4Media Inc. InfoQ.com hosted at Contegix, the best ISP we've ever worked with.
Privacy policy 
