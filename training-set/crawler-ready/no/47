ArcGIS Online Sign In    
Home Communities Help Blog Forums Videos

8 Facts You Didn’t Know About Community Maps
by Seth Sarakaitis on January 28, 2015
 2 10 19
By Don Cooke

ArcGIS now comes with so much free content that it has evolved into a Living Atlas of the World. Esri gets Content for the Living Atlas from three sources:

Partners including Navteq, which is now known as “Here,” and Digital Globe
Open sources such as OpenStreetMap
The community of ArcGIS users, many of whom serve as data stewards for important foundational themes Continue reading →
Posted in Community Maps	| Leave a comment
New Data Preparation Tools Workshops Available
by tyoder on January 28, 2015
 0 5 12
The ArcGIS Content Team has created new Community Maps Data Preparation Tools workshops. The main focus of these workshops are the Community Maps Data Prep Tools. The workshops also introduce students to the Living Atlas of the World, and provide an overview of each step in the Community Maps workflow. Continue reading →

Posted in ArcGIS Online, Community Maps	| Tagged ArcGIS Content, ArcGIS Online, Community Basemaps, Community Maps, Community Maps Data Prep Tools, Community Maps Data Preparation Tools, Living Atlas, Living Atlas of the World, Training, Workshops	| Leave a comment
ArcGIS Open Data Site of the Week: Muroran, Japan
by Courtney Claessens on January 27, 2015
Muroran  29 9 38
Governments and organizations have been hard at work creating Open Data sites to make their authoritative data accessible, discoverable, explorable, and collaborative. There are over 100 sites in the ArcGIS Open Data site gallery for you to explore!  The Site of the Week is a series … Continue reading →

Posted in ArcGIS Online, Local Government, National Government, Open Data, State Government, Web	| Tagged ArcGIS Open Data, Site of the Week	| Leave a comment
ArcGIS Pro – Reinventing Desktop GIS
by Rob Elkins on January 27, 2015
3D Editing with ArcGIS Pro  71 187 281
ArcGIS Pro is now available, and it is part of ArcGIS 10.3 for Desktop. This new desktop application includes many of the features and enhancements you’ve been asking for, including multiple layouts, 2D and 3D editing, 64-bit processing, project workflows, … Continue reading →

Posted in 3D GIS, ArcGIS Online, ArcGIS Pro	| Tagged 3D Analysis, 3D visualization, ArcGIS for Desktop, ArcGIS Online, ArcGIS Pro, Multiple Layouts	| 2 Comments
ArcGIS Runtime SDK 10.2.5 for WPF is now available!
by mbranscomb and Rex Hansen on January 27, 2015
 2 8 16
The 10.2.5 release of ArcGIS Runtime SDK for WPF is now available to download from ArcGIS for Developers. This release follows the 10.2.3 release in May 2014 and addresses at least 50 bugs you asked us to fix, and a few more we found as well! We also found time to add a few minor enhancements you asked for:

Visual Studio 2013 Community Edition is now a supported IDE.
The gdbVersion on RelationshipParameter Class is now supported.
New DrawMode.ScreenAlignedRectangle enumeration type was added (Note: DrawMode.Rectangle is map-aligned).
Support for feature collection items by reference in WebMaps.
Exposed Symbol properties on Editor, TemplatePicker and EditorWidget.
Support for WMTS multidimensional services.
You can read the full list of enhancements, issues addressed and known limitations in the release notes.

Note that version 10.2.5 marks the last planned release of the ArcGIS Runtime SDK for WPF. Technical support will continue to be available through June of 2016. Moving forward, we recommend you explore WPF solutions based on the ArcGIS Runtime SDK for .NET. For information on transitioning to the new ArcGIS Runtime SDK for .NET please read this blog post.

 

Posted in App Developers, Developer, Uncategorized	| Tagged .NET, .NET SDKs, ArcGIS Runtime, ArcGIS Runtime SDK, ArcGIS Runtime SDK for WPF, C#, WPF	| Leave a comment
Version 3.3 of the ArcGIS API for Silverlight and ArcGIS Viewer for Silverlight now available!
by Rex Hansen and Rich Zwaap on January 26, 2015
 2 26 37
New versions of the ArcGIS API for Silverlight and ArcGIS Viewer for Silverlight are now available for download.  The 3.3 release is focused on bug fixes and targeted enhancements to improve product quality and integration with the ArcGIS platform.  Changes include:

API

Support for GdbVersion on ArcGISDynamicMapServiceLayer and RelationshipParameter classes
Support for feature collection items in web maps
Exposed symbol properties to modify the editing experience with Editor, TemplatePicker, and EditorWidget
Viewer

Support for OAuth authentication when connecting to ArcGIS Online and Portal for ArcGIS content
Support for services with multi-point geometry
Fallback to authentication using proxy when service connections fail
In addition to the changes above, the 3.3 release includes support for Visual Studio Community 2013. You can learn more about what’s new in the API here, and what’s new in the Viewer here. The Viewer’s source code is available on GitHub here.

Note that version 3.3 marks the last planned releases of the ArcGIS API for Silverlight and ArcGIS Viewer for Silverlight. Technical support for both products will continue to be available through June of 2016. Please refer to this blog post for further information regarding the support plan for these products. Moving forward, we recommend that you explore web solutions based on JavaScript and HTML, leveraging the ArcGIS API for JavaScript for GIS functionality. If you are interested in configurable web applications, please have a look at the recently released Web AppBuilder for ArcGIS (downloadable developer edition here), which allows you to interactively create JavaScript and HTML-based GIS web apps.

Enjoy!

Posted in Developer, Web	| Tagged ArcGIS API for Silverlight, ArcGIS Viewer for Silverlight, ArcGIS Web Mapping API, Silverlight, viewer	| Leave a comment
Even More than Points: New Features in ArcGIS for Maritime: Bathymetry 10.3
by Caitlyn Raines on January 26, 2015
 1 23 32
ArcGIS for Maritime: Bathymetry 10.3 is full of new and exciting features, such as the new support for point feature classes!  Here’s a quick list of some of the highlights: Continue reading →

Posted in Oceans & Maritime, Uncategorized	| Tagged ArcGIS for Maritime: Bathymetry, bathymetry, Hydrography, Oceans and Maritime	| Leave a comment
Esri Maps for IBM Cognos 6.0.1 Released!
by JennWH on January 22, 2015
 6 10 33
We are pleased to announce the release of Esri Maps for IBM Cognos 6.0.1.

This release of Esri Maps for IBM Cognos offers the following updates:

Report consumers can add data from ArcGIS to the map
Support for IBM Cognos 10.1, 10.1.1, 10.2 FP 4, 10.2.1 FP5, and 10.2.2
Support for Novell SUSE Linux Enterprise Server and Red Hat Enterprise Linux Server on System z
Support for Apple Safari on iOS 8.x
Software and documentation available in seven languages: English, German, French, Japanese, Korean, Russian, and Simplified Chinese
Visit esri.com to download and install Esri Maps for IBM Cognos 6.0.1.

For more information about Esri Maps for IBM Cognos 6.0.1, see the Esri Maps for IBM Cognos product documentation, and be sure to visit GeoNet to get help from the community.

~The Esri Maps for IBM Cognos Team

Posted in Location Analytics	| Tagged 6.0.1, announcements, BI, business intelligence, Cognos, esri maps, Esri Maps for IBM Cognos, location analytics, Release, what's new	| Leave a comment
ArcPad 10.2.2 Released!
by Chris LeSueur on January 22, 2015
ArcPad  83 127 283
The ArcPad team is pleased to announce that ArcPad 10.2.2 is available to download via the My Esri website. The ArcPad 10.2.2 update addresses some important usability and feature enhancements that improves the utility and stability of ArcPad.  Here are … Continue reading →

Posted in Apps, Mobile, Uncategorized	| Tagged app, ArcPad, Mobile	| 1 Comment
A Brief Introduction to the Maritime Chart Server
by Caitlyn Raines on January 21, 2015
MCSpic  7 15 40
Esri is committed to developing new technologies and ways to utilize the ArcGIS platform that expands its support of the maritime operations.  One of our emerging technologies, the Maritime Chart Server, has been generating a lot of buzz and excitement … Continue reading →

Posted in Mapping, Oceans & Maritime	| Tagged ArcGIS for Maritime: Charting, ArcGIS Server, maritime, nautical, nautical charts, Oceans & Maritime, s-52, S-63, web maps	| Leave a comment
Pages:1234567...428»
This Blog
Sign in
Subscribe to the RSS Feed
Comments RSS
Technical Communities
3D GIS (119)
Analysis & Geoprocessing (262)
ArcGIS Online (844)
Developer (453)
Editing (149)
Geodata (195)
Imagery (291)
Mapping (508)
Mobile (198)
Python (47)
Security (7)
Services (623)
Story Maps (73)
Web (222)
Industry Communities
App Developers (96)
Community Maps (196)
Defense (107)
Electric & Gas (54)
Hydro (173)
Local Government (239)
Location Analytics (379)
National Government (76)
Oceans & Maritime (103)
Public Safety (125)
State Government (76)
Telecommunications (62)
Transportation (50)
Water Utilities (240)
Recent Posts
8 Facts You Didn’t Know About Community Maps
New Data Preparation Tools Workshops Available
ArcGIS Open Data Site of the Week: Muroran, Japan
ArcGIS Pro – Reinventing Desktop GIS
ArcGIS Runtime SDK 10.2.5 for WPF is now available!
Tags
.NET Android Announcement ArcGIS 10 ArcGIS Content ArcGIS Data Reviewer ArcGIS Desktop ArcGIS Methods ArcGIS Mobile ArcGIS Online ArcGIS Server ArcMap bao-only basemaps Best Practices Business Analyst Desktop Business Analyst Online Cartographic Design Community Basemaps Community Maps data demographic data Editing Esri Data Flex Geodatabase Geoprocessing GIS Imagery Java JavaScript maps map services mosaic dataset public safety Python Raster Rasters sewer stormwater Symbology Templates updates wastewater water

More Tags ...
Archives
January 2015
December 2014
November 2014
October 2014
September 2014
August 2014
July 2014
<12…14>
Feedback Site Map Legal Support Training
