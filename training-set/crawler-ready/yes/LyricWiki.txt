Skip to ContentSkip to Wiki NavigationSkip to Site Navigation
Wikia Navigation
Wikia
Create a wiki
Video Games
Entertainment
Lifestyle
Log in

Username

Password
Forgot your password?
 Stay logged in

Or
 Connect
Sign up
 Wikia API Wiki
On the Wiki
Wiki Activity
Random page
Videos
Photos
Chat
Forum
Resources
App Gallery
Support
 Contribute   Share Watchlist	Random page	Recent changes
LyricWiki API   Edit    Comments20 25PAGES ON
THIS WIKI

<< Documentation
Contents[show]
This page is for the documentation about the LyricWiki API. To see a list of some of the apps created using the API, please visit the LyricWiki Apps page.
Overview Edit
If we make backwards-incompatible changes or updates to the API, we will inform developers via the Google Group mailing list. We do NOT spam the list (you can see from the link that the volume is very low... about one email per year).

Google Groups
Subscribe to LyricWiki API Developers
Email:   
Visit this group
The LyricWiki API provides a convenient way for plugins and apps to easily find and display lyrics from LyricWiki. The typical process works like this:

Your app or plugin passes an artist and song-name to the API (there are also requests to get discographies etc, please see documentation).
The API returns a match (or a message saying there is no match) along with a small chunk of the lyrics and a link to a page where the user can view the full lyrics.
Your app either sends the user to the lyrics page or opens the lyrics page in a browser-panel with Javascript enabled.
This process is different from the original API which used to return the full lyrics in the API result. This had to be changed due to licensing restrictions. We can't get a license to distribute lyrics for free via an API, however we do have a license to display lyrics on a web-page as long as the web-page has javascript enabled so that we can track which songs are being viewed (for distributing royalties) and have an ad on them so that a percentage of the money can be paid as licensing fees.

Quick Start Guide Edit
TODO: Make a step-by-step list of how to make something using the API. Try to do so using one of the existing Client libraries.

Two ways to access the API: SOAP or REST-like Edit
The API has a number of functions which can currently be accessed by two different methods: as a SOAP (Simple Object Access Protocol) webservice or a more lightweight REST-like interface (the request is just a specially-formatted url). Each method has different advantages and disadvantages.

If you are overwhelmed by this section and don't know which one to choose, just use REST. It's easier.
SOAP Edit
SOAP documentation
The SOAP webservice lets your code use the API as if the functions were a normal part of your code. This saves the trouble of having to parse the results from the API. Most languages (PHP, Java, Ruby, .NET, etc.) have libraries which make it possible to get this ease-of-use from SOAP webservices. If there is not a library for your language, it would be a ton of work to use the SOAP API and you would probably have a much easier time using REST. Our SOAP documentation has information on what languages and libraries have been used successfully (or unsuccessfully in rare instances) with the LyricWiki API in the past.
More challenging to debug since you need to do it from a SOAP client rather than just a webbrowser.
There are glitches in some libraries which try to implement SOAP. Please check the SOAP documentation to make see if the library you want to use will work well enough for you.
REST-like Edit
REST documentation
Super easy to make a request and to debug the requests in your browser. Here are some example requests:
http://lyrics.wikia.com/api.php?artist=Cake&song=Dime
http://lyrics.wikia.com/api.php?artist=Cake&song=Dime&fmt=xml
Once you have the results back, you will have to parse them yourself (as opposed to the SOAP which will give you the result as an object).
There are several result formats that you can choose from (eg: text, xml, html, and sometimes json).
Is not "actual" REST. If you're used to very strictly RESTful APIs, then just pretend we're not using the word REST ;)
Support Matrix Edit
Matrix represents the table data, which describes data in rows and columns

tertert

Key
Completed	Fully supported
Incomplete	Partially supported (might be buggy)
Failed	Not supported
n/a	Don't plan to support this

Function	SOAP	REST
text	html	xml	json	php
Fetching
getSong	Completed	Completed [1]	Completed [2]	Completed [3]	Completed [4]*	Completed [5]
getArtist	Completed	Completed	Completed [6]	Completed [7]**	Completed [8]	Completed [9]
getAlbum	Failed	Failed	Failed	Failed	Failed	Failed
getHometown	Completed	Completed	Completed [10]	Completed [11]	Completed [12]	Completed
getSOTD	Completed	Failed	Failed	Completed	Completed	Completed
getTopSongs	Completed	Failed	Failed	Completed	Completed	Completed
Updating
postArtist	 ?	Failed	Failed	Failed	Failed	Failed
postAlbum	 ?	Failed	Failed	Failed	Failed	Failed
postSong	Completed	Failed	Failed	Completed	Completed	Completed
* Note the realjson format on some of the calls. It is recommended that this be used on all calls to get valid JSON as opposed to the original format of the Javascript. The old format was kept as the default to keep from breaking existing apps but is poorly formed JSON.

** Note the fixXML parameter on the getArtist XML call. It is recommended that this be used on all getArtist XML calls to get valid XML as opposed to the original format of the XML. The old format was kept as the default to keep from breaking existing apps but is poorly formed XML.

FAQs Edit
Why does the API only return a few lines of lyrics for each song?
Please note that currently, the API only returns a small portion of the lyrics (about 1/7th of them). This is because of the licensing restrictions mentioned in the Google Group thread. The small snippet (which is considered Fair Use) is just to help in identifying that the API matched the correct song you're looking for. Once you have the right match, you can link the user to the corresponding page (there is a URL provided in the result) or open that page inside your app in a browser-panel with javascript enabled.
Can I sub-license lyrics from LyricWiki to use the full lyrics in an app I'm making?
Unfortunately not. LyricWiki is not licensed to sub-license lyrics to other parties. The best system we can recommend is to use the API to match to the correct page-name, then display the LyricWiki page inside of a browser panel in your app. The only way we are licensed to display lyrics is on the web and with javascript enabled. This allows our javascript tracking-pixels to count how many times each song is viewed (which determines how much each music publisher gets paid) and it allows the ads to run (the royalties we pay are based at least partly off of a percent of that revenue).
_
In summary: please use a browser panel to display the lyrics, and choose the skin that works best for your app. For example, if your app has a small area for the lyrics, the mobile skin may be the best fit for your app.
Can I make jsonp requests to access the API from another domain?
Yes. As of the release on Wednesday April 25th, the LyricWiki REST API will support the 'callback' parameter for any fmt=realjson requests. Simply add the name of the callback as the &callback=MyCallbackName parameter, or use a popular library such as jQuery. We support the common convention of using callback=? to indicate that the result should just be wrapped in an anonymous function. This allows jQuery to operate on the jsonp result automatically.
Log of Recent Changes Edit
If we make any adjustments to the way the API works, we intend to list them here so that you can make sure your apps are still up to date (it's very rare that we make changes which aren't backward-compatible though).

None
Toolbox
Quick Start
Content API - Documentation
MediaWiki API - Documentation
API Explorer
Categories:
Is it possible to search for a word and get a list of songs containing this word? Add category
Around Wikia's networkRandom Wiki
Wikia Inc Navigation

[ Lifestyle ]
About  Community Central  Careers  Advertise  API  Contact Wikia  Terms of Use  Privacy Policy  Content is available under CC-BY-SA.
LifestyleEntertainmentVideo Games