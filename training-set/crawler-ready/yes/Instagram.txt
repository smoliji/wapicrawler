
Instagram

    Manage Clients
    Log in

    Overview
    Authentication
    Restrict API Requests
    Real-time
    Mobile Sharing
    API Console
    Endpoints
    Limits
    Embedding
    Libraries
    Support
    Platform Developers

Restrict API Requests

Most API calls require an access token, but malicious developers can impersonate OAuth Clients or steal access tokens. They will then use these to send spam on the behalf of your app. Instagram has automated systems to detect spam, and will automatically disable the OAuth Clients responsible for these calls.
You can mitigate the risk of your app being disabled by restricting some vectors of abuses. This document covers some of the ways you can protect your app.
Disable Client-Side (Implicit) Authentication

The Implicit OAuth Grant flow was created for java-script or mobile clients. Many developers use this flow because of its convenience. Unfortunately, malicious developers can also use this flow to trick people into authorizing your OAuth Client. They can collect access tokens and then make API calls on behalf of your app. When this occurs, your OAuth Client could be banned from the platform by our spam detection systems.

If your app is powered by a server infrastructure, you can disable the Client-Side (Implicit) OAuth flow by checking the Disable implicit OAuth setting in your OAuth Client configuration. If checked, Instagram will reject Client-Side (Implicit) authorization requests and only grant Server-Side (Explicit) authorization requests. This setting helps protect your app because the Server-Side (Explicit) OAuth flow requires the use of your Client Secret, which should be unknown to malicious developers.

    Important Note

    Your Client Secret should be kept secure at all times. Do not share this Secret with anyone, do not include it in java-script code or a mobile client. Mobile apps that do not have a server-side component should have the Disable implicit OAuth setting unchecked. You have the ability to reset your Client Secret to a new value at any time, if you suspect that it was leaked.

Enforce Signed Header

Access tokens are portable: they can be generated on one machine and re-used elsewhere. Access tokens can also be stolen by malicious software on a person's computer or a man in the middle attack. A stolen access token can then be used to generate spam. When targeted by such abuses, your app could be blocked by our automated systems.

You can help us better identify API calls from your app by making server-side calls with a HTTP header named X-Insta-Forwarded-For signed using your Client Secret. This header is optional, but recommended for any app making server-to-server calls. To enable this setting, edit your OAuth Client configuration and mark the Enforce signed header checkbox. When enabled, Instagram will check for the X-Insta-Forwarded-For HTTP header and verify its signature. The expected value is a combination of the client's IP address and a HMAC signed using the SHA256 hash algorithm with your client's IP address and Client Secret.

The following endpoints require the X-Insta-Forwarded-For header if Enforce signed header is enabled:

    POST /users/{user_id}/relationship
    POST /media/{media_id}/comments
    DEL /media/{media_id}/comments/{comment_id}
    POST /media/{media_id}/likes
    DEL /media/{media_id}/likes

    Important Note

    Your Client Secret should be kept secure at all times. Do not share this Secret with anyone, do not include it in java-script code or a mobile client. Mobile apps that do not have a server-side component should not use the Enforce signed header setting. You have the ability to reset your Client Secret to a new value at any time, if you suspect that it was leaked.

Header format

HTTP Header: X-Insta-Forwarded-For
Value: [IP information]|[Signature]

    IP information: Comma-separated list of one or more IPs; if your app receives requests directly from clients, then it should be the client's remote IP as detected by the your app's load balancer; if your app is behind another load balancer (for example, Amazon's ELB), this should contain the exact contents of the original X-Forwarded-For header. You can use the 127.0.0.1 loopback address during testing.
    Signature: Using your Client Secret, apply an HMAC with SHA256, and append the hex representation of the signature there.

Examples

Single IP: 200.15.1.1
App Secret: 6dc1787668c64c939929c17683d7cb74

With this data, the header should be:

200.15.1.1|7e3c45bc34f56fd8e762ee4590a53c8c2bbce27e967a85484712e5faa0191688

Multiple IPs: 200.15.1.1,131.51.1.35
App Secret: 6dc1787668c64c939929c17683d7cb74

Here the header would then be:

200.15.1.1,131.51.1.35|13cb27eee318a5c88f4456bae149d806437fb37ba9f52fac0b1b7d8c234e6cee

The signature describes the hex representation of a RFC 2104-compliant HMAC with the SHA256 hash algorithm, using the IP information with your Client Secret. Most programming languages provide the tools to create such a signature. Here are some examples to get you started.
Python

import hmac
from hashlib import sha256

ips = '200.15.1.1'
secret = '6dc1787668c64c939929c17683d7cb74'

signature = hmac.new(secret, ips, sha256).hexdigest()
header = '|'.join([ips, signature])
print header

Ruby

require 'openssl'
require 'base64'

ips = '200.15.1.1'
secret = '6dc1787668c64c939929c17683d7cb74'

digest = OpenSSL::Digest::Digest.new('sha256')
signature  = OpenSSL::HMAC.hexdigest(digest, secret, ips)
header = [ips, signature].join('|')
print header

PHP

$ips = '200.15.1.1';
$secret = '6dc1787668c64c939929c17683d7cb74';

$signature = (hash_hmac('sha256', $ips, $secret, false));
$header = join('|', array($ips, $signature));
echo $header;

Testing the X-Insta-Forwarded-For header

Sending an invalid X-Insta-Forwarded-For header will cause your API calls to fail. Because of this, you may want to test this parameter before you enable it for production code. Fortunately you can use cURL to test your header format and signature easily:

curl \
  -X POST \
  -F 'access_token=<your_access_token>' \
  -H 'X-Insta-Forwarded-For: <your_ip_information>|<your_signature>' \
  https://api.instagram.com/v1/media/657988443280050001_25025320/likes

Common responses:
REASON 	RESPONSE
Success 	{"meta":{"code":200},"data":null}
Header is missing 	{"code": 403, "error_type": "OAuthForbiddenException", "error_message": "Invalid header: X-Insta-Forwarded-For is required for this operation"}
Header format is invalid 	{"code": 403, "error_type": "OAuthForbiddenException", "error_message": "Invalid header: Invalid format"}
Failed to validate signature 	{"code": 403, "error_type": "OAuthForbiddenException", "error_message": "Invalid header: Signature did not match"}
IP format is invalid 	{"code": 403, "error_type": "OAuthForbiddenException", "error_message": "Invalid header: IP information doesn't match an IPv4 or IPv6 address"}

    About us
    Support
    Blog
    Press
    API
    Jobs
    Privacy
    Terms

© 2015 Instagram

