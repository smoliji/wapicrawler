Developers
Apps
Products
Docs
Tools
Support


Product Docs
Login
Sharing
App Links
Games
Payments
Ads
Audience Network
Insights
App Development
SDKs
iOS
Android
JavaScript
PHP
Unity
Third-Party
APIs
Graph API
Quickstart
Using the Graph API
Reference
Common Scenarios
Other APIs
Advanced
Ads API
Graph API Version
v2.2
Quickstart for Graph API
The Graph API is the primary way to get data in and out of Facebook's social graph. It's a low-level HTTP-based API that you can use to query data, post new stories, upload photos and a variety of other tasks that an app might need to do.

The basics
The Graph API is named after the idea of a 'social graph' - a representation of the information on Facebook composed of:

nodes (basically "things" such as a User, a Photo, a Page, a Comment)
edges (the connections between those "things", such as a Page's Photos, or a Photo's Comments)
fields (info about those "things", such as the birthday of a User, or the name of a Page).
The Graph API is HTTP based, so works with any language that has an HTTP library, such as cURL, urllib. We'll explain a bit more about what you can do with this in the section below, but it means you can also use the Graph API directly in your browser, for example this Graph API request is equivalent to:

GET graph.facebook.com
  /facebook/picture?
    redirect=false
Most Graph API requests will require the use of access tokens which your app can generate by implementing Facebook Login.

This quickstart will show you how the Graph API can read and publish data to the social graph.

How the Graph API is structured
We will cover this fully in our Using Graph API guide, but in general you can read APIs by making HTTP GET requests to nodes or edges on those nodes.

Almost all requests are passed to the API at graph.facebook.com - the single exception is video uploads which use graph-video.facebook.com.

Object IDs
Each node has a unique ID which is used to access it via the Graph API. We specifically do not document any node/object ID structure or format because it is extremely likely to change over time and apps should not make assumptions based on current structure.

Here's how you'd use the ID to make a request for a node:

GET graph.facebook.com
  /{node-id}
or edge:

GET graph.facebook.com
  /{node-id}/{edge-name}
You can generally publish to APIs by making HTTP POST requests with parameters to the node:

POST graph.facebook.com
  /{node-id}
or edge:

POST graph.facebook.com
  /{node-id}/{edge-name}
Deleting via APIs is accomplished using HTTP DELETE requests (and updating via POST requests) to the same endpoints.

API versions
The Graph API has multiple versions available to access at any one time. Each version contains a set of core fields and edge operations. We make a guarantee that those core APIs will be available and un-modified in that version for at least 2 years from release. The platform changelog can tell you which versions are currently available.

An important thing to note is that certain operations (such as publishing) within an edge, or certain fields within a node, may be core without the entire edge or node being core. We annotate these core APIs using this  symbol within our Graph API reference docs.

Everything outside of these core APIs are called extended APIs. These APIs are still accessed through versioned paths, but they can potentially be modified or removed at any time, subject to 90-day migrations that would be announced on our platform roadmap. Alternatively they may simply be included in the next available API version.

You can read more about the intent of versioning in our guide, but here we'll explain how you make a call to a specific version of the Graph API.

It's really simple, just pre-pend the version identifier to the start of the request path. For example, here's a call to v1.0

HTTPPHP SDKJavaScript SDKiOS SDKAndroid SDK
GET /v1.0/me HTTP/1.1
Host: graph.facebook.com
/* PHP SDK v4.0.0 */
/* make the API call */
$request = new FacebookRequest(
  $session,
  'GET',
  '/v1.0/me'
);
$response = $request->execute();
$graphObject = $response->getGraphObject();
/* handle the result */
/* make the API call */
FB.api(
    "/v1.0/me",
    function (response) {
      if (response && !response.error) {
        /* handle the result */
      }
    }
);
/* make the API call */
[FBRequestConnection startWithGraphPath:@"/v1.0/me"
                             parameters:nil
                             HTTPMethod:@"GET"
                      completionHandler:^(
                          FBRequestConnection *connection,
                          id result,
                          NSError *error
                      ) {
                          /* handle the result */
                      }];
/* make the API call */
new Request(
    session,
    "/v1.0/me",
    null,
    HttpMethod.GET,
    new Request.Callback() {
        public void onCompleted(Response response) {
            /* handle the result */
        }
    }
).executeAsync();
This works for all versions, in this general form:

GET graph.facebook.com
  /vn/{request-path}
where n is the version needed. We publish a full list of available versions in our changelog. All of our Graph API Reference docs provide per-version information, so please check you're looking at the correct version - some versions remove nodes and edges, some versions will add nodes and edges.

Now let's try an API request, so you can see how easy it is.

Load the Graph API Explorer
The easiest way to understand the Graph API is to use it with the Graph API Explorer, a low-level tool you can use to query, add and remove data. It's a very handy resource to have at your fingertips while you build your app.

So your next step is to goto https://developers.facebook.com/tools/explorer.

Generate a basic Access Token
When you get to building your own app, you'll need to learn about access tokens and how to generate them using Facebook Login, but for now, we can get one really quickly through the Graph API Explorer.

Click on the "Get Access Token" button in the top right of the Explorer:


In the next dialog that appears, don't check any boxes, just click the blue "Get Access Token" button. Now you'll see the Login Dialog, click "OK" here to proceed.

Make your first Graph API request
Now you're ready to make your first Graph API request, we'll start with a 'read' request. In the text field beside the "GET" dropdown button (we'll call this the path field), delete the existing text and type in 'me':


Now press the "Submit" button. It'll take a few seconds to process, but you should now see a whole bunch of information appear in the response panel. What appears here for you is determined by the privacy settings of your profile, but there should at least be some basic fields:


What you've just done here in the Graph API Explorer is the equivalent of the following Graph API 'read' request:

GET graph.facebook.com
  /me
/me is a special endpoint that translates to the user ID of the person whose access token is being used to make the request.

Congratulations, you've just made your first Graph API request!

Get Publishing Permissions
Next we'll try publishing something to Facebook using the Graph API. First, click on that "Get Access Token" button again, and this time, choose publish_actions permission under the "Extended Permissions" tab:


Now click on the blue "Get Access Token" button, and you'll see the Login Dialog again. Here you'll be asked for permission for the Graph API Explorer to post on your behalf. If you want you can change the audience here to 'Only Me' so that only you will be able to see the post, but you should accept this dialog and move onto the next step.

Publish a Post
Now click on the button that says "GET" on it and choose "POST" from the dropdown selector that appears. Enter me/feed in the path field, and then click "Add a Field".

In the new fields that appear, put message as the "name", and Hello, World as the "value. It should look something like this:


Now click that blue "Submit" button, and after a few seconds the response panel should show something like:

{
  "id": "{new-post-id}"
}
This means that you've just published your first post via the Graph API! You can goto your profile, and you should be able to see the post there:


What you've just done here in the Graph API Explorer is the equivalent of the following Graph API 'publish' request:

POST graph.facebook.com
  /me/feed?
    message="Hello, World."&
    access_token={your-access-token}
Next Steps
You've covered the basics here, so why not continue to learn more about Using the Graph API? If you haven't yet, we recommend you first read about Facebook Login and especially about how to generate Access Tokens which you will need to make more complex Graph API requests.

Was this document helpful?YesNo
The basics
How the Graph API is structured
Object IDs
API versions
Load the Graph API Explorer
Generate a basic Access Token
Make your first Graph API request
Get Publishing Permissions
Publish a Post
Next Steps