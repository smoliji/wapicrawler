Google  
  Search
smolikjirka@gmail.com
Odhlásit se

Skrýt
Služby Google Places API
Google Places API 
 Help developers solve problems with Google Helpouts
Nahlásit problém s dokumentací
Developer's Guide
Introduction
Usage Limits and Billing
Requesting more Quota
Place Search
Place Details
Place Actions
Manage
Events
Bumps
Place Photos
Place Autocomplete
Query Autocomplete
JavaScript Library
Reference
Supported Types
Blog
Forum
FAQ
Developer Policies and Terms
Places API Policies
Zagat Attribution
Terms of Service
Place Search

Looking to use this service in a JavaScript application? Check out the Places Library of the Google Maps API v3.

Note: The id and reference fields are deprecated as of June 24, 2014. They are replaced by the new place ID, a unique identifier that can be used to compare places and to retrieve information about a place. The Places API currently returns a place_id in all responses, and accepts a placeid in the Place Details and Place Delete requests. Soon after June 24, 2015, the API will stop returning the id and reference fields in responses. Some time later, the API will no longer accept the reference in requests. We recommend that you update your code to use the new place ID instead of id and reference as soon as possible.
The Google Places API allows you to query for place information on a variety of categories, such as: establishments, prominent points of interest, geographic locations, and more. You can search for places either by proximity or a text string. A Place Search returns a list of places along with summary information about each place; additional information is available via a Place Details query.

Nearby Search Requests
Text Search Requests
Radar Search Requests
Search Responses
Search Results
Accessing Additional Results
Nearby Search Requests

Earlier versions of the Places API referred to Nearby Search as Place Search.

A Nearby Search lets you search for places within a specified area. You can refine your search request by supplying keywords or specifying the type of place you are searching for.

A Nearby Search request is an HTTP URL of the following form:

https://maps.googleapis.com/maps/api/place/nearbysearch/output?parameters
where output may be either of the following values:

json (recommended) indicates output in JavaScript Object Notation (JSON)
xml indicates output as XML
Certain parameters are required to initiate a Nearby Search request. As is standard in URLs, all parameters are separated using the ampersand (&) character.

Required parameters

key — Your application's API key. This key identifies your application for purposes of quota management and so that places added from your application are made immediately available to your app. Visit the APIs Console to create an API Project and obtain your key.
location — The latitude/longitude around which to retrieve place information. This must be specified as latitude,longitude.
radius — Defines the distance (in meters) within which to return place results. The maximum allowed radius is 50 000 meters. Note that radius must not be included if rankby=distance (described under Optional parameters below) is specified.
If rankby=distance (described under Optional parameters below) is specified, then one or more of keyword, name, or types is required.
Optional parameters

keyword — A term to be matched against all content that Google has indexed for this place, including but not limited to name, type, and address, as well as customer reviews and other third-party content.
language — The language code, indicating in which language the results should be returned, if possible. See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.
minprice and maxprice (optional) — Restricts results to only those places within the specified range. Valid values range between 0 (most affordable) to 4 (most expensive), inclusive. The exact amount indicated by a specific value will vary from region to region.
name — One or more terms to be matched against the names of places, separated with a space character. Results will be restricted to those containing the passed name values. Note that a place may have additional names associated with it, beyond its listed name. The API will try to match the passed name value against all of these names. As a result, places may be returned in the results whose listed names do not match the search term, but whose associated names do.
opennow — Returns only those places that are open for business at the time the query is sent. Places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
rankby — Specifies the order in which results are listed. Possible values are:
prominence (default). This option sorts results based on their importance. Ranking will favor prominent places within the specified area. Prominence can be affected by a place's ranking in Google's index, the number of check-ins from your application, global popularity, and other factors.
distance. This option sorts results in ascending order by their distance from the specified location. When distance is specified, one or more of keyword, name, or types is required.
types — Restricts the results to places matching at least one of the specified types. Types should be separated with a pipe symbol (type1|type2|etc). See the list of supported types.
pagetoken — Returns the next 20 results from a previously run search. Setting a pagetoken parameter will execute a search with the same parameters used previously — all parameters other than pagetoken will be ignored.
zagatselected — Add this parameter (just the parameter name, with no associated value) to restrict your search to locations that are Zagat selected businesses. This parameter must not include a true or false value. The zagatselected parameter is experimental, and is only available to Places API enterprise customers.
Maps API for Work customers should not include a client or signature parameter with their requests.

An example request is below, showing a search for places of type 'food' within a 500m radius of a point in Sydney, Australia, containing the word 'cruise' in their name:

https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AddYourOwnKeyHere
Note that you'll need to replace the key in this example with your own key in order for the request to work in your application.

Text Search Requests

The Google Places API Text Search Service is a web service that returns information about a set of places based on a string — for example "pizza in New York" or "shoe stores near Ottawa". The service responds with a list of places matching the text string and any location bias that has been set. The search response will include a list of places, you can send a Place Details request for more information about any of the places in the response.

The Google Places search services share the same usage limits. However, the Text Search service is subject to a 10-times multiplier. That is, each Text Search request that you make will count as 10 requests against your quota. If you've purchased the Google Places API as part of your Maps API for Work contract, the multiplier may be different. Please refer to the Google Maps API for Business documentation for details.

A Text Search request is an HTTP URL of the following form:

https://maps.googleapis.com/maps/api/place/textsearch/output?parameters
where output may be either of the following values:

json (recommended) indicates output in JavaScript Object Notation (JSON)
xml indicates output as XML
Certain parameters are required to initiate a search request. As is standard in URLs, all parameters are separated using the ampersand (&) character.

Required parameters

query — The text string on which to search, for example: "restaurant". The Google Places service will return candidate matches based on this string and order the results based on their perceived relevance.
key — Your application's API key. This key identifies your application for purposes of quota management and so that places added from your application are made immediately available to your app. Visit the APIs Console to create an API Project and obtain your key.
Optional parameters

location — The latitude/longitude around which to retrieve place information. This must be specified as latitude,longitude. If you specify a location parameter, you must also specify a radius parameter.
radius — Defines the distance (in meters) within which to bias place results. The maximum allowed radius is 50 000 meters. Results inside of this region will be ranked higher than results outside of the search circle; however, prominent results from outside of the search radius may be included.
language — The language code, indicating in which language the results should be returned, if possible. See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.
minprice and maxprice (optional) — Restricts results to only those places within the specified price level. Valid values are in the range from 0 (most affordable) to 4 (most expensive), inclusive. The exact amount indicated by a specific value will vary from region to region.
opennow — Returns only those places that are open for business at the time the query is sent. places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
types — Restricts the results to places matching at least one of the specified types. Types should be separated with a pipe symbol (type1|type2|etc). See the list of supported types.
zagatselected — Add this parameter (just the parameter name, with no associated value) to restrict your search to locations that are Zagat selected businesses. This parameter must not include a true or false value. The zagatselected parameter is experimental, and is only available to Places API enterprise customers.
You may bias results to a specified circle by passing a location and a radius parameter. This will instruct the Google Places service to prefer showing results within that circle. Results outside the defined area may still be displayed.

Maps API for Work customers should not include a client or signature parameter with their requests.

The below example shows a search for restaurants near Sydney.

https://maps.googleapis.com/maps/api/place/textsearch/xml?query=restaurants+in+Sydney&key=AddYourOwnKeyHere
Note that you'll need to replace the key in this example with your own key in order for the request to work in your application.

Radar Search Requests

The Google Places API Radar Search Service allows you to search for up to 200 places at once, but with less detail than is typically returned from a Text Search or Nearby Search request. With Radar Search, you can create applications that help users identify specific areas of interest within a geographic area.

The search response will include up to 200 places, and will include only the following information about each place:

The geometry field containing geographic coordinates.
The place_id, which you can use in a Place Details request to get more information about the place.
The deprecated reference field. See the deprecation notice on this page.
A Radar Search request is an HTTP URL of the following form:

https://maps.googleapis.com/maps/api/place/radarsearch/output?parameters
where output may be either of the following values:

json (recommended) indicates output in JavaScript Object Notation (JSON)
xml indicates output as XML
Certain parameters are required to initiate a search request. As is standard in URLs, all parameters are separated using the ampersand (&) character.

Required parameters

key — Your application's API key. This key identifies your application for purposes of quota management and so that places added from your application are made immediately available to your app. Visit the APIs Console to create an API Project and obtain your key.
location — The latitude/longitude around which to retrieve place information. This must be specified as latitude,longitude.
radius — Defines the distance (in meters) within which to return place results. The maximum allowed radius is 50 000 meters.
Optional parameters

A Radar Search request must include at least one of keyword, name, or types. Other parameters are completely optional.

keyword — A term to be matched against all content that Google has indexed for this place, including but not limited to name, type, and address, as well as customer reviews and other third-party content.
minprice and maxprice (optional) — Restricts results to only those places within the specified price level. Valid values are in the range from 0 (most affordable) to 4 (most expensive), inclusive. The exact amount indicated by a specific value will vary from region to region.
name — One or more terms to be matched against the names of places, separated by a space character. Results will be restricted to those containing the passed name values. Note that a place may have additional names associated with it, beyond its listed name. The API will try to match the passed name value against all of these names. As a result, places may be returned in the results whose listed names do not match the search term, but whose associated names do.
opennow — Returns only those places that are open for business at the time the query is sent. Places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
types — Restricts the results to places matching at least one of the specified types. Types should be separated with a pipe symbol (type1|type2|etc). See the list of supported types.
zagatselected — Add this parameter (just the parameter name, with no associated value) to restrict your search to locations that are Zagat selected businesses. This parameter must not include a true or false value. The zagatselected parameter is experimental, and is only available to Places API enterprise customers.
Maps API for Work customers should not include a client or signature parameter with their requests.

The below example returns a list of museums near London, England.

https://maps.googleapis.com/maps/api/place/radarsearch/json?location=51.503186,-0.126446&radius=5000&types=museum&key=AddYourOwnKeyHere
Using a combination of the keyword, name and types parameters, you can perform more precise queries. The below example shows restaurants and cafes in Paris that users have described as vegetarian.

https://maps.googleapis.com/maps/api/place/radarsearch/json?location=48.859294,2.347589&radius=5000&types=food|cafe&keyword=vegetarian&key=AddYourOwnKeyHere
Note that you'll need to replace the key in these examples with your own key in order for the request to work in your application.

Search Responses

Search responses are returned in the format indicated by the output flag within the URL request's path.

The following example shows a Nearby Search response. A Text Search response is similar, except that it returns a formatted_address instead of a vicinity property. A Radar Search includes only limited fields, as described above.

JSON
{
   "html_attributions" : [],
   "results" : [
      {
         "geometry" : {
            "location" : {
               "lat" : -33.870775,
               "lng" : 151.199025
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/travel_agent-71.png",
         "id" : "21a0b251c9b8392186142c798263e289fe45b4aa",
         "name" : "Rhythmboat Cruises",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 270,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAAF-LjFR1ZV93eawe1cU_3QNMCNmaGkowY7CnOf-kcNmPhNnPEG9W979jOuJJ1sGr75rhD5hqKzjD8vbMbSsRnq_Ni3ZIGfY6hKWmsOf3qHKJInkm4h55lzvLAXJVc-Rr4kI9O1tmIblblUpg2oqoq8RIQRMQJhFsTr5s9haxQ07EQHxoUO0ICubVFGYfJiMUPor1GnIWb5i8",
               "width" : 519
            }
         ],
         "place_id" : "ChIJyWEHuEmuEmsRm9hTkapTCrk",
         "scope" : "GOOGLE",
         "alt_ids" : [
            {
               "place_id" : "D9iJyWEHuEmuEmsRm9hTkapTCrk",
               "scope" : "APP"
            }
         ],
         "reference" : "CoQBdQAAAFSiijw5-cAV68xdf2O18pKIZ0seJh03u9h9wk_lEdG-cP1dWvp_QGS4SNCBMk_fB06YRsfMrNkINtPez22p5lRIlj5ty_HmcNwcl6GZXbD2RdXsVfLYlQwnZQcnu7ihkjZp_2gk1-fWXql3GQ8-1BEGwgCxG-eaSnIJIBPuIpihEhAY1WYdxPvOWsPnb2-nGb6QGhTipN0lgaLpQTnkcMeAIEvCsSa0Ww",
         "types" : [ "travel_agency", "restaurant", "food", "establishment" ],
         "vicinity" : "Pyrmont Bay Wharf Darling Dr, Sydney"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -33.866891,
               "lng" : 151.200814
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "45a27fd8d56c56dc62afc9b49e1d850440d5c403",
         "name" : "Private Charter Sydney Habour Cruise",
         "photos" : [
            {
               "height" : 426,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAAL3n0Zu3U6fseyPl8URGKD49aGB2Wka7CKDZfamoGX2ZTLMBYgTUshjr-MXc0_O2BbvlUAZWtQTBHUVZ-5Sxb1-P-VX2Fx0sZF87q-9vUt19VDwQQmAX_mjQe7UWmU5lJGCOXSgxp2fu1b5VR_PF31RIQTKZLfqm8TA1eynnN4M1XShoU8adzJCcOWK0er14h8SqOIDZctvU",
               "width" : 640
            }
         ],
         "place_id" : "ChIJqwS6fjiuEmsRJAMiOY9MSms",
         "scope" : "GOOGLE",
         "reference" : "CpQBhgAAAFN27qR_t5oSDKPUzjQIeQa3lrRpFTm5alW3ZYbMFm8k10ETbISfK9S1nwcJVfrP-bjra7NSPuhaRulxoonSPQklDyB-xGvcJncq6qDXIUQ3hlI-bx4AxYckAOX74LkupHq7bcaREgrSBE-U6GbA1C3U7I-HnweO4IPtztSEcgW09y03v1hgHzL8xSDElmkQtRIQzLbyBfj3e0FhJzABXjM2QBoUE2EnL-DzWrzpgmMEulUBLGrtu2Y",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Australia"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -33.870943,
               "lng" : 151.190311
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "30bee58f819b6c47bd24151802f25ecf11df8943",
         "name" : "Bucks Party Cruise",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 600,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAA48AX5MsHIMiuipON_Lgh97hPiYDFkxx_vnaZQMOcvcQwYN92o33t5RwjRpOue5R47AjfMltntoz71hto40zqo7vFyxhDuuqhAChKGRQ5mdO5jv5CKWlzi182PICiOb37PiBtiFt7lSLe1SedoyrD-xIQD8xqSOaejWejYHCN4Ye2XBoUT3q2IXJQpMkmffJiBNftv8QSwF4",
               "width" : 800
            }
         ],
         "place_id" : "ChIJLfySpTOuEmsRsc_JfJtljdc",
         "scope" : "GOOGLE",
         "reference" : "CoQBdQAAANQSThnTekt-UokiTiX3oUFT6YDfdQJIG0ljlQnkLfWefcKmjxax0xmUpWjmpWdOsScl9zSyBNImmrTO9AE9DnWTdQ2hY7n-OOU4UgCfX7U0TE1Vf7jyODRISbK-u86TBJij0b2i7oUWq2bGr0cQSj8CV97U5q8SJR3AFDYi3ogqEhCMXjNLR1k8fiXTkG2BxGJmGhTqwE8C4grdjvJ0w5UsAVoOH7v8HQ",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "37 Bank St, Pyrmont"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -33.867591,
               "lng" : 151.201196
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/travel_agent-71.png",
         "id" : "a97f9fb468bcd26b68a23072a55af82d4b325e0d",
         "name" : "Australian Cruise Group",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 242,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAABjeoPQ7NUU3pDitV4Vs0BgP1FLhf_iCgStUZUr4ZuNqQnc5k43jbvjKC2hTGM8SrmdJYyOyxRO3D2yutoJwVC4Vp_dzckkjG35L6LfMm5sjrOr6uyOtr2PNCp1xQylx6vhdcpW8yZjBZCvVsjNajLBIQ-z4ttAMIc8EjEZV7LsoFgRoU6OrqxvKCnkJGb9F16W57iIV4LuM",
               "width" : 200
            }
         ],
         "place_id" : "ChIJrTLr-GyuEmsRBfy61i59si0",
         "scope" : "GOOGLE",
         "reference" : "CoQBeQAAAFvf12y8veSQMdIMmAXQmus1zqkgKQ-O2KEX0Kr47rIRTy6HNsyosVl0CjvEBulIu_cujrSOgICdcxNioFDHtAxXBhqeR-8xXtm52Bp0lVwnO3LzLFY3jeo8WrsyIwNE1kQlGuWA4xklpOknHJuRXSQJVheRlYijOHSgsBQ35mOcEhC5IpbpqCMe82yR136087wZGhSziPEbooYkHLn9e5njOTuBprcfVw",
         "types" : [ "travel_agency", "restaurant", "food", "establishment" ],
         "vicinity" : "32 The Promenade, King Street Wharf 5, Sydney"
      }
   ],
   "status" : "OK"
}
A JSON response contains up to four root elements:

"status" contains metadata on the request. See Status Codes below.
"results" contains an array of places, with information about each. See Search Results for information about these results. The Places API returns up to 20 establishment results per query. Additionally, political results may be returned which serve to identify the area of the request.
html_attributions contain a set of attributions about this listing which must be displayed to the user.
next_page_token contains a token that can be used to return up to 20 additional results. A next_page_token will not be returned if there are no additional results to display. The maximum number of results that can be returned is 60. There is a short delay between when a next_page_token is issued, and when it will become valid.
See Processing JSON with Javascript for help parsing JSON responses.

Status Codes

The "status" field within the search response object contains the status of the request, and may contain debugging information to help you track down why the request failed. The "status" field may contain the following values:

OK indicates that no errors occurred; the place was successfully detected and at least one result was returned.
ZERO_RESULTS indicates that the search was successful but returned no results. This may occur if the search was passed a latlng in a remote location.
OVER_QUERY_LIMIT indicates that you are over your quota.
REQUEST_DENIED indicates that your request was denied, generally because of lack of an invalid key parameter.
INVALID_REQUEST generally indicates that a required query parameter (location or radius) is missing.
Error Messages

When the Google Places service returns a status code other than OK, there may be an additional error_message field within the search response object. This field contains more detailed information about the reasons behind the given status code.

Note: This field is not guaranteed to be always present, and its content is subject to change.
Search Results

When the Google Places service returns JSON results from a search, it places them within a results array. Even if the service returns no results (such as if the location is remote) it still returns an empty results array. XML responses consist of zero or more <result> elements.

Each element of the results array contains a single result from the specified area (location and radius), ordered by prominence. The ordering of results can be influenced by check-in activity within your application - places with many recent check-ins may figure more prominently in your application's results.

The result may also contain attribution information which must be displayed to the user. This is an example of an attribution in JSON format:

"html_attributions" : [
      "Listings by \u003ca href=\"http://www.example.com/\"\u003eExample Company\u003c/a\u003e"
],
This is an attribution in XML format:
<html_attribution>Listings by <a href="http://www.example.com/">Example Company</a></html_attribution>
Each result within the results array may contain the following fields:

events[] (deprecated): An array of one or more <event> elements provides information about current events happening at this place. Up to three events are returned for each place, ordered by start time. Each event contains:
event_id: The unique ID of this event.
summary: A textual description of the event. This property contains a string, the contents of which are not sanitized by the server. Your application should be prepared to prevent or deal with attempted exploits, if necessary.
url: A URL pointing to details about the event. This property will not be returned if no URL was specified for the event.
icon contains the URL of a recommended icon which may be displayed to the user when indicating this result.
id contains a unique stable identifier denoting this place. This identifier may not be used to retrieve information about this place, but is guaranteed to be valid across sessions. It can be used to consolidate data about this place, and to verify the identity of a place across separate searches. Note: The id is now deprecated in favor of place_id. See the deprecation notice on this page.
geometry contains geometry information about the result, generally including the location (geocode) of the place and (optionally) the viewport identifying its general area of coverage.
name contains the human-readable name for the returned result. For establishment results, this is usually the business name.
opening_hours may contain the following information:
open_now is a boolean value indicating if the place is open at the current time.
photos[] — an array of photo objects, each containing a reference to an image. A Place Search will return at most one photo object. Performing a Place Details request on the place may return up to ten photos. More information about Place Photos and how you can use the images in your application can be found in the Place Photos documentation. A photo object is described as:
photo_reference — a string used to identify the photo when you perform a Photo request.
height — the maximum height of the image.
width — the maximum width of the image.
html_attributions[] — contains any required attributions. This field will always be present, but may be empty.
place_id — a unique identifier for a place. To retrieve information about the place, pass this identifier in the placeId field of a Places API request.
scope — Indicates the scope of the place_id. The possible values are:
APP: The place ID is recognised by your application only. This is because your application added the place, and the place has not yet passed the moderation process.
GOOGLE: The place ID is available to other applications and on Google Maps.
Note: The scope field is included only in Nearby Search results and Place Details results. You can only retrieve app-scoped places via the Nearby Search and the Place Details requests. If the scope field is not present in a response, it is safe to assume the scope is GOOGLE.
alt_ids — An array of zero, one or more alternative place IDs for the place, with a scope related to each alternative ID. Note: This array may be empty or not present. If present, it contains the following fields:
place_id — The most likely reason for a place to have an alternative place ID is if your application adds a place and receives an application-scoped place ID, then later receives a Google-scoped place ID after passing the moderation process.
scope — The scope of an alternative place ID will always be APP, indicating that the alternative place ID is recognised by your application only.
For example, let's assume your application adds a place and receives a place_id of AAA for the new place. Later, the place passes the moderation process and receives a Google-scoped place_id of BBB. From this point on, the information for this place will contain:
    "results" : [
      {
        "place_id" : "BBB",
        "scope" : "GOOGLE",
        "alt_ids" : [
          {
            "place_id" : "AAA",
            "scope" : "APP",
          }
        ],
      }
    ]
    
price_level — The price level of the place, on a scale of 0 to 4. The exact amount indicated by a specific value will vary from region to region. Price levels are interpreted as follows:
0 — Free
1 — Inexpensive
2 — Moderate
3 — Expensive
4 — Very Expensive
rating contains the place's rating, from 1.0 to 5.0, based on user reviews.
reference contains a unique token that you can use to retrieve additional information about this place in a Place Details request. Although this token uniquely identifies the place, the converse is not true. A place may have many valid reference tokens. It's not guaranteed that the same token will be returned for any given place across different searches. Note: The reference is now deprecated in favor of place_id. See the deprecation notice on this page.
types[] contains an array of feature types describing the given result. See the list of supported types for more information. XML responses include multiple <type> elements if more than one type is assigned to the result.
vicinity contains a feature name of a nearby location. Often this feature refers to a street or neighborhood within the given results. The vicinity property is only returned for a Nearby Search.
formatted_address is a string containing the human-readable address of this place. Often this address is equivalent to the "postal address". The formatted_address property is only returned for a Text Search.
Premium Data

In addition to the fields listed above, Places API enterprise customers may receive the following fields. These fields will appear as top level children of the result field.

Note: Premium data features are considered experimental and subject to change.

aspects contains a single AspectRating object, for the Primary rating of that establishment. Each AspectRating is described as:
type the name of the aspect that is being rated. The following types are supported: appeal, atmosphere, decor, facilities, food, overall, quality and service.
rating the aggregate rating for this particular aspect, from 0 to 30. Note that aggregate ratings range from 0 to 30, while ratings that appear as part of a review range from 0 to 3.
zagat_selected indicates that the place has been selected as a Zagat quality location. The Zagat label identifies places known for their consistently high quality or that have a special or unique character.
Accessing Additional Results

By default, each Nearby Search or Text Search returns up to 20 establishment results per query; however, each search can return as many as 60 results, split across three pages. If your search will return more than 20, then the search response will include an additional value — next_page_token. Pass the value of the next_page_token to the pagetoken parameter of a new search to see the next set of results. If the next_page_token is null, or is not returned, then there are no further results. There is a short delay between when a next_page_token is issued, and when it will become valid. Requesting the next page before it is available will return an INVALID_REQUEST response. Retrying the request with the same next_page_token will return the next page of results.

For example, in the query below, we search for restaurants near Darling Harbour, in Sydney Australia, and rank the results by distance. You can see that the response contains a next_page_token property.

https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&rankby=distance&types=food&key=AddYourOwnKeyHere
{
   "html_attributions" : [],
   "next_page_token" : "CpQCAgEAAFxg8o-eU7_uKn7Yqjana-HQIx1hr5BrT4zBaEko29ANsXtp9mrqN0yrKWhf-y2PUpHRLQb1GT-mtxNcXou8TwkXhi1Jbk-ReY7oulyuvKSQrw1lgJElggGlo0d6indiH1U-tDwquw4tU_UXoQ_sj8OBo8XBUuWjuuFShqmLMP-0W59Vr6CaXdLrF8M3wFR4dUUhSf5UC4QCLaOMVP92lyh0OdtF_m_9Dt7lz-Wniod9zDrHeDsz_by570K3jL1VuDKTl_U1cJ0mzz_zDHGfOUf7VU1kVIs1WnM9SGvnm8YZURLTtMLMWx8-doGUE56Af_VfKjGDYW361OOIj9GmkyCFtaoCmTMIr5kgyeUSnB-IEhDlzujVrV6O9Mt7N4DagR6RGhT3g1viYLS4kO5YindU6dm3GIof1Q",
   "results" : [
      {
         "geometry" : {
            "location" : {
               "lat" : -33.867217,
               "lng" : 151.195939
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png",
         "id" : "7eaf747a3f6dc078868cd65efc8d3bc62fff77d7",
         "name" : "Biaggio Cafe - Pyrmont",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 600,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAAmWmj0BqA0Jorm1_vjAvx1n6c7ZNBxyY-U9x99-oNyOxvMjDlo2npJzyIq7c3EK1YyoNXdMFDcRPzwLJtBzXAwCUFDGo_RtLRGBPJTA2CoerPdC5yvT2SjfDwH4bFf5MrznB0_YWa4Y2Qo7ABtAxgeBIQv46sGBwVNJQDI36Wd3PFYBoUTlVXa0wn-zRITjGp0zLEBh8oIBE",
               "width" : 900
            }
         ],
         "place_id" : "ChIJIfBAsjeuEmsRdgu9Pl1Ps48",
         "scope" : "GOOGLE",
         "price_level" : 1,
         "rating" : 3.4,
         "reference" : "CoQBeAAAAGu0wNJjuZ40DMrRe3mpn7fhlfIK1mf_ce5hgkhfM79u-lqy0G2mnmcueTq2JGWu9wsgS1ctZDHTY_pcqFFJyQNV2P-kdhoRIeYRHeDfbWtIwr3RgFf2zzFBXHgNjSq-PSzX_OU6OT2_3dzdhhpV-bPezomtrarW4DsGl9uh773yEhDJT6R3V8Fyvl_xeE761DTCGhT1jJ3floFI5_c-bHgGLVwH1g-cbQ",
         "types" : [ "cafe", "bar", "restaurant", "food", "establishment" ],
         "vicinity" : "48 Pirrama Rd, Pyrmont"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -33.866786,
               "lng" : 151.195633
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "3ef986cd56bb3408bc1cf394f3dad9657c1d30f6",
         "name" : "Doltone House",
         "photos" : [
            {
               "height" : 1260,
               "html_attributions" : [ "From a Google User" ],
               "photo_reference" : "CnRwAAAAeM-aLqAm573T44qnNe8bGMkr_BOh1MOVQaA9CCggqtTwuGD1rjsviMyueX_G4-mabgH41Vpr8L27sh-VfZZ8TNCI4FyBiGk0P4fPxjb5Z1LrBZScYzM1glRxR-YjeHd2PWVEqB9cKZB349QqQveJLRIQYKq2PNlOM0toJocR5b_oYRoUYIipdBjMfdUyJN4MZUmhCsTMQwg",
               "width" : 1890
            }
         ],
         "place_id" : "ChIJ5xQ7szeuEmsRs6Kj7YFZE9k",
         "scope" : "GOOGLE",
         "reference" : "CnRvAAAA22k1PAGyDxAgHZk6ErHh_h_mLUK_8XNFLvixPJHXRbCzg-gw1ZxdqUwA_8EseDuEZKolBs82orIQH4m6-afDZV9VcpggokHD9x7HdMi9TnJDmGb9Bdh8f-Od4DK0fASNBL7Me3CsAWkUMWhlNQNYExIQ05W7VbxDTQe2Kh9TiL840hoUZfiO0q2HgDHSUyRdvTQx5Rs2SBU",
         "types" : [ "food", "establishment" ],
         "vicinity" : "48 Pirrama Rd, Pyrmont"
      },
      {
         "aspects" : [
            {
               "rating" : 23,
               "type" : "overall"
            }
         ],
      ...
   ],
   "status" : "OK"
}
To see the next set of results, you can either reissue the same query, or submit a new query, passing the result of the next_page_token to the pagetoken parameter. For example:

https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=CpQCAgEAAFxg8o-eU7_uKn7Yqjana-HQIx1hr5BrT4zBaEko29ANsXtp9mrqN0yrKWhf-y2PUpHRLQb1GT-mtxNcXou8TwkXhi1Jbk-ReY7oulyuvKSQrw1lgJElggGlo0d6indiH1U-tDwquw4tU_UXoQ_sj8OBo8XBUuWjuuFShqmLMP-0W59Vr6CaXdLrF8M3wFR4dUUhSf5UC4QCLaOMVP92lyh0OdtF_m_9Dt7lz-Wniod9zDrHeDsz_by570K3jL1VuDKTl_U1cJ0mzz_zDHGfOUf7VU1kVIs1WnM9SGvnm8YZURLTtMLMWx8-doGUE56Af_VfKjGDYW361OOIj9GmkyCFtaoCmTMIr5kgyeUSnB-IEhDlzujVrV6O9Mt7N4DagR6RGhT3g1viYLS4kO5YindU6dm3GIof1Q&key=AddYourOwnKeyHere
Setting pagetoken will cause any other parameters to be ignored. The query will execute the same search as before, but will return a new set of results. You can request a new page up to two times following the original query. Each page of results must be displayed in turn. Two or more pages of search results should not be displayed as the result of a single query. Note that each search counts as a single request against your usage limits.

The sensor Parameter

The Google Places API previously required that you include the sensor parameter to indicate whether your application used a sensor to determine the user's location. This parameter is no longer required.

Není-li uvedeno jinak, je obsah této stránky licencován podle licence Creative Commons Attribution 3.0 License a ukázky kódu jsou licencovány podle licence Apache 2.0 License. Další informace naleznete v zásadách pro webové stránky.

Last updated září 16, 2014.
 Search

Explore

Products
Showcase
Events
Communities
Connect

Blog
Google+ Community
YouTube Channel
Nahlásit problém
Jobs
Programs

Groups
Experts
Startups
Women Techmakers
Top Products

Ads
Analytics
Android
Cast
Chrome
Cloud
Glass
Google Apps
Google+
Maps
Wallet
YouTube
Terms of Use  Privacy Policy
 
 