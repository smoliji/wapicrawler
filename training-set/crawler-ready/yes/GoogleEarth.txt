Google  
  Search
smolikjirka@gmail.com
Odhlásit se

Skrýt
Služby Google Earth API
Google Earth API 
 Help developers solve problems with Google Helpouts
Nahlásit problém s dokumentací
API Reference
Developer's Guide
Introduction
Placemarks
Balloons
Geometries & Overlays
Camera Control
Layers & Controls
Time
Ocean
Sky, Mars, & Moon
Touring
Events
Accessors
Object Containers
KML
Options
Debugging
Code Samples
Example Applications
Developer's Guide Samples
Demo Gallery
Code Playground
Forums
Browser Plugin
KML
Advanced KML
More Resources
FAQ
Articles
Blog
Release Notes
Utility Library
Known Issues
Related Docs
KML
Maps API
Terms of Service
Google Earth API Reference

The Google Earth API Reference includes a description of the various interfaces, members, and google.earth functions in the Earth API. Interactive inheritance diagrams are also included to depict relationships between interfaces. To view a complete list of all the members for a particular interface, including all the inherited members, click the 'List all members' link below the inheritance diagram.

Note: This reference describes version 1.010 of the API. You can determine the API version that the user's installed plugin binary is compiled against using the getApiVersion method of the GEPlugin interface.

google.earth Namespace Reference

The google.earth namespace contains global functions that aid in the process of using the Earth API interfaces. For example, instantiation of Google Earth Browser Plug-in objects is done via the google.earth.createInstance method and event handling can be accomplished via the google.earth.addEventListener and google.earth.removeEventListener methods.

Browser Plugin-specific Interfaces

Interfaces whose names begin with GE allow for programmatic access to core plugin functionality and other miscellaneous options.

GEAbstractBalloon	GEGlobe	GENavigationControl	GESun
GEControl	GEHitTestResult	GEOptions	GETime
GEEventEmitter	GEHtmlBalloon	GEPhotoOverlayViewer	GETimeControl
GEFeatureBalloon	GEHtmlDivBalloon	GEPlugin	GETourPlayer
GEFeatureContainer	GEHtmlStringBalloon	GESchemaObjectContainer	GEView
GEGeometryContainer	GELinearRingContainer	GEStyleSelectorContainer	GEWindow
KML-based Interfaces (see the KML reference for more information)

Interfaces whose names begin with Kml represent KML-related objects such as <Placemark> and <LookAt>.

KmlAbstractView	KmlFolder	KmlLocation	KmlPolygon
KmlAltitudeGeometry	KmlGeometry	KmlLod	KmlRegion
KmlBalloonOpeningEvent	KmlGroundOverlay	KmlLookAt	KmlScale
KmlBalloonStyle	KmlIcon	KmlModel	KmlScreenOverlay
KmlCamera	KmlIconStyle	KmlMouseEvent	KmlStyle
KmlColor	KmlLabelStyle	KmlMultiGeometry	KmlStyleMap
KmlColorStyle	KmlLatLonAltBox	KmlNetworkLink	KmlStyleSelector
KmlContainer	KmlLatLonBox	KmlObject	KmlTimePrimitive
KmlCoord	KmlLayer	KmlObjectList	KmlTimeSpan
KmlCoordArray	KmlLayerRoot	KmlOrientation	KmlTimeStamp
KmlDateTime	KmlLineString	KmlOverlay	KmlTour
KmlDocument	KmlLineStyle	KmlPhotoOverlay	KmlVec2
KmlEvent	KmlLinearRing	KmlPlacemark	KmlViewerOptions
KmlExtrudableGeometry	KmlLink	KmlPoint
KmlFeature	KmlListStyle	KmlPolyStyle
Není-li uvedeno jinak, je obsah této stránky licencován podle licence Creative Commons Attribution 3.0 License a ukázky kódu jsou licencovány podle licence Apache 2.0 License. Další informace naleznete v zásadách pro webové stránky.

Last updated červen 30, 2014.
 Search

Explore

Products
Showcase
Events
Communities
Connect

Blog
Google+ Community
YouTube Channel
Nahlásit problém
Jobs
Programs

Groups
Experts
Startups
Women Techmakers
Top Products

Ads
Analytics
Android
Cast
Chrome
Cloud
Glass
Google Apps
Google+
Maps
Wallet
YouTube
Terms of Use  Privacy Policy
 
 