Skip to main content
Beta
The BBC Developer site is currently open for registration to BBC Employees. Account requests from other users are not currently being activated. Please check back soon for more info.
 BBC Developer Portal  Toggle navigation
Home
Documentation
FAQs
Blog
Forum
Home
Documentation
FAQs
Blog
Forum
Register
Login
Search

Documentation
BBC Nitro
Nitro Quickstart
Nitro Quickstart

1. Getting Access to Nitro

To use BBC Nitro, you will first need an application key. This is easy to do:

Register for developer.bbc.co.uk
Create your app and add the "BBC Nitro" product to it.
Try out your key on the stage environment: http://nitro.stage.api.bbci.co.uk/nitro/api?api_key={your new key}. 
Once you have built your app on Stage and are happy, please contact nitro@bbc.co.uk to get access to the Live environment.
2. Find the Feed

The starting point for using Nitro is always to work out what type of thing you're trying to find. That determines which feed you use:

Feed	Purpose	Endpoint
Programmes	Find and navigate brands, series, episodes and clips	/nitro/api/programmes
Schedules	Dates, Times, Schedules: when and where are programmes being shown?	/nitro/api/schedules
Versions	Helps you handle the editorial "versions" of episodes (eg signed, cut for language, regional variations, etc)	/nitro/api/versions
Services	Exposes both live and historical BBC services, across TV and Radio.	/nitro/api/services
People	Find the People PIPs knows about: casts, crews, contributors, guests, artists, singers, bands ...	/nitro/api/people
Items	Step inside programmes to find tracklists, chapters, segments, songs, highlights and more	/nitro/api/items
Availabilities	For advanced users only: get specific details around when programmes and clips are available to play	/nitro/api/availabilities
Images	Find images, particularly those in galleries	/nitro/api/images
Promotions	Details of short-term editorially curated "promotions", for instance those programmes featured on iPlayer today	/nitro/api/promotions
Groups	Long-lived collections of programmes and more, including Collections, Seasons and Galleries	/nitro/api/groups
3. Find What You Want

Once you have the feed, there are four tools for getting what you want:

Tool	Description
Filter	Filters let you narrow down the request and are the core function of Nitro. To use a feed, you will virtually always have to filter it. (Generally speaking, performance improves as you add filters and narrow down the amount of data Nitro has to process. In broadcasts especially, providing a date range filter can dramatically improve speed.)
Sort	Sorts order the data, including by properties calculated by Nitro, such as the view counts used to drive the  "most_popular" sort of programmes.
Mixin	Mixins allow you to specify optional elements you would like included in the output, on the understanding that they will impact performance (but save you making additional Nitro calls). For instance, the ancestor_titles mixin exposes all the titles and pids of the ancestors of a given object.
Pagination	Nitro output is paginated. You can request how many items you want returned, but Nitro does not guarantee to honour this, only to return as many as possible. The results expose the page size. A page size of 0 is a special case that only returns you a count of the objects matching your filters.
4. Read the feed

Although there's documentation here, the best place to find the feeds, filters and their values is to read the Nitro "homepage", which is the page at /nitro/api in whichever environment you're using. This is generated automatically from the Nitro code, and is also used to validate parameters and values.

Examples include:

<feed name="Programmes" rel="feed" href="/nitro/api/programmes" title="Start here for programmes metadata: Brands, Series, Episodes and Clips" release_status="supported">
 
<filter name="duration" type="string" title="filter for subset of programmes that have given duration" release_status="supported">
<option value="short" title="filter for programmes that have short duration (< 5m)" href="/nitro/api/programmes?duration=short" />
 
<sort name="views" is_default="false" title="sort numerically by number of views (most popular first - faster most_popular)" release_status="supported">
<sort_direction name="sort_direction" value="ascending" is_default="false" href="/nitro/api/programmes?sort=views&sort_direction=ascending" />
 
<mixin name="people" title="mixin to return information about contributors to a programme" release_status="supported" href="/nitro/api/programmes?mixin=people" />
 

Most important to notice here are the hrefs: these are links that go directly to the feed with those filters/sorts/mixins applied. (You should follow the hrefs where possible, as we reserve the right to change parameter URIs in future.) As you go deeper into the feeds, you'll see that the number of available filters and sorts reduces to only show those that are relevant as further filters on the current set of results. This makes it very easy to build up the query you're looking for.

Responding to changes

Nitro feeds are continually evolving and subject to change, and updates come every two weeks. While we don't break existing calls, we do deprecate them and replace them with better alternatives as they become available.

The best place to see all the hypermedia is the "front page" at /nitro/api/. There you can see, for example:

<sort name="most_popular" is_default="false" title="sort numerically by popularity (most popular first)" release_status="deprecated" deprecated="true" deprecated_since="2013-11-11" replaced_by="views">
The most important one to watch is "release_status". Possible values are:

Supported This is a fully supported Nitro feature, and has the full history of data available. You can use this feature in production code
Beta This feature has been developed, and is available for you to preview and build against. It may not have the full history of data available, and you use it in production code at your own risk. Responses are subject to change.
Alpha This feature is under development, and should not be used in production code. It is highly likely to change, or not operate as expected.
Deprecated This feature is scheduled for removal and should be removed from production code as soon as possible. If a replacement is available, it will be listed under replaced_by although the replacement is not guaranteed to have identical responses to the previous feature.
Once we mark something as deprecated, we mark it with a date under "deprecated_since". Actually removing the feature is done according to a timetable set by the BBC. The fastest this can happen is six weeks.

If a replacement is available, look for it in the replaced_by element. Additionally, you should use the "href" elements in the feed as far as is possible to insulate you from our deprecations - we'll update them automatically as we go.

‹ BBC Nitro up
Privacy Policy
 
Terms of Use