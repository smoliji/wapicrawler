Eventful API

Sign in | Register

Overview
Tools
Technical Reference
Overview

Terms of Use
Branding guidelines
Tools

Join the API mailing list
Tutorial on searching
Application keys
Technical Reference

API documentation
XML feed dictionary
Interface libraries
Output format options
FAQ
Wise words

"Everything is theoretically impossible, until it is done. One could write a history of science in reverse by assembling the solemn pronouncements of highest authority about what could not be done and could never happen."

- Robert A. Heinlein

API Technical Reference

Common API Tasks

Event search
Find lists of events by keyword, location, time period, category, performer, or venue.
Venue search
Find lists of venues by location, name, or type.
For more detail on how to search Eventful effectively, see our search tutorial.

All API Methods

Events

/events/new
Add a new event record.
/events/get
Get an event record.
/events/modify
Modify an event record.
/events/withdraw
Withdraw (delete, remove) an event.
/events/restore
Restore a withdrawn event.
/events/search
Search for events.
/events/reindex
Update the search index for an event record.
/events/ical
Get events in iCalendar format.
/events/rss
Get events in RSS format.
/events/tags/list
List all tags attached to an event.
/events/going/list
List all users going to an event.
/events/tags/new
Add tags to an event.
/events/tags/remove
Remove tags from an event.
/events/comments/new
Add a comment to an event.
/events/comments/modify
Make changes to an event comment.
/events/comments/delete
Remove a comment from an event.
/events/links/new
Add a URL to an event.
/events/links/delete
Remove a URL from an event.
/events/images/add
Add an image to an event.
/events/images/remove
Remove an image from an event.
/events/performers/add
Add a performer to an event.
/events/performers/remove
Remove a performer from an event.
/events/properties/add
Add a property to an event.
/events/properties/list
List properties for an event.
/events/properties/remove
Remove a property from an event.
/events/categories/add
Add a category to an event.
/events/categories/remove
Remove a category from an event.
/events/dates/resolve
Resolve start and end dates from a date string.
Venues

/venues/new
Add a new venue record.
/venues/get
Get a venue record.
/venues/modify
Make changes to a venue.
/venues/withdraw
Withdraw (delete, remove) a venue.
/venues/restore
Restore a withdrawn venue.
/venues/search
Search for venues.
/venues/tags/list
List all tags attached to an venue.
/venues/tags/new
Add tags to an venue.
/venues/tags/delete
Remove tags from an venue.
/venues/ical
Get events at a venue in iCalendar format.
/venues/rss
Get events at a venue in RSS format.
/venues/comments/new
Add a comment to a venue.
/venues/comments/modify
Make changes to a venue comment.
/venues/comments/delete
Remove a comment from a venue.
/venues/links/new
Add a URL to a venue.
/venues/links/delete
Remove a URL from an event.
/venues/properties/add
Add a property to an venue.
/venues/properties/list
List properties for an venue.
/venues/properties/remove
Remove a property from an venue.
/venues/resolve
Resolve a venue from a location string.
Users

/users/calendars/list
List a user's calendars.
/users/calendars/get
Get the settings for a user's calendar.
/users/get
Get a user record.
/users/search
Searches for users.
/users/groups/list
List the groups of which a user is a member.
/users/venue/list
List a user's recently added venues.
/users/locales/add
Add a locale to a user's saved locations.
/users/locales/list
List a user's saved locations.
/users/locales/delete
Delete a locale from a user's saved locations.
/users/going/add
Marks a user as "I'm going" to an event.
/users/going/remove
Removes a user from an event's "I'm going" list.
Images

/images/new
Add a new image.
/images/delete
Delete an image.
Performers

/performers/new
Add a new performer.
/performers/get
Get the details for a performer.
/performers/modify
Modify a performer.
/performers/search
Search for performers.
/performers/withdraw
Delete a performer.
/performers/links/add
Add links to an performer.
/performers/links/remove
Remove links from an performer.
/performers/images/add
Add an image to a performer.
/performers/images/remove
Remove an image from a performer.
/performers/events/list
Get all events for a performer.
/performers/xids/list
Get all performers based on external ids (facebook, etc).
Demand

/demands/get
Get the details for a demand.
/demands/search
Search for demands.
Categories

/categories/list
List the available categories.
All Eventful API methods are available for use via the REST XML, JSON, or YAML interfaces.

Usage Note

Use of the API requires an application key and is subject to the terms of use and branding guidelines. Some methods require user authentication.

PARTNERS //

MTV Networks
Country Music Television
NEXT MOVIE
seatwave.com
About
Blog
FAQ
Advertise
Data Licensing
Jobs
Developer API
Privacy Policy
Terms of Service
Contact Us
© Copyright 2004-2010 Eventful, Inc. All rights reserved. Patent pending.

Like Us
Follow Us
Send Feedback