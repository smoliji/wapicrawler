DEVELOPER
My Apps
Tools
Support
News

Home
News
My Applications
Web API
HTML Widgets
iOS SDK (Beta)
Android SDK (Beta)
Libspotify SDK
Metadata API (deprecated)
Lookup
Search
Support
Design Resources
Terms of Use


Newsletter

Sign up for our regular newsletter and get the latest news for our developer community.  

Metadata API (Deprecated)
Use the Metadata API to explore the Spotify music catalog and retrieved track, album and artist data.

Topics
Services
Requests
Examples
Responses
Encoding
Response format
Territory restrictions
Explicit Flag
Caching
Rate limiting
Related topics
Terms of Use
With the release of our new Web API the Metadata API is deprecated. We recommend you migrate your existing code to the new API as soon as possible to take advantage of its richer metadata and improved rate limits for registered applications. For now the Metadata API endpoints are supported, but they will be removed sometime in the future.
Note that by using Spotify developer tools, you accept our Developer Terms of Use.
Services
These are the services available through the Metadata API.

lookup
search
Requests
You may use ordinary HTTP GET messages. The base URL for each API method should adhere to the following syntax:

https://ws.spotify.com/service/version/method[.format]?parameters

Examples
https://ws.spotify.com/search/1/track?q=kaizers+orchestra
https://ws.spotify.com/search/1/track.json?q=kaizers+orchestra
Responses
Currently, the API uses only HTTP status codes for error messages. If the status code is not 200 OK, the response body will be empty.

Available status codes
200 OK
The request was successfully processed.
304 Not Modified
The data hasn’t changed since your last request. See Caching.
400 Bad Request
The request was not understood. Used for example when a required parameter was omitted.
403 Forbidden
The rate limiting has kicked in.
404 Not Found
The requested resource was not found. Also used if a format is requested using the url and the format isn’t available.
406 Not Acceptable
The requested format isn’t available.
500 Internal Server Error
The server encountered an unexpected problem. Should not happen.
503 Service Unavailable
The API is temporarily unavailable.
Encoding
All requests must be UTF-8. All responses are UTF-8. Example: https://ws.spotify.com/search/1/artist?q=Bj%C3%B6rk.

Response format
There are two ways of negotiating response formats. The preferred way is using the Accept header. In cases where you have no control of the headers sent, you can also use the url to specify response format. In order to do this, append a format extension to the end of the API method. A table of available formats can be found below. XML is the default response format. The JSON format in version 1 of lookup and search is quite ugly. We will change this in a later version.

In order to search for “true affection”, and get JSON back, you can either do

GET /search/1/track?q=true+affection HTTP/1.1
Accept: application/json
or, the format extension way

GET /search/1/track.json?q=true+affection HTTP/1.1
Available formats
Description
Accept Header
Format Extension
JSON	application/json	.json
XML	application/xml, text/xml	.xml
Requesting an unavailable format with content negotiation will result in a 406 Not Acceptable error. Specifying an unavailable format in the url will result in a 404 Not Found.

Territory restrictions
Albums and tracks in Spotify are subject to territorial restrictions. The APIs expose in which country each album and track may be played. In the album element (of album and track search), there is an element called availability. Inside it, there is an element territories. It contains a list of the ISO 3166-1 alpha-2 country codes in which the album is available. The value may also be the special string “worldwide”, indicating that the album can be played anywhere.

Example:

<?xml version="1.0" encoding="utf-8"?>
<tracks xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/" xmlns="http://www.spotify.com/ns/music/1">
  <opensearch:Query role="request" startPage="1" searchTerms="lykke li"/>
  <opensearch:totalResults>117</opensearch:totalResults>
  <opensearch:startIndex>0</opensearch:startIndex>
  <opensearch:itemsPerPage>100</opensearch:itemsPerPage>
  <track href="spotify:track:6PZDPg3dZgJkNL6nVMUB4b">
    <name>Little Bit</name>
    <artist href="spotify:artist:6oBm8HB0yfrIc9IHbxs6in">
      <name>Lykke Li</name>
    </artist>
    <album href="spotify:album:6zBW3pmU291VbFHq4EdU8C">
      <name>Youth Novels</name>
      <released>2008</released>
      <availability>
        <territories>FI NO SE</territories>
      </availability>
      ...
If you know in which country the user of your application lives, and this is outside of Finland, Norway and Sweden, you may want to indicate that this track will not be playable.

Explicit Flag
Certain tracks in the Spotify catalogue contain explicit language or themes. These can be identified by an explicit element returned in the API response. If a track is considered to be explicit the element will be set to true, otherwise it will be absent.

Please note that this element may also be absent if we do not have the information for that specific track.

Caching
Most API responses come with the Last-Modified and Expires HTTP headers set. These can be used for client-side cache control. If you have a response cache, don’t request again until the response has expired, and whenever you do request something again, set the If-Modified-Sincerequest header to the last Last-Modified value returned. If the response hasn’t changed, 304 Not Modified is returned.

If you first request

GET /search/1/track?q=i+turn+my+camera+on HTTP/1.1

HTTP/1.1 200 OK
Last-Modified: Wed, 06 May 2009 07:45:56 GMT

<?xml version="1.0" encoding="utf-8"?>
...
And then

GET /search/1/track?q=i+turn+my+camera+on HTTP/1.1
If-Modified-Since: Wed, 06 May 2009 07:45:56 GMT

HTTP/1.1 304 Not Modified
...
Rate limiting
To make the Metadata API snappy and open for everyone to use, rate limiting rules apply. If you make too many requests too fast, you’ll start getting 403 Forbidden responses. When rate limiting has kicked in, you’ll have to wait 10 seconds before making more requests.

The rate limit is currently 10 request per second per ip. This may change.

Related topics
Linking to Spotify — a guide to linking to Spotify using URLs and URIs.
Terms of Use
Note that by using Spotify developer tools, you accept our Developer Terms of Use.
 

comments powered by Disqus
Developer Terms of Use
About Jobs Press News
Legal Cookies  AdChoices © 2007-2014 Spotify AB