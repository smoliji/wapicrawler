Google Maps
Explore


Sign in
Sign up
Panoramio API - Display photos from Panoramio on your own website

Using Panoramio API you can display the photos from Panoramio on your own web site. Geolocated photos from Panoramio are great to enrich your maps or illustrate information where location is a important factor (real estate sites, hotels and vacation sites, routes & trails...).

Example 1: Photos enriching a map

Photos provided by Panoramio. Photos are under the copyright of their owners.

Example 2: Photos illustrating an area

Hotel Acme - Miami Beach

Hotel Acme buildingA landmark building in the historic city centre, sophisticated in design, the hotel is walking distance of major tourist attractions, 192 techno savvy rooms, stylish restaurant & bar, hot spot rooftop bar & terrace and fitness centre.

Photos from the area around the hotel

Photos provided by Panoramio. Photos are under the copyright of their owners.

How does it work?

It's a very simple REST api, you only have to do a GET on:

http://www.panoramio.com/map/get_panoramas.php?set=public&from=0&to=20&minx=-180&miny=-90&maxx=180&maxy=90&size=medium&mapfilter=true
for "set" you can use:

public (popular photos)
full (all photos)
user ID number
for "size" you can use:

original
medium (default value)
small
thumbnail
square
mini_square
the minx, miny, maxx, maxy define the area to show photos from (minimum longitude, latitude, maximum longitude and latitude, respectively).

You can define the number of photos to be displayed using "from=X" and "to=Y", where Y-X is the number of photos included. The value 0 represents the latest photo uploaded to Panoramio. For example, "from=0 to=20" will extract a set of the last 20 photos uploaded to Panoramio, "from=20 to=40" the previous set of 20 photos and so on. The maximum number of photos you can retrieve in one query is 100.

When the mapfilter parameter is set to true, photos are filtered such that they look better when they are placed on a map. It takes into account the location and tries to avoid of returning photos of the same location.

The result data is formatted using JSON. An example:


    {
      "count": 773840,"photos": [
        {
          "photo_id": 532693,
          "photo_title": "Wheatfield in afternoon light",
          "photo_url": "http://www.panoramio.com/photo/532693",
          "photo_file_url": "http://static2.bareka.com/photos/medium/532693.jpg",
          "longitude": 11.280727,
          "latitude": 59.643198,
          "width": 500,
          "height": 333,
          "upload_date": "22 January 2007",
          "owner_id": 39160,
          "owner_name": "Snemann",
          "owner_url": "http://www.panoramio.com/user/39160",
        },
        {
          "photo_id": 505229,
          "photo_title": "Etangs prÃ¨s de Dijon",
          "photo_url": "http://www.panoramio.com/photo/505229",
          "photo_file_url": "http://static2.bareka.com/photos/medium/505229.jpg",
          "longitude": 5.168552,
          "latitude": 47.312642,
          "width": 350,
          "height": 500,
          "upload_date": "20 January 2007",
          "owner_id": 78506,
          "owner_name": "Philippe Stoop",
          "owner_url": "http://www.panoramio.com/user/78506"
        }, ...
      ]
    }
    
"count" is the total number of photos available on that set of photos on that area. "photos" is an array with the requested photos. The variables of each "photo" object should be trivial to interpret, except maybe "owner_id". If the "owner_id" is 78506, then the user page of the guy that uploaded that photo will be http://www.panoramio.com/user/78506

It's also available as JSONP, just add an extra callback=your_function_name to the GET, and you will get:

your_function_name({ "count": 773840, "photos": [...] })
If you have any doubt about the code, don't hesitate to contact us at questions‌@panoramio.com

Attribution Requirements

Every photo displayed in your site should include Panoramio's name and our logo. The API provides photos in six possible versions: mini square, square, thumbnail, small, medium (default) and original. Each mini square-, square-, thumbnail- or small-size version of a photo must link to a medium or original sized version of the same photo. Each medium or original sized version must link to the photo page on the Panoramio.com domain (for example, http://www.panoramio.com/photo/532693). The link should be under the image itself. No more than 50 instances of photos can be displayed on one single webpage.

Additionally, every time a medium or original photo is used, you must also show the Panoramio logo directly under or directly next to the photo, as well as display "author: name", where "name" is linked to the author's Panoramio photos homepage (for example, http://www.panoramio.com/user/1429589). You must also display the text "Photos provided by Panoramio are under the copyright of their owners" under Panoramio photos.

Query Limit

Panoramio API is free for both commercial and non-commercial purposes that don't exceed the limit of 100000 queries through the API per day.

Questions

More information can be found here: Panoramio API - Terms of Use.

If you have any doubt of suggestions, feel free to contact us at questions‌@panoramio.com

Terms of Service
Privacy Policy
API
Help
