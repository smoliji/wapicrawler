MapLarge Logo
PRODUCTS
VISUALIZATION
USE CASES
DEMOS
FEATURES
GEOCODER
NEWS
CONTACT US
1-855-MAP-LARGE

API
API Benefits
Industry Examples
Tutorial
Examples
API Reference
API SDK
Compatible API
GIS Data Formats
Mapping API with Interactive Data Visualization Tools

MapLarge mapping software offers an API to embed interactive maps on your website, blog or news story. Web editors can save months of development time. Installation only requires a few lines of code.

Input data, processed by API, create stunning maps, analytics, and charts
THE MAPLARGE API CONSISTS OF THREE COMPONENTS:

circle with arrow pointed rightUser Data or our Curated Data Sets
circle with arrow pointed rightThe MapLarge Geo Processing and Mapping Engine
circle with arrow pointed rightA JavaScript and Flex API to request map tiles and interactive content

Daytime Demographic Estimates
DATA UPLOAD
Users can upload both point and polygon data. An example would be a list of customer locations and their revenues. Once uploaded, a user could view zip codes shaded by the customer revenues within each zip code. Administrators can update the data as often as they wish.

REAL TIME AUTOMATED IMPORTS
Data can be added to the system on the fly and viewed instantly, either by users or by automated processes.

Map of schools in FL that have been geocoded
GEO PROCESSING AND MAPPING ENGINE

The MapLarge API only requires a data set with locations and values. Once uploaded the API can dynamically perform point-in-polygon tests and math functions on the values included within a polygon. The Geo Processor can determine sum, average, minimum and maximum values.

The Mapping Engine will then create the interactive tiles with polygons and points color shaded and sized according to the values requested by the user. Multiple users can each request their own unique queries of the data.

INTERACTIVE TILE REQUEST API
The MapLarge API supports tile requests from standard Google Maps, Bing Maps and ESRI Flex implementations. We also support stand alone URL requests for other applications.

Heat Map

DROP IN COMPATIBLE
It is easy to implement and use our maps API with your existing solution. Compatiblity with other popular mapping APIs means you can seamlessly switch and get the best imagery and other resources from each system.

URL REQUEST FORMAT
Interactive tiles and data are returned when the MapLarge receives a simple REST query. A typical query will include the tile requested and the layer to display. What follows is a brief explanation of the core functions. Many additional functions are available.



HTTP Driven, Restful API
 

Layer Parameter - Geographical Visualization & Data Set
 

Shader Parameter - Color & Design Options for Polygons & Points
 

Filter Parameter - Queries the Data Set
Organizations around the world use MapLarge.     Learn what we can do for you. Request More Information Popup
ABOUT MAPLARGE
Products
Features
Visualization
Demos
Use Cases
MAPLARGE API
API
API Benefits
API Examples
API Tutorials
Industry Examples
DETAILS
Data Format Types
SDK & Languages
Integration
Videos
Online GIS
MAPPING
Business
Geocoder
Geocoder API
Routing
Traffic Count
INFORMATION
Contact
Sales
News
Terms of Use
Privacy Policy
MapLargeCopyright © 2014 All rights reserved. Facebook logo  twitterlogo email icon1-855-MAP-LARGE