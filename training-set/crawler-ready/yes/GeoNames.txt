
 GeoNames Home | Postal Codes | Download / Webservice | About |	
search 
GeoNames Web Services Documentation

GeoNames is mainly using REST webservices. 

Important:
The parameter 'username' needs to be passed with each request. The username for your application can be registered here. You will then receive an email with a confirmation link and after you have confirmed the email you can enable your account for the webservice on your account page
Don't forget to url encode string parameters containing special characters or spaces. (Faq entry on url encoding)
Use the JSON services if you want to use GeoNames from javascript, as most browsers do not allow to call xml services from ANOTHER server.
all web services on one table.
client libraries
Service Level Agreement is available for our commercial web services.
credits per request
Exceptions - error handling



Webservices

GeoNames search

Webservice for the GeoNames full text search in xml and json format. See the service description for details. 


Postal Code Search

Url	»	api.geonames.org/postalCodeSearch?
Result	»	returns a list of postal codes and places for the placename/postalcode query as xml document 
For the US the first returned zip code is determined using zip code area shapes, the following zip codes are based on the centroid. For all other supported countries all returned postal codes are based on centroids.
Parameter	Value	Description
postalcode	string (postalcode or placename required)	postal code
postalcode_startsWith	string	the first characters or letters of a postal code
placename	string (postalcode or placename required)	all fields : placename,postal code, country, admin name (Important:urlencoded utf8)
placename_startsWith	string	the first characters of a place name
country	string : country code, ISO-3166 (optional)	Default is all countries. The country parameter may occur more than once, example: country=FR&country=GP
countryBias	string	records from the countryBias are listed first
maxRows	integer (optional)	the maximal number of rows in the document returned by the service. Default is 10
style	string SHORT,MEDIUM,LONG,FULL (optional)	verbosity of returned xml document, default = MEDIUM
operator	string AND,OR (optional)	the operator 'AND' searches for all terms in the placename parameter, the operator 'OR' searches for any term, default = AND
charset	string (optional)	default is 'UTF8', defines the encoding used for the document returned by the web service.
isReduced	true or false (optional)	default is 'false', when set to 'true' only the UK outer codes are returned. Attention: the default value on the commercial servers is currently set to 'true'. It will be changed later to 'false'.
east,west,north,south	float (optional)	bounding box, only features within the box are returned
Example http://api.geonames.org/postalCodeSearch?postalcode=9011&maxRows=10&username=demo

This service is also available in JSON format : http://api.geonames.org/postalCodeSearchJSON?postalcode=9011&maxRows=10&username=demo

[more on free-geocoding ...] 



Placename lookup with postalcode (JSON)

Webservice Type : REST /JSON 
Url : api.geonames.org/postalCodeLookupJSON?
Parameters : postalcode,country ,maxRows (default = 20),callback, charset (default = UTF-8)
Result : returns a list of places for the given postalcode in JSON format, sorted by postalcode,placename 
Example http://api.geonames.org/postalCodeLookupJSON?postalcode=6600&country=AT&username=demo
Details for this service with an ajax step by step example for placename autocomplete 



Find nearby postal codes / reverse geocoding

This service comes in two flavors. You can either pass the lat/long or a postalcode/placename.
Webservice Type : REST 
Url : api.geonames.org/findNearbyPostalCodes?
Parameters : 
lat,lng, radius (in km), maxRows (default = 5),style (verbosity : SHORT,MEDIUM,LONG,FULL), country (default = all countries), localCountry (in border areas this parameter will restrict the search on the local country, value=true)
or
postalcode,country, radius (in Km), maxRows (default = 5)
Result : returns a list of postalcodes and places for the lat/lng query as xml document. The result is sorted by distance. For Canada the FSA is returned (first 3 characters of full postal code) 
Example: 
http://api.geonames.org/findNearbyPostalCodes?lat=47&lng=9&username=demo 
or 
api.geonames.org/findNearbyPostalCodes?postalcode=8775&country=CH&radius=10&username=demo

This service is also available in JSON format : api.geonames.org/findNearbyPostalCodesJSON?postalcode=8775&country=CH&radius=10&username=demo 

[more reverse geocoding webservices] 



Postal code country info

Webservice Type : REST 
Url : api.geonames.org/postalCodeCountryInfo?
Result : countries for which postal code geocoding is available.
Example : http://api.geonames.org/postalCodeCountryInfo?username=demo 


Find nearby populated place / reverse geocoding

Webservice Type : REST 
Url : api.geonames.org/findNearbyPlaceName?
Parameters : lat,lng,
lang: language of returned 'name' element (the pseudo language code 'local' will return it in local language),
radius: radius in km (optional), maxRows: max number of rows (default 10)
style: SHORT,MEDIUM,LONG,FULL (default = MEDIUM), verbosity of returned xml document
localCountry: in border areas this parameter will restrict the search on the local country, value=true
cities: optional filter parameter with three possible values 'cities1000', 'cities5000','cities15000'. See the download readme for further infos
Result : returns the closest populated place (feature class=P) for the lat/lng query as xml document. The unit of the distance element is 'km'. 
Example: 
http://api.geonames.org/findNearbyPlaceName?lat=47.3&lng=9&username=demo 

This service is also available in JSON format :
http://api.geonames.org/findNearbyPlaceNameJSON?lat=47.3&lng=9&username=demo 


Find nearby toponym / reverse geocoding

Webservice Type : REST 
Url : api.geonames.org/findNearby?
Parameters : lat,lng, featureClass,featureCode, radius: radius in km (optional), maxRows : max number of rows (default 10)
style : SHORT,MEDIUM,LONG,FULL (default = MEDIUM), verbosity of returned xml document
localCountry: in border areas this parameter will restrict the search on the local country, value=true
Result : returns the closest toponym for the lat/lng query as xml document 
Example: 
http://api.geonames.org/findNearby?lat=47.3&lng=9&username=demo 

This service is also available in JSON format :
http://api.geonames.org/findNearbyJSON?lat=47.3&lng=9&username=demo 


Extended Find nearby toponym / reverse geocoding

Webservice Type : REST 
Url : api.geonames.org/extendedFindNearby?
Parameters : lat,lng
Result : returns the most detailed information available for the lat/lng query as xml document
It is a combination of several services. Example: 
In the US it returns the address information. 
In other countries it returns the hierarchy service: http://api.geonames.org/extendedFindNearby?lat=47.3&lng=9&username=demo 
On oceans it returns the ocean name. 

[more reverse geocoding webservices] 


get geoNames feature for geoNameId

Webservice Type : REST 
Url : api.geonames.org/get?
Parameters : geonameId, lang (optional), style (optional)
Result : returns the attribute of the geoNames feature with the given geonameId as xml document 
Example: 
http://api.geonames.org/get?geonameId=1&username=demo. 



Place Hierarchy Webservices

Children

Hierarchy

Siblings

Neighbours




Wikipedia Webservices

Find nearby Wikipedia Entries / reverse geocoding

Wikipedia full text search




JSON Webservices

Cities and Placenames

Recent Earthquakes

Weather Stations with most recent Weather Observation

Placename lookup with postalcode




Other Webservices

RSS to GeoRSS Conversion

Details on RSS to GeoRSS converter.
Semantic Web Webservices

Details on GeoNames Semantic Web services. 


Country Info (Bounding Box, Capital, Area in square km, Population)

Webservice Type : REST 
Url : api.geonames.org/countryInfo?
Parameters : country (default = all countries)
lang : ISO-639-1 language code (en,de,fr,it,es,...) (default = english)
Result : Country information : Capital, Population, Area in square km, Bounding Box of mainland (excluding offshore islands)
Example : http://api.geonames.org/countryInfo?username=demo
An other countryInfo service is available as csv output :
Example : http://api.geonames.org/countryInfoCSV?lang=it&country=DE&username=demo





CountryCode / reverse geocoding

The iso country code of any given point.
Webservice Type : REST 
Url : api.geonames.org/countryCode?
Parameters : lat,lng, type, lang, radius (buffer in km for closest country in coastal areas, a positive buffer expands the positiv area whereas a negative buffer reduces it);
Result : returns the iso country code for the given latitude/longitude
With the parameter type=xml this service returns an xml document with iso country code and country name. The optional parameter lang can be used to specify the language the country name should be in. JSON output is produced with type=JSON
Example http://api.geonames.org/countryCode?lat=47.03&lng=10.2&username=demo 

[more reverse geocoding webservices] 



Country Subdivision / reverse geocoding

The iso country code and the administrative subdivision of any given point.
Webservice Type : REST 
Url : api.geonames.org/countrySubdivision?
Parameters : lat,lng, lang (default= names in local language), radius (buffer in km for closest country in coastal areas, a positive buffer expands the positiv area whereas a negative buffer reduces it),level (level of ADM);
Result : returns the country and the administrative subdivison (state, province,...) for the given latitude/longitude
Example http://api.geonames.org/countrySubdivision?lat=47.03&lng=10.2&username=demo

With the parameters 'radius' and 'maxRows' you get the closest subdivisions ordered by distance : 
api.geonames.org/countrySubdivision?lat=47.03&lng=10.2&maxRows=10&radius=40 

This service is also available in JSON format : api.geonames.org/countrySubdivisionJSON?lat=47.03&lng=10.2&username=demo 



Ocean / reverse geocoding

The name of the ocean or sea.
Webservice Type : REST 
Url : api.geonames.org/ocean?
Parameters : lat,lng, radius (optional)
Result : returns the ocean or sea for the given latitude/longitude
The oceans returned by the service are listed here. Example http://api.geonames.org/ocean?lat=40.78343&lng=-43.96625&username=demo 

This service is also available in JSON format : api.geonames.org/oceanJSON?lat=40.78343&lng=-43.96625&username=demo 



Neighbourhood / reverse geocoding

The neighbourhood for US cities. Data provided by Zillow under cc-by-sa license.
Webservice Type : REST 
Url : api.geonames.org/neighbourhood?
Parameters : lat,lng
Result : returns the neighbourhood for the given latitude/longitude
Example http://api.geonames.org/neighbourhood?lat=40.78343&lng=-73.96625&username=demo 

This service is also available in JSON format : api.geonames.org/neighbourhoodJSON?lat=40.78343&lng=-73.96625&username=demo 

[more reverse geocoding webservices] 



Elevation - SRTM3

Shuttle Radar Topography Mission (SRTM) elevation data. SRTM consisted of a specially modified radar system that flew onboard the Space Shuttle Endeavour during an 11-day mission in February of 2000. The dataset covers land areas between 60 degrees north and 56 degrees south.
This web service is using SRTM3 data with data points located every 3-arc-second (approximately 90 meters) on a latitude/longitude grid. Data voids in the original SRTM files have been filled by cgiar. Thanks to Andy Jarvis and Cgiar to allow GeoNames to use the processed data. 
Documentation : cgiar-csi,Nasa
Webservice Type : REST 
Url : api.geonames.org/srtm3?
Parameters : lat,lng;
request method: GET or POST
sample area: ca 90m x 90m Result : a single number giving the elevation in meters according to srtm3, ocean areas have been masked as "no data" and have been assigned a value of -32768 
Example http://api.geonames.org/srtm3?lat=50.01&lng=10.2&username=demo 

This service is also available in XML and JSON format :api.geonames.org/srtm3XML?lat=50.01&lng=10.2&username=demo api.geonames.org/srtm3JSON?lat=50.01&lng=10.2&username=demo 

The text version of the service also accepts a list of lat/lng for the parameters 'lats' and 'lngs'. On the free server the number of points per call is limited to 20, for the premium service the limit is 2000:http://api.geonames.org/srtm3?lats=50.01,51.01&lngs=10.2,11.2&username=demo 



Elevation - Aster Global Digital Elevation Model

Webservice Type : REST 
Url : api.geonames.org/astergdem?
Parameters : lat,lng;
request method: GET or POST
sample are: ca 30m x 30m, between 83N and 65S latitude. Result : a single number giving the elevation in meters according to aster gdem, ocean areas have been masked as "no data" and have been assigned a value of -9999 
Example http://api.geonames.org/astergdem?lat=50.01&lng=10.2&username=demo 

This service is also available in XML and JSON format : api.geonames.org/astergdemXML?lat=50.01&lng=10.2&username=demo and api.geonames.org/astergdemJSON?lat=50.01&lng=10.2&username=demo 

The text version of the service also accepts a list of lat/lng for the parameters 'lats' and 'lngs'. On the free server the number of points per call is limited to 20, for the premium service the limit is 2000. http://api.geonames.org/srtm3?lats=50.01,51.01&lngs=10.2,11.2&username=demo 


Elevation - GTOPO30

GTOPO30 is a global digital elevation model (DEM) with a horizontal grid spacing of 30 arc seconds (approximately 1 kilometer). GTOPO30 was derived from several raster and vector sources of topographic information. Documentation : USGS Gtopo30
Webservice Type : REST 
Url : api.geonames.org/gtopo30?
Parameters : lat,lng;
sample area: ca 1km x 1km Result : a single number giving the elevation in meters according to gtopo30, ocean areas have been masked as "no data" and have been assigned a value of -9999 
Example http://api.geonames.org/gtopo30?lat=47.01&lng=10.2&username=demo 

This service is also available in JSON format : http://api.geonames.org/gtopo30JSON?lat=47.01&lng=10.2&username=demo 



Timezone

Webservice Type : REST 
Url : api.geonames.org/timezone?
Parameters : lat,lng, radius (buffer in km for closest timezone in coastal areas), date (date for sunrise/sunset);
Result : the timezone at the lat/lng with gmt offset (1. January) and dst offset (1. July) 
Example http://api.geonames.org/timezone?lat=47.01&lng=10.2&username=demo 

This service is also available in JSON format : http://api.geonames.org/timezoneJSON?lat=47.01&lng=10.2&username=demo 


Element:
timezoneId: name of the timezone (according to olson), this information is sufficient to work with the timezone and defines DST rules, consult the documentation of your development environment. Many programming environments include functions based on the olson timezoneId (expamle java TimeZone) 
time: the local current time
rawOffset: the amount of time in hours to add to UTC to get standard time in this time zone. Because this value is not affected by daylight saving time, it is called raw offset. 
gmtOffset: offset to GMT at 1. January (deprecated)
dstOffset: offset to GMT at 1. July (deprecated)






Support GeoNames with a donation : 
 

 

 

 

info@geonames.org
GeoNames Home • Postal Codes • Download / Webservice • Forum • Blog • Sitemap