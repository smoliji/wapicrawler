Google  
  Search
smolikjirka@gmail.com
Odhlásit se

Skrýt
Služby Monetize AdSense AdSense Management API
AdSense Management API 
 Help developers solve problems with Google Helpouts
Nahlásit problém s dokumentací
Client Libraries and Samples
Implementation
Getting Started
Making Direct Requests
Reporting
Reporting Basics
Dimensions and Metrics
Handling the Results
Filtering
Common pitfalls
Using Dimensions and Metrics
Using the Right Dimension
Metadata for Dimensions and Metrics
Filling in Missing Dates
Using Relative Date Keywords
Performance and Scale
Running Large Reports
Batching
Reference
Metrics and Dimensions List
Local Time Zone Reporting
Release Notes
Standard Parameters
Currency codes
Version 1.4
Accounts
get
list
Accounts.adclients
list
Accounts.adunits
get
getAdCode
list
Accounts.adunits.customchannels
list
Accounts.alerts
delete
list
Accounts.customchannels
get
list
Accounts.customchannels.adunits
list
Accounts.payments
list
Accounts.reports
generate
Accounts.reports.saved
generate
list
Accounts.savedadstyles
get
list
Accounts.urlchannels
list
Metadata.dimensions
list
Metadata.metrics
list
AdclientsDeprecated
list
AdunitsDeprecated
get
getAdCode
list
Adunits.customchannelsDeprecated
list
AlertsDeprecated
delete
list
CustomchannelsDeprecated
get
list
Customchannels.adunitsDeprecated
list
PaymentsDeprecated
list
ReportsDeprecated
generate
Reports.savedDeprecated
generate
list
SavedadstylesDeprecated
get
list
UrlchannelsDeprecated
list
Version 1.3
Accounts
get
list
Accounts.adclients
list
Accounts.adunits
get
list
getAdCode
Accounts.alerts
list
Accounts.customchannels
get
list
Accounts.customchannels.adunits
list
Accounts.reports
generate
Accounts.reports.saved
generate
list
Accounts.savedadstyles
get
list
Accounts.urlchannels
list
Metadata.dimensions
list
Metadata.metrics
list
Metadata resource
AdclientsDeprecated
list
AdunitsDeprecated
get
list
getAdCode
Adunits.customchannelsDeprecated
list
AlertsDeprecated
list
CustomchannelsDeprecated
get
list
Customchannels.adunitsDeprecated
list
ReportsDeprecated
generate
Reports.savedDeprecated
generate
list
SavedadstylesDeprecated
get
list
UrlchannelsDeprecated
list
Version 1.2
Accounts
get
list
Accounts.adclients
list
Accounts.adunits
get
list
Accounts.customchannels
get
list
Accounts.customchannels.adunits
list
Accounts.reports
generate
Accounts.reports.saved
generate
list
Accounts.savedadstyles
get
list
Accounts.urlchannels
list
AdclientsDeprecated
list
AdunitsDeprecated
get
list
Adunits.customchannelsDeprecated
list
CustomchannelsDeprecated
get
list
Customchannels.adunitsDeprecated
list
ReportsDeprecated
generate
Reports.savedDeprecated
generate
list
SavedadstylesDeprecated
get
list
UrlchannelsDeprecated
list
Resources
APIs Console
Performance Tips
System limits
Upgrading from an older version
Terms
Community
Announcements Mailing List
Stack Overflow
Blog
Google+ Page
Forum
Getting Started

This document is for developers who want to use the AdSense Management API to get information about their AdSense account. This document assumes that you're familiar with web programming concepts and web data formats.

Contents

Before you start
Get an AdSense account
Get familiar with AdSense
Choose your client library
Register your application
Quick start tutorial
Before you start

Get an AdSense account

You need an AdSense account for testing purposes. If you already have a test account, then you're all set; you can visit the AdSense user interface to set up, edit, or view your test data.

Get familiar with AdSense

If you're not familiar with AdSense concepts read the introductory information on AdSense and experiment with the user interface before starting to code.

Choose your client library

In the Client Libraries and Samples page you’ll find information on the available libraries and samples. Click the tab for your chosen language and follow the links to download the source. The client libraries handle the following for you:

authentication
discovery of services
building the requests to the API
response parsing
Read the instructions to install and configure your chosen client library, typically found in the README file in the root directory of the repository.

If your implementation has special needs, such as using an unsupported language, you can make direct requests to the API instead of using a client library.

Register your application

To use the AdSense Management API you must register the application you're developing with Google:

Go to the APIs Console.
Either sign into your Google account or create an account.
Create a new project.
In your newly created project, click the Activate button under the AdSense Management API.
Go to the API Access section.
Create a Client ID:
Web application, if your implementation lives in a web server.
Installed application, for everything else.
Note: Service Accounts are not supported as they access protected data.

Edit the configuration files for your client library project with the new credentials you just created. Check the client library documentation for more details.
Note: The Google account used for registration should be your developer account, that is, the account that you wish users of your application to see as the developer of the application. This account does not need to be tied to an AdSense login, as users will be granting access to their own accounts while using the application.

Quick start tutorial

Follow the steps below to make your first requests, note that these steps may vary slightly depending on the library or language you are using:

Use the appropriate sample to retrieve a list of ad clients from an AdSense account. This request initiates a one-time process in which the AdSense user authenticates and authorizes your project.
Ad clients represent an association between an AdSense account and an AdSense product, such as Content Ads or Search Ads. An AdSense account can have one or multiple ad clients.

For web applications, users will be redirected to a site where they can choose to grant access. Once authorized, they will be redirected to the callback URL defined in the APIs console.
Installed applications work similarly. The client library will try to open a browser window and use an authorization code. For Android, Chrome and iOS applications this method is platform-specific.
Use the APIs explorer in reports.generate to request the following report:
startDate: today-1m
endDate: today
dimensions: DATE
metrics: EARNINGS
Reports give you insight into what you're earning, as well as what's having an impact on those earnings. They can be run on an entire account or on a subset of ad units, through the use of channels.

Try to request the same report from your application.
You can filter the reports by ad units. Fetch the list of ad units using adunits.list. Note that an ad client ID is needed (get it from step 1). After you choose an ad unit, use its ID in the filter parameter for reports.generate:
Ad Units are user-configured placeholders for ads, that define some properties for the ads being shown (such as size and shape).

startDate: today-1m
endDate: today
dimensions: DATE
metrics: EARNINGS
filter: AD_UNIT_ID==ca-pub-123456789:987654321
Try filtering by custom or URL channels or mixing multiple filters.
Channels are tools that let you track the performance of a subset of your ad units. There are two types of channels: URL and custom. The former lets you track performance across a specific page or domain, whereas the latter help you track performance on specific user-selected groups of ad units.

Optionally, join the AdSense API Announcements Group.
You are ready to start your implementation by exploring the rest of the available calls and resources in the reference documentation.

Není-li uvedeno jinak, je obsah této stránky licencován podle licence Creative Commons Attribution 3.0 License a ukázky kódu jsou licencovány podle licence Apache 2.0 License. Další informace naleznete v zásadách pro webové stránky.

Last updated červenec 16, 2014.
 Search

Explore

Products
Showcase
Events
Communities
Connect

Blog
Google+ Community
YouTube Channel
Nahlásit problém
Jobs
Programs

Groups
Experts
Startups
Women Techmakers
Top Products

Ads
Analytics
Android
Cast
Chrome
Cloud
Glass
Google Apps
Google+
Maps
Wallet
YouTube
Terms of Use  Privacy Policy
 
 