YelpDevelopersSign Up Log In  

Documentation
Learn how to authenticate the Yelp API using OAuth v1.0a protocol
Search for businesses by location, keyword and category
Query Yelp attributes such as photo, rating, menus and review snippets
Yelp Developers
Get started
Documentation
Introduction
Authentication
Search API
Business API
iPhone Apps
Errors
Additional Resources
Display requirements
Support group
Code samples
Yelp Platform
Yelp Open Source
FAQ
Terms of use
Search API
Request
Name	Method	Description
/v2/search	GET	Search for local businesses.
General Search Parameters
Name	Data Type	Required / Optional	Description
term	string	optional	Search term (e.g. "food", "restaurants"). If term isn’t included we search everything.
limit	number	optional	Number of business results to return
offset	number	optional	Offset the list of returned business results by this amount
sort	number	optional	Sort mode: 0=Best matched (default), 1=Distance, 2=Highest Rated. If the mode is 1 or 2 a search may retrieve an additional 20 businesses past the initial limit of the first 20 results. This is done by specifying an offset and limit of 20. Sort by distance is only supported for a location or geographic search. The rating sort is not strictly sorted by the rating value, but by an adjusted rating value that takes into account the number of ratings, similar to a bayesian average. This is so a business with 1 rating of 5 stars doesn’t immediately jump to the top.
category_filter	string	optional	Category to filter search results with. See the list of supported categories. The category filter can be a list of comma delimited categories. For example, 'bars,french' will filter by Bars and French. The category identifier should be used (for example 'discgolf', not 'Disc Golf').
radius_filter	number	optional	Search radius in meters. If the value is too large, a AREA_TOO_LARGE error may be returned. The max value is 40000 meters (25 miles).
deals_filter	bool	optional	Whether to exclusively search for businesses with deals
Sample Request:
http://api.yelp.com/v2/search?term=food&location=San+Francisco
Don’t forget to review the Location Parameters section below to understand how to pass required location parameters.

Methods of Specifying Location
There are three available methods to specify location in a search. The location is a required parameter, and exactly one of these methods should be used for a request.

Specify Location by Neighborhood, Address, or City
Location is specified by a particular neighborhood, address or city.

The location format is defined below:

location=location
Location Parameters:
Name	Data Type	Required / Optional	Description
location	string	required	Specifies the combination of "address, neighborhood, city, state or zip, optional country" to be used when searching for businesses.
An optional latitude, longitude parameter can also be specified as a hint to the geocoder to disambiguate the location text.

The format for this is defined as:

cll=latitude,longitude
cll Parameters:
Name	Data Type	Required / Optional	Description
latitude	double	required	Latitude of current location
longitude	double	required	Longitude of current location
Sample Request:
http://api.yelp.com/v2/search?term=cream+puffs&location=San+Francisco
http://api.yelp.com/v2/search?term=german+food&location=Hayes&cll=37.77493,-122.419415
Specify Location by Geographical Bounding Box
Location is specified by a bounding box, defined by a southwest latitude/longitude and a northeast latitude/longitude geographic coordinate.

The bounding box format is defined as:

bounds=sw_latitude,sw_longitude|ne_latitude,ne_longitude
Bounds Parameters:
Name	Data Type	Required / Optional	Description
sw_latitude	double	required	Southwest latitude of bounding box
sw_longitude	double	required	Southwest longitude of bounding box
ne_latitude	double	required	Northeast latitude of bounding box
ne_longitude	double	required	Northeast longitude of bounding box
Sample Request:
http://api.yelp.com/v2/search?term=food&bounds=37.900000,-122.500000|37.788022,-122.399797&limit=3
Specify Location by Geographic Coordinate
The geographic coordinate format is defined as:

ll=latitude,longitude,accuracy,altitude,altitude_accuracy
ll Parameters:
Name	Data Type	Required / Optional	Description
latitude	double	required	Latitude of geo-point to search near
longitude	double	required	Longitude of geo-point to search near
accuracy	double	optional	Accuracy of latitude, longitude
altitude	double	optional	Altitude
altitude_accuracy	double	optional	Accuracy of altitude
Sample Request:
http://api.yelp.com/v2/search?term=food&ll=37.788022,-122.399797
Locale Parameters
The following lists optional locale parameters. Results will be localized in the region format and language if supported. Both cc and lang should be specified for proper localization.

Name	Data Type	Required/Optional	Description
cc	string	optional	ISO 3166-1 alpha-2 country code. Default country to use when parsing the location field. United States = US, Canada = CA, United Kingdom = GB (not UK).
lang	string	optional	ISO 639 language code (default=en). Reviews and snippets written in the specified language will be shown.
Response Values:
This section outlines the standard response values from a search.

Name	Type	Definition
region	dict	Suggested bounds in a map to display results in
region.span	dict	Span of suggested map bounds
region.span.latitude_delta	double	Latitude width of map bounds
region.span.longitude_delta	double	Longitude height of map bounds
region.center	dict	Center position of map bounds
region.center.latitude	double	Latitude position of map bounds center
region.center.longitude	double	Longitude position of map bounds center
total	number	Total number of business results
businesses	list	The list of business entries (see Business)
Business:
Name	Type	Definition
id	string	Yelp ID for this business
is_claimed	bool	Whether business has been claimed by a business owner
is_closed	bool	Whether business has been (permanently) closed
name	string	Name of this business
image_url	string	URL of photo for this business
url	string	URL for business page on Yelp
mobile_url	string	URL for mobile business page on Yelp
phone	string	Phone number for this business with international dialing code (e.g. +442079460000)
display_phone	string	Phone number for this business formatted for display
review_count	number	Number of reviews for this business
categories	list	Provides a list of category name, alias pairs that this business is associated with. For example, 
[["Local Flavor", "localflavor"], ["Active Life", "active"], ["Mass Media", "massmedia"]] 
The alias is provided so you can search with the category_filter.
distance	number	Distance that business is from search location in meters, if a latitude/longitude is specified.
rating	number	Rating for this business (value ranges from 1, 1.5, ... 4.5, 5)
rating_img_url	string	URL to star rating image for this business (size = 84x17)
rating_img_url_small	string	URL to small version of rating image for this business (size = 50x10)
rating_img_url_large	string	URL to large version of rating image for this business (size = 166x30)
snippet_text	string	Snippet text associated with this business
snippet_image_url	string	URL of snippet image associated with this business
location	dict	Location data for this business
location.address	list	Address for this business. Only includes address fields.
location.display_address	list	Address for this business formatted for display. Includes all address fields, cross streets and city, state_code, etc.
location.city	string	City for this business
location.state_code	string	ISO 3166-2 state code for this business
location.postal_code	string	Postal code for this business
location.country_code	string	ISO 3166-1 country code for this business
location.cross_streets	string	Cross streets for this business
location.neighborhoods	list	List that provides neighborhood(s) information for business
deals	list	Deal info for this business (optional: this field is present only if there’s a Deal)
deals.id	string	Deal identifier
deals.title	string	Deal title
deals.url	url	Deal url
deals.image_url	url	Deal image url
deals.currency_code	string	ISO_4217 Currency Code
deals.time_start	number	Deal start time (Unix timestamp)
deals.time_end	number	Deal end time (optional: this field is present only if the Deal ends)
deals.is_popular	bool	Whether the Deal is popular (optional: this field is present only if true)
deals.what_you_get	string	Additional details for the Deal, separated by newlines
deals.important_restrictions	string	Important restrictions for the Deal, separated by newlines
deals.additional_restrictions	string	Deal additional restrictions
deals.options	list	Deal options
deals.options.title	string	Deal option title
deals.options.purchase_url	url	Deal option url for purchase
deals.options.price	number	Deal option price (in cents)
deals.options.formatted_price	string	Deal option price (formatted, e.g. "$6")
deals.options.original_price	number	Deal option original price (in cents)
deals.options.formatted_original_price	string	Deal option original price (formatted, e.g. "$12")
deals.options.is_quantity_limited	bool	Whether the deal option is limited or unlimited
deals.options.remaining_count	number	The remaining deal options available for purchase (optional: this field is only present if the deal is limited)
gift_certificates	list	Gift certificate info for this business (optional: this field is present only if there are gift certificates available)
gift_certificates.id	string	Gift certificate identifier
gift_certificates.url	url	Gift certificate landing page url
gift_certificates.image_url	url	Gift certificate image url
gift_certificates.currency_code	string	ISO_4217 Currency Code
gift_certificates.unused_balances	string	Whether unused balances are returned as cash or store credit
gift_certificates.options	list	Gift certificate options
gift_certificates.options.price	number	Gift certificate option price (in cents)
gift_certificates.options.formatted_price	string	Gift certificate option price (formatted, e.g. "$50")
menu_provider	string	Provider of the menu for this business
menu_date_updated	number	Last time this menu was updated on Yelp (Unix timestamp)
Sample Response (for term=yelp, location=sf, limit=1):
{
  "businesses": [
    {
      "categories": [
        [
          "Local Flavor",
          "localflavor"
        ],
        [
          "Mass Media",
          "massmedia"
        ]
      ],
      "display_phone": "+1-415-908-3801",
      "id": "yelp-san-francisco",
      "is_claimed": true,
      "is_closed": false,
      "image_url": "http://s3-media2.ak.yelpcdn.com/bphoto/7DIHu8a0AHhw-BffrDIxPA/ms.jpg",
      "location": {
        "address": [
          "140 New Montgomery St"
        ],
        "city": "San Francisco",
        "country_code": "US",
        "cross_streets": "3rd St & Opera Aly",
        "display_address": [
          "140 New Montgomery St",
          "(b/t Natoma St & Minna St)",
          "SOMA",
          "San Francisco, CA 94105"
        ],
        "neighborhoods": [
          "SOMA"
        ],
        "postal_code": "94105",
        "state_code": "CA"
      },
      "mobile_url": "http://m.yelp.com/biz/4kMBvIEWPxWkWKFN__8SxQ",
      "name": "Yelp",
      "phone": "4159083801",
      "rating_img_url": "http://media1.ak.yelpcdn.com/static/201012161694360749/img/ico/stars/stars_3.png",
      "rating_img_url_large": "http://media3.ak.yelpcdn.com/static/201012161053250406/img/ico/stars/stars_large_3.png",
      "rating_img_url_small": "http://media1.ak.yelpcdn.com/static/201012162337205794/img/ico/stars/stars_small_3.png",
      "review_count": 3347,
      "snippet_image_url": "http://s3-media2.ak.yelpcdn.com/photo/LjzacUeK_71tm2zPALcj1Q/ms.jpg",
      "snippet_text": "Sometimes we ask questions without reading an email thoroughly as many of us did for the last event.  In honor of Yelp, the many questions they kindly...",
      "url": "http://www.yelp.com/biz/yelp-san-francisco",
      "menu_provider": "yelp",
      "menu_date_updated": 1317414369
    }
  ],
  "region": {
    "center": {
      "latitude": 37.786138600000001,
      "longitude": -122.40262130000001
    },
    "span": {
      "latitude_delta": 0.0,
      "longitude_delta": 0.0
    }
  },
  "total": 10651
}
About

About Yelp
Yelp Blog
Press
Investor Relations
Terms of Service
Privacy Policy
Ad Choices
Help

Advertise
Content Guidelines
Support
Business Support
Developers
More

Careers
Yelp Mobile
The Weekly Yelp
Yelp SeatMe
RSS
Top Searches
Copyright © 2004–2014 Yelp Inc. Yelp, Yelp logo, Yelp burst and related marks are registered trademarks of Yelp.