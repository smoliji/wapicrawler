

rss_feed facebook_account twitter_account youtube_account linkedin_account pinterest_account

    DATA BLOG
    DATA GALLERY
    OPEN DATASETS
    API DOCS
    CONTACT

The Basics
What is JSON?

JSON (JavaScript Object Notation) is a simple, lightweight data format that represents arrays and key/value pairs, and is often used in place of XML.

While JSON was originally formulated to take advantage of Javascript’s eval function, libraries are now available for PHP, Java, Actionscript, C#, Ruby, Python, Objective C, and many others.

Learn more about JSON and access popular packages/parsers at http://www.json.org/.

 

How can I make an HTTP request?

Our API is simple: you make an HTTP request–assembled very similarly to a regular DonorsChoose.org front-end project search–and receive classroom project listings in a JSON response.

The easiest way to build your first request is to start by searching for projects on our site. For example, if you’d like to see a list of Arts & Music projects, the search results page address looks like this:

    http://www.donorschoose.org/donors/search.html?subject1=-1

To view the same list in JSON format, simply change the URL like so:

    http://api.donorschoose.org/common/json_feed.html?subject1=-1&APIKey=DONORSCHOOSE

Notice that in addition to changing the sub-domain from www. to api. and the URI from /donors/search.html to /common/json_feed.html, you will need to include an API Key on the query string.

Heads up that the first 70 lines of the response are blank so if you’re viewing the response in your browser, you’ll likely have to scroll a bit to get to the good stuff. (We know this is a bit annoying and hope to fix it soon.)

 

How can I receive a unique API key?

Our fully-featured trial API key “DONORSCHOOSE” is ideal for evaluative or testing purposes, or non-production-quality prototypes.

Once you’re ready to make things official, request a unique API key from us.

Also, we suggest you join our email list so we can notify you of any functional or policy changes.
January 25, 2013
SUBSCRIBE TO OUR BLOG
Play with Data
Explore Open Data Download Data API Live Data
Tags
analytics community donors economics education trends forum generosity impact looker mapping open data opensource policy postgres quote recession redshift teach teachers trends visualization


