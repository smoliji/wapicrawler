API 1.3.0
smugmug.fans.get

Retrieve a list of fans for a user.
Arguments

    OAuth

    oauth_consumer_key string (required)
    oauth_nonce string (required)
    oauth_signature string (required)
    oauth_signature_method string (required)
    oauth_timestamp string (required)
    oauth_token string (required)
    oauth_version string
    Associative boolean (JSON & PHP responses only)
    Callback string (JSON & PHP responses only)
    Extras string
    Pretty boolean
    Sandboxed boolean
    Strict boolean

Response

    Fans array
        Fan struct
            Name string
            NickName string
            URL string

Errors

    Basic
    OAuth

    3 - invalid session
    18 - invalid API key
    22 - missing required parameter (message)
    98 - service unavailable

Sample Responses

    JSON
    PHP
    REST
    XML-RPC

{
    "stat": "ok",
    "method": "smugmug.fans.get",
    "Fans": [
        {
            "Name": "Joe Citizen",
            "NickName": "joe",
            "URL": "http://joe.smugmug.com"
        }
    ]
}


