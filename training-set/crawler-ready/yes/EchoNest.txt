Navigation

Developer
Account
Downloads
Forums
Blog
Echo Nest API Overview¶

Overview¶

The Echo Nest API allows you to call methods that respond in JSON or XML. Individual methods are detailed in the menu on the left. You can call a method by using an HTTP GET to retrieve http://developer.echonest.com/api/v4/[type]/[method] along with method specific arguments.

Keys¶

You must have your own API key to make use of The Echo Nest API. To obtain a key, register for an account.

The example method calls in this set of documentation use a guest API key that is for demonstration purposes only and should not be used for any other application.

Ground Rules¶

We want to be as flexible as possible with our API and data feeds; if you have an interesting use of our platform, we want to help. Here are some basic ground rules that all developers should abide by when using the API and data feeds.

Use common sense. If you're unsure, ask us. - Our terms of service allow for most reasonable non-commercial uses. If you're doing something you think might be a problem or if you have any questions, just contact us at biz@echonest.com.
Read the Terms of Service. - Before you start developing anything using the Echo Nest API, you need to read and abide by the full Terms of Service.
Do not use the API or feeds for commercial use without talking to us. - The Echo Nest's API is free for non-commercial purposes. To use them commercially, talk to us and we will go over the licensing options. In either case, we request that you give us proper branding and attribution when using our data. (But be sure to see the commercial exception for song/identify in the next section). If you are a lone developer selling advertising that is barely covering your running costs, we consider that non-commercial. If you are a company writing paychecks, making plenty of money or driving a venture-capital-purchased MacBook Pro, you're probably commercial. For commercial use, we have standard terms & conditions that we can share with you. We want to be flexible and support many uses of the platform, so just contact us at biz@echonest.com if you have any questions about whether your use is commercial.
Commercial use of Song/Identify is OK. - As part of the free and open Echoprint music identfication system, we are making access to the song identification method when called without any bucket parameters be free for both non-commercial and commercial use. You will still need an API key to access the api method and calls will be rate limited (however, only severely high query rates should trigger the rate limit).
Credit. - Please give us due credit when using the Echo Nest API and feeds. Display an Echo Nest logo available on the logos page along with a link to http://the.echonest.com/ when displaying content provided by the API or feeds.
Don't do anything illegal. - Don't use the API or feeds in connection with anything that promotes or takes part in any products, services, or materials that we would consider malicious, hateful, or illegal.
Don't pretend the data is yours. - You may not sell, lease, share, transfer, or sublicense the API or feeds or access to the API/feeds to any other party than the API key holder. You can cache our data to lighten bandwidth and improve latency, but don't crawl/spider our API or data feeds and make a copy of our data to steal our mojo.
Help us manage server and bandwidth costs. - Don't use the API or feeds in a manner that exceeds reasonable request volume, or constitutes excessive or abusive usage as determined by The Echo Nest. Don't register for multiple API keys in an attempt to cirumvent rate limits.
Respect the Terms of Service of our Partners. - Some of the data returned by the Echo Nest API may be subject to 3rd party terms of service. Make sure you understand and abide by these terms:
7Digital - http://developer.7digital.net/Developers/APIUsageGuidelines
Encoding¶

Use UTF-8 encoding when sending arguments to API methods.

Rate Limits¶

API methods are subject to rate limits. In general, non-commercial users of our API can expect a rate limit of around 120 calls per minute. This may vary depending upon overall system activity. If we are under a light load we may increase the limit associated with your API key and conversely, if we are under heavy load we may reduce that limit. If you need a guaranteed API limit, contact us at biz@echonest.com. You can discover what your current rate limit and activity is by inspecting the response header that is returned with each API method call. There are three fields in the header associated with rate limits:

X-RateLimit-Limit - the current rate limit for this API key
X-RateLimit-Used - the number of method calls used on this API key this minute
X-RateLimit-Remaining - the estimated number of remaining calls allowed by this API key this minute
You can inspect the response header with the -i option to curl like so:

curl -i 'http://developer.echonest.com/api/v4/artist/profile?api_key=YOUR_API_KEY&name=weezer'
This returns a response header with content like so:

HTTP/1.1 200 OK
Content-Length: 135
X-RateLimit-Limit: 120
X-RateLimit-Remaining: 62
X-RateLimit-Used: 58
This indicates that the current rate limit is 120 calls per minute. In the current minute 58 calls have been made and we estimate that you will have 62 calls remaining for the current minute.

If you exceed the rate limit during a period, API methods will fail with an HTTP response status code of 429.

Identifiers¶

Many calls take ID of items (artists, songs, tracks, etc). IDs can be full formed Echo Nest IDs like music://id.echonest.com/~/AR/ARC51DL1187B9A9FED or can in abbreviated form such as ARC51DL1187B9A9FED. We also support other ID spaces through Project Rosetta stone.

Project Rosetta Stone¶

The Echo Nest API supports multiple ID spaces. You can use an ID from a supported ID space in place of an Echo Nest ID in any call that takes an Echo Nest ID. For example, you can get news for Radiohead using a MusicBrainz ID like so:

http://developer.echonest.com/api/v4/artist/news?api_key=YOUR_API_KEY&id=musicbrainz:artist:a74b1b7f-71a5-4011-9441-d0b5e4122711&format=json
Or using a 7Digital ID like so:

http://developer.echonest.com/api/v4/artist/news?api_key=YOUR_API_KEY&id=7digital-US:artist:304&format=json
Similarly you can use a personal catalog foreign_id like so:

http://developer.echonest.com/api/v4/artist/news?api_key=YOUR_API_KEY&id=CAXFDYO12E2688C130:artist:item-1&format=json
Methods that return Echo Nest IDs can also be used to retrieve IDs in a foreign ID space. For example, the following artist similarity call will return musicbrainz artist IDs:

http://developer.echonest.com/api/v4/artist/similar?api_key=YOUR_API_KEY&name=radiohead&format=json&bucket=id:musicbrainz
Certain methods can be constrained to only return items that are members of the given ID space. For example, by setting the limit parameter to true, the following artist/similar call will limit results to those that fall in the 7Digital catalog:

http://developer.echonest.com/api/v4/artist/similar?api_key=YOUR_API_KEY&name=radiohead&format=json&bucket=id:7digital-US&limit=true
Similarly, methods can be constrained to only return items that are members of a personal catalog. For example, the following call will limit similar artists to only those that are contained in the catalog CAABOUD13216257FC7:

http://developer.echonest.com/api/v4/artist/similar?api_key=YOUR_API_KEY&name=radiohead&format=json&bucket=id:CAABOUD13216257FC7&limit=true
Some Rosetta spaces are associated with specific locales. The format for a locale-specific Rosetta ID space is name-XX where XX is the country code for the supported locale. For example, 7digital-US:track:6372821 is a 7Digital ID space for the US locale. The special locale 'WW' indicates a catalog that is a superset of a number of supported locales.

Currently supported ID spaces are:

7Digital artists - Example: 7digital-US:artist:142111 - Supported locales are US, UK and AU
7Digital tracks - Example: 7digital-US:track:6372821 - Supported locales are US, UK and AU
Deezer artists - Example: deezer:artist:399
Deezer tracks - Example: deezer:track:20592361
Discogs artists - Example: discogs:artist:125776
Eventful artists - Example: eventful:artist:P0-001-000083341-6
Facebook artists - Example: facebook:artist:6979332244
Free Music Archive artists - Example: fma:artist:3243
Free Music Archive tracks - Example: fma:track:11764
JamBase artists - Example: jambase:artist:8317
LyricFind US songs - Example: lyricfind-US:song:3d294a4831babc7d57169ecda7117a16
OpenAura artists - Example: openaura:artist:528ff583e4b037311cddd0bf
Musicbrainz artists - Example: musicbrainz:artist:a74b1b7f-71a5-4011-9441-d0b5e4122711
MusixMatch songs - Example: musixmatch-WW:song:3310380
Playme artists - Example: playme:artist:1234
Playme tracks - Example: playme:track:554928
Rhapsody artists - Example: rhapsody-US:artist:Art.10620458
Rhapsody tracks - Example: rhapsody-US:track:Tra.1872369
Rdio artists - Example: rdio-US:artist:r91318 - Supported locals are AT, AU, BR, CA, CH, DE, DK, ES, FI, FR, GB, IE, IT, NL NO, NZ, PT, SE and US
Rdio tracks - Example: rdio-US:track:t4438390 - Supported locals are AT, AU, BR, CA, CH, DE, DK, ES, FI, FR, IE, IT, NL NO, NZ, PT, SE, UK and US
SeatGeek artists - Example: seatgeek:artist:35
Seatwave artists - Example: seatwave:artist:1000
Songkick artists - Example: songkick:artist:3084961
SongMeanings artists - Example: songmeaningsg:artist:200
SongMeanings songs - Example: songmeanings:song:471679
Spotify artists - Example: spotify:artist:4Z8W4fKeB5YxbusRsdQVPb
Spotify tracks - Example: spotify:track:3L7BcXHCG8uT92viO6Tikl
Twitter artists - Example: twitter:artist:justinbieber
WhoSampled artists - Example: whosampled:artist:3309
WhoSampled tracks - Example: whosampled:track:8482
Standard Parameters¶

These parameters are valid for all methods in the API

Parameter	Required?	Multiple?	Values	Description
api_key	yes	no	YOUR_API_KEY	the developer API key
format	no	no	json, jsonp, xml	the format of the response
callback	Required if format is jsonp	no	MyJSFunc	the callback function for JSONP requests
Response Codes¶

Code	Value
-1	Unknown Error
0	Success
1	Missing/ Invalid API Key
2	This API key is not allowed to call this method
3	Rate Limit Exceeded
4	Missing Parameter
5	Invalid Parameter
HTTP Response Codes¶

The HTTP Status response status code can be used to determine the status of an API request:

200 - Success
304 - Not Modified - indicates that the resource has not been modified since the version specified by the request headers If-Modified-Since or If-Match.
400 - Bad Request - the request is not valid
403 - Forbidden - you are not authorized to access that resource
404 - Not Found - The requested resource could not be found
429 - Too Many Requests - You have exceeded the rate limit associated with your API key
5XX - Server Error - The Echo Nest is having an internal issue and cannot serve your request
Resources¶

Visit our downloads for links to existing Echo Nest API-related code.

Discussion¶

Join our forums to provide feedback and discuss development with other developers.

Release History¶

August 11, 2014 - Eliminated tasteprofile/similar call
June 16, 2014 - Changed name of Spotify-WW ID space to 'spotify'
May 16, 2014 - Added Rosetta id support for OpenAura
January 15, 2014 - moved taste_profile/predict api to the alpha endpoint
January 9, 2014 - added Rosetta id support for Eventful
January 8, 2014 - added support and documentation for the genre APIs
November 22, 2013 - added *_rank buckets for artist and song methods, removed beta indicators for artist_location and years_active plus minor wording cleanup for many methods
October 31, 2013 - catalog methods renamed to tasteprofile. Item ID is now optional for tasteprofile updates. Added target_xxx parameters to premium static and dynamic playlisting
September 17, 2013 - added support for reserved catalog attributes, support multiple genre seeds in genre-radio, partitioned playisting documentation into basic, standard and premium.
August 1, 2013 - 'styleb' is default style when dmca=true, HTTP Response code 429 returned when rate limit is exceeded.
June 20, 2013 - Added acousticness audio attribute and the acoustic and electric song_types
May 2, 2013 - Added Rosetta id support for SeatGeek
April 29, 2013 - Some playlist/dynamic API usability improvements
March 20, 2013 - Added support and documentation for 'speechiness' and 'liveness' in song and playlist methods.
January 31, 2013 - Added support and documentation for 'artist_location' in artist methods.
December 17, 2012 - Added support and documentation for 'genre' in artist and playlisting methods.
December 14, 2012 - Added support and documentation for 'general' taste profiles.
December 11, 2012 - Added Rosetta id support for WhoSampled artists and tracks. Details in this blog post: WhoSampled Joins Rosetta Stone, Opening Music App Possibilities
November 21, 2012 - Added support for the 'live' and 'studio' song type. Details in the blog post: We are doing it live!
November 16, 2012 - Added support for the 'christmas' song type. Details in the blog post: Christmas comes early to The Echo Nest
November 15, 2012 - Added Rosetta id support for Rhapsody artists and tracks. Details in the blog post: The Echo Nest Partners with Rhapsody
November 5, 2012 - Added Rosetta id support for Deezer artists and tracks. Details in the blog post: The Echo Nest Partners with Deezer
October 9, 2012 - Added support for Taste Profile Attributes Details in the blog post: Taste Profile Attributes Go Public
July 12, 2012 - Added support for Taste Profile similarity. Details in the blog post: The Echo Nest’s Taste Profile Similarity Will Bring People Together
June 8, 2012 - Added support for 19 new Rdio territories. Details in the blog post: New Rdio Rosetta catalogs
May 22, 2012 - Added Rosetta support for Discogs. Details in the blog post: Combining the Discogs and The Echo Nest APIs
April 23, 2012 - Added support Rosetta support for Songkick. Details in the blog post: Songkick Joins Rosetta Stone for Concert Listings in Music Apps
April 12, 2012 - Added Rosetta support for Jambase and SongMeanings. Details in the blog post: JamBase and SongMeanings Bring Concert Listings and Lyric Interpretations to Rosetta Stone
March 30, 2012 - Added Rosetta support for Musixmatch. Details in the blog post: Expand your Echo Nest app with MusixMatch lyrics
March 29, 2012 - Added Rosetta support for Spotify. Details in the blog post: Spotify + The Echo Nest API… Spotify Apps Just Got Smarter
Table Of Contents

Echo Nest API Overview
Overview
Keys
Ground Rules
Encoding
Rate Limits
Identifiers
Project Rosetta Stone
Standard Parameters
Response Codes
HTTP Response Codes
Resources
Discussion
Release History
Artist API Methods
biographies
blogs
familiarity
hotttnesss
images
list_genres
list_terms
news
profile
reviews
search
extract (beta)
songs
similar
suggest (beta)
terms
top_hottt
top_terms
twitter
urls
video
Genre API Methods
artists
list
profile
search
similar
Song API Methods
search
profile
identify
Track API Methods
profile
upload
Playlisting Overview
Overview
Capabilities
Basic Playlisting
basic
Standard Playlisting
static
dynamic
dynamic/create
dynamic/restart
dynamic/next
dynamic/feedback
dynamic/info
dynamic/delete
Premium Playlisting
static
dynamic
dynamic/create
dynamic/restart
dynamic/next
dynamic/feedback
dynamic/steer
dynamic/info
dynamic/delete
Taste Profile API Methods
create
update
keyvalues
play
skip
favorite
ban
rate
status
profile
read
feed
delete
list
predict (alpha)
Data Feeds
Found Video Feed
Found News Feed
Found Blog Posts Feed
Found Reviews Feed
Previous topic

The Echo Nest API documentation

Next topic

Artist API Methods

Home | Developer | Account | Downloads | Forums | Blog | © 2010-2011, The Echo Nest.