Last.fm Site Search 
Music  Listen  Events  Charts  Originals Join Login
BETA Feedback
Account
Your API accounts
Add API account
API Guides
Introduction
User Authentication
Scrobbling
Radio API
Feeds
Playlists API
Tools
REST requests
XML-RPC requests
Error codes
Terms of Service
API Methods
Album
album.addTags
album.getBuylinks
album.getInfo
album.getShouts
album.getTags
album.getTopTags
album.removeTag
album.search
album.share
Artist
artist.addTags
artist.getCorrection
artist.getEvents
artist.getInfo
artist.getPastEvents
artist.getPodcast
artist.getShouts
artist.getSimilar
artist.getTags
artist.getTopAlbums
artist.getTopFans
artist.getTopTags
artist.getTopTracks
artist.removeTag
artist.search
artist.share
artist.shout
Auth
auth.getMobileSession
auth.getSession
auth.getToken
Chart
chart.getHypedArtists
chart.getHypedTracks
chart.getLovedTracks
chart.getTopArtists
chart.getTopTags
chart.getTopTracks
Event
event.attend
event.getAttendees
event.getInfo
event.getShouts
event.share
event.shout
Geo
geo.getEvents
geo.getMetroArtistChart
geo.getMetroHypeArtistChart
geo.getMetroHypeTrackChart
geo.getMetroTrackChart
geo.getMetroUniqueArtistChart
geo.getMetroUniqueTrackChart
geo.getMetroWeeklyChartlist
geo.getMetros
geo.getTopArtists
geo.getTopTracks
Group
group.getHype
group.getMembers
group.getWeeklyAlbumChart
group.getWeeklyArtistChart
group.getWeeklyChartList
group.getWeeklyTrackChart
Library
library.addAlbum
library.addArtist
library.addTrack
library.getAlbums
library.getArtists
library.getTracks
library.removeAlbum
library.removeArtist
library.removeScrobble
library.removeTrack
Playlist
playlist.addTrack
playlist.create
Radio
radio.getPlaylist
radio.search
radio.tune
Tag
tag.getInfo
tag.getSimilar
tag.getTopAlbums
tag.getTopArtists
tag.getTopTags
tag.getTopTracks
tag.getWeeklyArtistChart
tag.getWeeklyChartList
tag.search
Tasteometer
tasteometer.compare
tasteometer.compareGroup
Track
track.addTags
track.ban
track.getBuylinks
track.getCorrection
track.getFingerprintMetadata
track.getInfo
track.getShouts
track.getSimilar
track.getTags
track.getTopFans
track.getTopTags
track.love
track.removeTag
track.scrobble
track.search
track.share
track.unban
track.unlove
track.updateNowPlaying
User
user.getArtistTracks
user.getBannedTracks
user.getEvents
user.getFriends
user.getInfo
user.getLovedTracks
user.getNeighbours
user.getNewReleases
user.getPastEvents
user.getPersonalTags
user.getPlaylists
user.getRecentStations
user.getRecentTracks
user.getRecommendedArtists
user.getRecommendedEvents
user.getShouts
user.getTopAlbums
user.getTopArtists
user.getTopTags
user.getTopTracks
user.getWeeklyAlbumChart
user.getWeeklyArtistChart
user.getWeeklyChartList
user.getWeeklyTrackChart
user.shout
user.signUp
user.terms
Venue
venue.getEvents
venue.getPastEvents
venue.search
Last.fm Web Services
REST Requests

The API root URL is located at http://ws.audioscrobbler.com/2.0/
Generally speaking, you will send a method parameter expressed as 'package.method' along with method specific arguments to the root URL. The following parameters are required for all calls:

api_key : A Last.fm API Key.
method : An API method expressed as package.method, corresponding to a documented last.fm API method name. 
For example:

http://ws.audioscrobbler.com/2.0/?method=artist.getSimilar&api_key=xxx...
If you are accessing a write service, you will need to submit your request as an HTTP POST request. All POST requests should be made to the root url:

http://ws.audioscrobbler.com/2.0/
With all parameters (including the 'method') sent in the POST body. In order to perform write requests you will need to authenticate a user with the API. See authentication for more.

REST Responses
Responses will be wrapped in an lfm status node

<lfm status="$status">
    ...
</lfm>
Where $status is either ok or failed. If the status is failed you'll get an error code and message. You can strip the status wrapper from the response by sending a raw=true argument with your method call.
REST Errors
See the individual method call pages for service specific error codes. Errors will communicate a code and a message in the following format:

<lfm status="failed">
    <error code="10">Invalid API Key</error>
</lfm>
JSON Responses
You can request API responses in JSON format with the following parameters:

format=json : A Last.fm API Key.
callback (Optional) : A callback function name which will wrap the JSON response.

Note: If you don't specify a callback, there's no default, and the response will be pure JSON content with a application/json MIME type. With a callback, the MIME type is text/javascript

The response is a translation of the XML response format, converted according to the following rules:

Attributes are expressed as string member values with the attribute name as key.
Element child nodes are expressed as object members values with the node name as key.
Text child nodes are expressed as string values, unless the element also contains attributes, in which case the text node is expressed as a string member value with the key #text. *
Repeated child nodes will be grouped as an array member with the shared node name as key.
* This idiom is rarely used in our XML responses.

Example success response:

{
"results": {
    "tagmatches": {
      "tag": [{
        "name": "disco",
        "count": "55483",
        "url": "www.last.fm\/tag\/disco"
      },
      ...
      {
        "name": "disco pop",
        "count": "160",
        "url": "www.last.fm\/tag\/disco%20pop"
      }]
    },
    "for": "disco"
  }
}
Original XML response:
<?xml version="1.0" encoding="utf-8"?>
<lfm status="ok">
 <results for="disco" xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">
    <opensearch:Query role="request" searchTerms="disco" startPage="1" />
    <opensearch:totalResults>2641</opensearch:totalResults>
    <opensearch:startIndex>0</opensearch:startIndex>
    <opensearch:itemsPerPage>20</opensearch:itemsPerPage>
    <tagmatches>
      <tag>
        <name>disco</name>
        <count>55483</count>
        <url>www.last.fm/tag/disco</url>
      </tag>
      ...
      <tag>
        <name>disco pop</name>
        <count>160</count>
        <url>www.last.fm/tag/disco%20pop</url>
      </tag>
    </tagmatches>
  </results>
</lfm>
JSON Errors
JSON errors do not follow the same transformation rules as success errors, but use the following simplified form:

Example failure response:

{
    "error": 10,
    "message": "Invalid API Key"
}
Artists and Labels
Connect with fans by uploading your music to Music Manager
Download Our Apps
Get the Last.fm apps for desktop and mobile and start scrobbling
Company
About us
Contact us
Jobs
Blog
Advertise
Join in
Sign up
Find people
Find groups
Community
Forums
Goodies
More apps & devices
Developer API
Tools & plugins
Playground
Free music downloads
Help
Help & support
System status
Languages:
English (current language) Deutsch Español Français Italiano 日本語 Polski Português Руccкий Svenska Türkçe 简体中文 
“Builds Bonny Last.fm.”
CBS Interactive © 2015 Last.fm Ltd. All rights reserved Terms of Use, Privacy Policy and Legal Policies — Updated 26 Nov 2013 Gamespot 
