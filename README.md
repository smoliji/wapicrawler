# Wapicrawler -- nástroj pro sběr web API dokumentace #

Wapicrawler je nástroj, jehož účelem je nalezení a identifikace Web API dokumentací na webu. V první fázi použije vyhledávač Google CSE k nalezení relevantních výsledků, které použije jako referenční body (seedy) pro inicializaci crawlování. Všechny nalezené dokumenty jsou indexovány a následně ohodnoceny klasifikačními algoritmy zda jsou, nebo nejsou Web API dokumentací.

Adresářová struktura odpovídá projektu pro NetBeans IDE 8.0.2

## Jak zprovoznit ##

Nástroj využívá Javu 1.7, crawler [Apache Nutch](http://nutch.apache.org/), indexovací platformu [Apache Solr](http://lucene.apache.org/solr/) a knihovny dostupné se software [Weka3](http://www.cs.waikato.ac.nz/ml/weka/). Pro špuštění nástroje je potřeba

  * Nainstalovat Apache Nutch a Apache Solr. To dostatečně popisuje tutoriál [zde](http://www.cs.waikato.ac.nz/ml/weka/)
  * Přidat následující pole do indexovacího schématu Solr.
```
#!xml

<field name="api-prob" type="float" stored="true" indexed="true " default="1.0"/>
<field name="api-class" type="string" stored="true" indexed="true " default=""/>
```


  * Nakonfigurovat nástroj v souboru crawler.properties. Zejména

     * URL Solr -- solr.url
     * Cestu k Nutch -- nutch.home
     * Cestu k trénovací sadě (jedna je dostupná v training-set/crawler-ready)
     * Příp. vlastní API klíš (cse.api-key) a další parametry.

Po spuštění Solr je nástroj funkční a připraven k použití:

```
#!bash

java -jar Wapicrawler.jar "term to search"
```